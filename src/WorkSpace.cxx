#include "Phoenix/WorkSpace.h"

using namespace std;
using namespace Phoenix;

WorkSpace::WorkSpace(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> c, const TString &rname, const TString &fname) : Result(MSG, c, rname, fname)
{
   TString tree_name = TString::Format("%s_Parameter_Tree", rname.TString::Data());
   Par_Tree = new TTree(tree_name, tree_name);

   Par_Tree->Branch("Name", &Par_Name);
   Par_Tree->Branch("Value", &Par_Value);
   Par_Tree->Branch("Error", &Par_Error);
   Par_Tree->Branch("Nominal", &Par_Nominal);
   Par_Tree->Branch("Min", &Par_Min);
   Par_Tree->Branch("Max", &Par_Max);
   Par_Tree->Branch("Type", &Par_Type);
   Par_Tree->Branch("IsFixed", &Par_IsFixed);
   Par_Tree->Branch("Gamma_Gaus_Sigma", &Par_Gamma_Gaus_Sigma);

   tree_name = TString::Format("%s_MorphingCategory_Tree", rname.TString::Data());
   MorphCate_Tree = new TTree(tree_name, tree_name);

   MorphCate_Parameters = new std::vector<TString>();

   MorphCate_Tree->Branch("Name", &MorphCate_Name);
   MorphCate_Tree->Branch("Mode", &MorphCate_Mode);
   MorphCate_Tree->Branch("Parameters", &MorphCate_Parameters);

   tree_name = TString::Format("%s_Observable_Tree", rname.TString::Data());
   Obs_Tree = new TTree(tree_name, tree_name);

   Obs_Value = new std::vector<double>();
   Obs_Error = new std::vector<double>();

   Obs_Tree->Branch("Name", &Obs_Name);
   Obs_Tree->Branch("Value", &Obs_Value);
   Obs_Tree->Branch("Error", &Obs_Error);

   tree_name = TString::Format("%s_Sample_Tree", rname.TString::Data());
   Sample_Tree = new TTree(tree_name, tree_name);

   Sample_Value = new std::vector<double>();
   Sample_Error = new std::vector<double>();

   Sample_Tree->Branch("Name", &Sample_Name);
   Sample_Tree->Branch("Observable_Name", &Sample_Observable_Name);
   Sample_Tree->Branch("Value", &Sample_Value);
   Sample_Tree->Branch("Error", &Sample_Error);

   tree_name = TString::Format("%s_Variation_Tree", rname.TString::Data());
   Var_Tree = new TTree(tree_name, tree_name);

   Var_MorphingValue = new std::vector<double>();
   Var_Value = new std::vector<double>();
   Var_Error = new std::vector<double>();

   Var_Tree->Branch("Observable_Name", &Var_Observable_Name);
   Var_Tree->Branch("Sample_Name", &Var_Sample_Name);
   Var_Tree->Branch("MorphingCategory_Name", &Var_MorphingCategory_Name);
   Var_Tree->Branch("MorphingValue", &Var_MorphingValue);
   Var_Tree->Branch("Value", &Var_Value);
   Var_Tree->Branch("Error", &Var_Error);

   tree_name = TString::Format("%s_Correlation_Tree", rname.TString::Data());
   Corr_Tree = new TTree(tree_name, tree_name);

   Corr_Matrix = new std::vector<double>();

   Corr_Tree->Branch("Observable_Name_X", &Corr_Observable_Name_X);
   Corr_Tree->Branch("Observable_Name_Y", &Corr_Observable_Name_Y);
   Corr_Tree->Branch("Sample_Name_X", &Corr_Sample_Name_X);
   Corr_Tree->Branch("Sample_Name_Y", &Corr_Sample_Name_Y);
   Corr_Tree->Branch("NBins_X", &Corr_NBins_X);
   Corr_Tree->Branch("NBins_Y", &Corr_NBins_Y);
   Corr_Tree->Branch("Matrix", &Corr_Matrix);
}

void WorkSpace::SaveParameters(std::map<TString, std::shared_ptr<Parameter>>* par_map,
                               std::map<TString, std::shared_ptr<MorphingCategory>>* morph_map)
{
   for (auto &par : (*par_map))
   {
      Par_Name = par.second->GetName();
      Par_Value = par.second->GetValue();
      Par_Error = par.second->GetError();
      Par_Nominal = par.second->GetNominal();
      Par_Min = par.second->GetMin();
      Par_Max = par.second->GetMax();
      Par_Type = Parameter::TypeToTString(par.second->GetType());
      Par_IsFixed = par.second->IsFixed();
      Par_Gamma_Gaus_Sigma = par.second->GetGammaSigma();

      Par_Tree->Fill();
   }

   for (auto &morph : (*morph_map))
   {
      if (morph.second->GetNParameters() == 1 &&
            morph.second->GetName().TString::EqualTo(morph.second->GetParameter(0)->GetName()))
         continue;

      MorphCate_Name = morph.second->GetName();
      MorphCate_Mode = MorphingCategory::ModeToTString(morph.second->GetMorphingMode());
      MorphCate_Parameters->clear();
      for (int i = 0; i < morph.second->GetNParameters(); i++)
         MorphCate_Parameters->push_back(morph.second->GetParameter(i)->GetName());

      MorphCate_Tree->Fill();
   }
}

void WorkSpace::SaveObservables(std::map<TString, std::shared_ptr<Observable>>* obs_map)
{
   for (auto &obs : (*obs_map))
   {
      Obs_Name = obs.second->GetName();
      Obs_Value->clear();
      Obs_Error->clear();
      for (int i = 0; i < obs.second->GetNBins(); i++)
      {
         Obs_Value->push_back(obs.second->GetValue(i));
         Obs_Error->push_back(obs.second->GetError(i));
      }

      Obs_Tree->Fill();
   }
}

void WorkSpace::SaveSamples(std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* samp_map)
{
   for (auto &obs : (*samp_map))
   {
      for (auto &samp : obs.second)
      {
         Sample_Name = samp.second->GetName();
         Sample_Observable_Name = obs.first->GetName();
         Sample_Value->clear();
         Sample_Error->clear();
         for (int i = 0; i < samp.second->GetNBins(); i++)
         {
            Sample_Value->push_back(samp.second->GetValue(i));
            Sample_Error->push_back(samp.second->GetError(i));
         }

         Sample_Tree->Fill();
      }
   }
}

void WorkSpace::SaveVariations(std::map<std::shared_ptr<Observable>,
                               std::map<std::shared_ptr<Sample>,
                               std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>>>
                               * var_val_map)
{
   for (auto &obs : (*var_val_map))
   {
      for (auto &samp : obs.second)
      {
         for (auto &morph : samp.second)
         {
            for (int i = 0; i < morph.second.size(); i++)
            {
               Var_Observable_Name = obs.first->GetName();
               Var_Sample_Name = samp.first->GetName();
               Var_MorphingCategory_Name = morph.first->GetName();
               Var_MorphingValue->clear();
               for (int j = 0; j < morph.second[i]->GetNValues(); j++)
                  Var_MorphingValue->push_back(morph.second[i]->GetValue(j));

               Var_Value->clear();
               Var_Error->clear();
               for (int j = 0; j < morph.second[i]->GetVariation()->GetNBins(); j++)
               {
                  Var_Value->push_back(morph.second[i]->GetVariation()->GetValue(j));
                  Var_Error->push_back(morph.second[i]->GetVariation()->GetError(j));
               }

               Var_Tree->Fill();
            }
         }
      }
   }
}

void WorkSpace::SaveCorrelations(std::map<std::shared_ptr<Observable>,
                                 std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>
                                 * corr_obs_map,
                                 std::map<std::shared_ptr<Sample>,
                                 std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>
                                 * corr_samp_map)
{
   for (auto &obs_x : (*corr_obs_map))
   {
      for (auto &obs_y : obs_x.second)
      {
         Corr_Observable_Name_X = obs_x.first->GetName();
         Corr_Observable_Name_Y = obs_y.first->GetName();
         Corr_Sample_Name_X = "";
         Corr_Sample_Name_Y = "";
         Corr_NBins_X = obs_x.first->GetNBins();
         Corr_NBins_Y = obs_y.first->GetNBins();

         Corr_Matrix->clear();
         for (int i = 0; i < obs_x.first->GetNBins(); i++)
         {
            for (int j = 0; j < obs_y.first->GetNBins(); j++)
               Corr_Matrix->push_back(obs_y.second->GetCovariance(i, j));
         }

         Corr_Tree->Fill();
      }
   }

   for (auto &samp_x : (*corr_samp_map))
   {
      for (auto &samp_y : samp_x.second)
      {
         Corr_Observable_Name_X = samp_x.first->RetriveObservable()->GetName();
         Corr_Observable_Name_Y = samp_y.first->RetriveObservable()->GetName();
         Corr_Sample_Name_X = samp_x.first->GetName();
         Corr_Sample_Name_Y = samp_y.first->GetName();
         Corr_NBins_X = samp_x.first->GetNBins();
         Corr_NBins_Y = samp_y.first->GetNBins();

         Corr_Matrix->clear();
         for (int i = 0; i < samp_x.first->GetNBins(); i++)
         {
            for (int j = 0; j < samp_y.first->GetNBins(); j++)
               Corr_Matrix->push_back(samp_y.second->GetCovariance(i, j));
         }

         Corr_Tree->Fill();
      }
   }
}

void WorkSpace::WriteToFile()
{
   if (GetOutputFileName() == "")
   {
      MSGUser()->MSG_WARNING("input an empty output file name, will be ignored");
      return;
   }

   TFile* file = new TFile(GetOutputFileName(), "UPDATE");
   file->cd();

   Par_Tree->Write();
   MorphCate_Tree->Write();
   Obs_Tree->Write();
   Sample_Tree->Write();
   Var_Tree->Write();
   Corr_Tree->Write();

   file->Close();
}
