#include "Phoenix/Variation.h"

using namespace std;
using namespace Phoenix;

void Variation::RecoverModifications()
{
   if (Cache_ptr != nullptr)
   {
      Value = Cache_ptr->GetCache("Value");
      Error = Cache_ptr->GetCache("Error");
      NBins = Value.rows();
   }
}
void Variation::SetRange(int start, int end)
{
   if (start >= 0 && end >= start)
   {
      int newbinnum = end - start + 1;
      Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
      Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

      if (Cache_ptr == nullptr)
         BookCache();
      int index = 0;
      for (int i = start; i <= end; i++)
      {
         New_Value(index) = Value(i);
         New_Error(index) = Error(i);
         index = index + 1;
      }

      Value = New_Value;
      Error = New_Error;
      NBins = newbinnum;
   }
}

void Variation::Rebin(int num)
{
   if (GetNBins() % num != 0)
      return;
   if (Cache_ptr == nullptr)
      BookCache();

   int newbinnum = GetNBins() / num;
   Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
   Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

   double sum_val = 0;
   double sum_err2 = 0;
   int index = 0;
   for (int i = 0; i < GetNBins(); i++)
   {
      sum_val = sum_val + Value(i);
      sum_err2 = sum_err2 + pow(Error(i), 2);
      if ((i + 1) % num == 0)
      {
         New_Value(index) = sum_val;
         New_Error(index) = sqrt(sum_err2);
         index = index + 1;
         sum_val = 0;
         sum_err2 = 0;
      }
   }

   Value = New_Value;
   Error = New_Error;
   NBins = newbinnum;
}
void Variation::BookCache()
{
   if (Cache_ptr == nullptr)
   {
      Cache_ptr = std::make_shared<Cache>();
      Cache_ptr->SaveCache("Value", Value);
      Cache_ptr->SaveCache("Error", Error);
   }
}
