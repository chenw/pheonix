#include "Phoenix/PLH.h"
#include "Phoenix/Parameter.h"

using namespace std;
using namespace Phoenix;

double PLH_FCN::operator()(const std::vector<double> &vpara) const
{
   // -------------------- //
   // Calculate the -log L // 
   // -------------------- //
   // The structure of this function is like this:
   // if (Fit_Nthread == 1)
   //     Do Single Thread Mode
   // else 
   //   if (WithMetal) 
   //       Do Multi Thread Mode With GPU Boost (CPU + GPU) 
   //   else 
   //       Do Multi Thread Mode With CPU only (CPU only)
   // With Metal might not be able to make the fitting faster 
   // Since at the moment we cannot make CPU and GPU working together
   // TODO:: Add a separation between CPU and GPU, at the moment,
   //        CPU is waiting for GPU to calculate the impact of each parameters
   //        So we need to detect this and adjust this separation point
   //        to balance the work between CPU and GPU
   // TODO:: Add an GPU only mode
   
   if (Fit_Nthread == 1)
   {
      // Single Thread Mode
      int index = 0;
      double prob = 0;

      index = 0;
      for (auto &par : *Par_Map)
      {
         par.second->SetValue(vpara[index]);
         prob = prob + par.second->GetLikelihood();
         index = index + 1;
      }

      std::map<std::shared_ptr<Observable>, Eigen::VectorXd> prof_map;
      if (SeparatePOI)
      {
         for (auto &obs : *Obs_Map)
         {
            Eigen::VectorXd Profile = Eigen::VectorXd::Zero(obs.second->GetNBins());
            Eigen::VectorXd Profile_Error2 = Eigen::VectorXd::Zero(obs.second->GetNBins());
            auto findobs = Sample_Map->find(obs.second);
            if (findobs != Sample_Map->end())
            {
               for (auto &samp : (*Sample_Map)[obs.second])
               {
                  Eigen::VectorXd Profile_Sample = Eigen::VectorXd::Zero(obs.second->GetNBins());
                  Eigen::VectorXd Profile_Error2_Sample = Eigen::VectorXd::Zero(obs.second->GetNBins());
                  double norm = 1;
                  if (samp.second->HasNormalizationParameter())
                     norm = samp.second->GetNormalizationParameter()->GetValue();
                  Eigen::VectorXd SampleNominal = samp.second->GetValue();
                  Profile_Sample = Profile_Sample + SampleNominal;
                  if (!WithStatGamma)
                     Profile_Error2_Sample = Profile_Error2_Sample + samp.second->GetError2();
                  auto findobs2 = Variation_MorphingRange_Map->find(obs.second);
                  if (findobs2 != Variation_MorphingRange_Map->end())
                  {
                     auto findsamp = findobs2->second.find(samp.second);
                     if (findsamp != findobs2->second.end())
                     {
                        // Check Whether this sample is related to an poi
                        bool needpoicheck = false;
                        for (auto &morph_cate : (*Variation_MorphingRange_Map)[obs.second][samp.second])
                        {
                           for (int i = 0; i < morph_cate.first->GetNParameters(); i++)
                           {
                              std::shared_ptr<Parameter> par = morph_cate.first->GetParameter(i);
                              if (par->GetType() == ParameterType::Interest)
                                 needpoicheck = true;
                           }
                        }

                        for (auto &morph_cate : (*Variation_MorphingRange_Map)[obs.second][samp.second])
                        {
                           if (morph_cate.first->GetCache() == nullptr)
                              morph_cate.first->BookCache();

                           // Check Whether Contains POI
                           if (needpoicheck)
                           {
                              bool containpoi = false;
                              for (int i = 0; i < morph_cate.first->GetNParameters(); i++)
                              {
                                 std::shared_ptr<Parameter> par = morph_cate.first->GetParameter(i);
                                 if (par->GetType() == ParameterType::Interest)
                                    containpoi = true;
                              }
                              if (!containpoi)
                                 continue;
                           }
                           std::vector<double> val_vec = morph_cate.first->GetParameterValues();
                           bool getvariation = false;
                           Eigen::VectorXd Var;
                           Eigen::VectorXd Impact;
                           for (int i = 0; i < morph_cate.second.size(); i++)
                           {
                              if (morph_cate.second[i]->Contains(val_vec))
                              {
                                 if (needpoicheck)
                                 {
                                    std::vector<Eigen::VectorXd> Profiled_Variations;
                                    int nvar = val_vec.size() + 1;
                                    for (int j = 0; j < nvar; j++)
                                    {
                                       Eigen::VectorXd Virtual_Nominal = morph_cate.second[i]->RetriveMorphingValue(j)->GetVariation()->GetValue();
                                       Eigen::VectorXd Virtual_Profile = morph_cate.second[i]->RetriveMorphingValue(j)->GetVariation()->GetValue();
                                       // cout << "Original Variation for Var " << j << " " << Virtual_Nominal.transpose() << endl;
                                       for (auto &morph_cate2 : (*Variation_MorphingRange_Map)[obs.second][samp.second])
                                       {
                                          if (morph_cate2.first->GetCache() == nullptr)
                                             morph_cate2.first->BookCache();

                                          bool containpoi = false;
                                          for (int k = 0; k < morph_cate2.first->GetNParameters(); k++)
                                          {
                                             std::shared_ptr<Parameter> par = morph_cate2.first->GetParameter(k);
                                             if (par->GetType() == ParameterType::Interest)
                                                containpoi = true;
                                          }
                                          if (containpoi)
                                             continue;

                                          std::vector<double> virtual_val_vec = morph_cate2.first->GetParameterValues();
                                          bool virtual_getvariation = false;
                                          Eigen::VectorXd Virtual_Var;
                                          Eigen::VectorXd Virtual_Impact;

                                          for (int k = 0; k < morph_cate2.second.size(); k++)
                                          {
                                             if (morph_cate2.second[k]->Contains(virtual_val_vec))
                                             {
                                                Virtual_Var = morph_cate2.second[k]->GetMorphingResult(virtual_val_vec);
                                                virtual_getvariation = true;
                                                break;
                                             }
                                          }
                                          if (virtual_getvariation == false)
                                          {
                                             cout << morph_cate2.first->GetName().TString::Data() << " Out of Range!" << endl;
                                             continue;
                                          }

                                          TString nom_name = TString::Format("%s_%s_Nominal", obs.second->GetName().TString::Data(), samp.second->GetName().TString::Data());
                                          if (morph_cate2.first->GetCache()->FindCache(nom_name))
                                          {
                                             Eigen::VectorXd Virtual_Nom = morph_cate2.first->GetCache()->GetCache(nom_name);
                                             if (UseRelative)
                                                Virtual_Impact = ((Virtual_Var.array() / Virtual_Nom.array() - 1) * Virtual_Nominal.array()).matrix();
                                             else
                                                Virtual_Impact = Virtual_Var - Virtual_Nom;
                                             if (SumImpact)
                                                Virtual_Profile = Virtual_Profile + Virtual_Impact;
                                             else
                                                Virtual_Profile = (Virtual_Var.array() / Virtual_Nom.array() * Virtual_Profile.array()).matrix();
                                          }
                                          else
                                          {
                                             virtual_val_vec = morph_cate2.first->GetParameterNominals();
                                             for (int k = 0; k < morph_cate2.second.size(); k++)
                                             {
                                                if (morph_cate2.second[k]->Contains(virtual_val_vec))
                                                {
                                                   Eigen::VectorXd virtual_nominal = morph_cate2.second[k]->GetMorphingResult(virtual_val_vec);
                                                   if (UseRelative)
                                                      Virtual_Impact = ((Virtual_Var.array() / virtual_nominal.array() - 1) * Virtual_Nominal.array()).matrix();
                                                   else
                                                      Virtual_Impact = Virtual_Var - virtual_nominal;
                                                   if (SumImpact)
                                                      Virtual_Profile = Virtual_Profile + Virtual_Impact;
                                                   else
                                                      Virtual_Profile = (Virtual_Var.array() / virtual_nominal.array() * Virtual_Profile.array()).matrix();
                                                   morph_cate2.first->GetCache()->SaveCache(nom_name, virtual_nominal);
                                                   break;
                                                }
                                             }
                                          }
                                       }
                                       Profiled_Variations.push_back(Virtual_Profile);
                                       // cout << "Profiled Variation for Var " << j << " " << Virtual_Profile.transpose() << endl;
                                    }

                                    // Make a new MorphingRange with the profiled variations
                                    // Transform Eigen::VectorXd to Variation
                                    std::vector<std::shared_ptr<Variation>> Virtual_Variations;
                                    for (int j = 0; j < Profiled_Variations.size(); j++)
                                    {
                                       Eigen::VectorXd virtual_zero = Eigen::VectorXd::Zero(obs.second->GetNBins());
                                       std::shared_ptr<Variation> virtual_var = std::make_shared<Variation>(obs.second, samp.second, morph_cate.first, Profiled_Variations[j], virtual_zero);
                                       Virtual_Variations.push_back(virtual_var);
                                    }
                                    // Transform Variation to MorphingValue
                                    std::vector<std::shared_ptr<MorphingValue>> Virtual_MorphingValues;
                                    for (int j = 0; j < Virtual_Variations.size(); j++)
                                    {
                                       int virtual_nvalue = morph_cate.second[i]->RetriveMorphingValue(j)->GetNValues();
                                       std::vector<double> virtual_val;
                                       for (int k = 0; k < virtual_nvalue; k++)
                                          virtual_val.push_back(morph_cate.second[i]->RetriveMorphingValue(j)->GetValue(k));
                                       std::shared_ptr<MorphingValue> virtual_morphingvalue = std::make_shared<MorphingValue>(virtual_val, Virtual_Variations[j], morph_cate.first);
                                       Virtual_MorphingValues.push_back(virtual_morphingvalue);
                                    }

                                    // Transform MorphingValue to MorphingRange
                                    std::shared_ptr<MorphingRange> Virtual_MorphingRange;
                                    std::vector<std::shared_ptr<MorphingValue>> Original_RangeMorphingValues;
                                    for (int j = 0; j < Virtual_Variations.size(); j++)
                                       Original_RangeMorphingValues.push_back(morph_cate.second[i]->RetriveRangeMorphingValue(j));
                                    Virtual_MorphingRange = std::make_shared<MorphingRange>(Virtual_MorphingValues, Original_RangeMorphingValues, morph_cate.first, samp.second, obs.second);

                                    // Calculate Var with the Virutal Morphing Range
                                    Var = Virtual_MorphingRange->GetMorphingResult(val_vec);
                                 }
                                 else
                                    Var = morph_cate.second[i]->GetMorphingResult(val_vec);
                                 getvariation = true;
                                 break;
                              }
                           }
                           if (getvariation == false)
                           {
                              cout << morph_cate.first->GetName().TString::Data() << " Out of Range!" << endl;
                              continue;
                           }

                           TString nom_name = TString::Format("%s_%s_Nominal", obs.second->GetName().TString::Data(), samp.second->GetName().TString::Data());
                           if (morph_cate.first->GetCache()->FindCache(nom_name))
                           {
                              Eigen::VectorXd Nom = morph_cate.first->GetCache()->GetCache(nom_name);
                              if (UseRelative)
                                 Impact = ((Var.array() / Nom.array() - 1) * SampleNominal.array()).matrix();
                              else
                                 Impact = Var - Nom;
                              if (SumImpact)
                                 Profile_Sample = Profile_Sample + Impact;
                              else
                                 Profile_Sample = (Var.array() / Nom.array() * Profile_Sample.array()).matrix();
                           }
                           else
                           {
                              val_vec = morph_cate.first->GetParameterNominals();
                              for (int i = 0; i < morph_cate.second.size(); i++)
                              {
                                 if (morph_cate.second[i]->Contains(val_vec))
                                 {
                                    Eigen::VectorXd nominal = morph_cate.second[i]->GetMorphingResult(val_vec);
                                    if (UseRelative)
                                       Impact = ((Var.array() / nominal.array() - 1) * SampleNominal.array()).matrix();
                                    else
                                       Impact = Var - nominal;
                                    if (SumImpact)
                                       Profile_Sample = Profile_Sample + Impact;
                                    else
                                       Profile_Sample = (Var.array() / nominal.array() * Profile_Sample.array()).matrix();
                                    morph_cate.first->GetCache()->SaveCache(nom_name, nominal);
                                    break;
                                 }
                              }
                           }
                        }
                     }
                  }

                  if (WithCorr)
                     Profile = Profile + norm * Profile_Sample;
                  else
                  {
                     if (WithStatGamma)
                     {
                        Eigen::VectorXd gamma = norm * samp.second->GetGammaValue();
                        Profile = Profile + (Profile_Sample.array() * gamma.array()).matrix();
                     }
                     else
                     {
                        Profile = Profile + norm * Profile_Sample;
                        Profile_Error2 = Profile_Error2 + pow(norm, 2) * Profile_Error2_Sample;
                     }
                  }
               }
            }
            if (WithCorr)
               prof_map.emplace(std::pair<std::shared_ptr<Observable>, Eigen::VectorXd>(obs.second, Profile));
            else
            {
               if (WithStatGamma)
                  prob = prob + obs.second->GetLikelihood(Profile);
               else
                  prob = prob + obs.second->GetLikelihood_Gaus(Profile, Profile_Error2);
            }
         }
      }
      else
      {
         for (auto &obs : *Obs_Map)
         {
            Eigen::VectorXd Profile = Eigen::VectorXd::Zero(obs.second->GetNBins());
            Eigen::VectorXd Profile_Error2 = Eigen::VectorXd::Zero(obs.second->GetNBins());
            auto findobs = Sample_Map->find(obs.second);
            if (findobs != Sample_Map->end())
            {
               for (auto &samp : (*Sample_Map)[obs.second])
               {
                  Eigen::VectorXd Profile_Sample = Eigen::VectorXd::Zero(obs.second->GetNBins());
                  Eigen::VectorXd Profile_Error2_Sample = Eigen::VectorXd::Zero(obs.second->GetNBins());
                  double norm = 1;
                  if (samp.second->HasNormalizationParameter())
                     norm = samp.second->GetNormalizationParameter()->GetValue();
                  Eigen::VectorXd SampleNominal = samp.second->GetValue();
                  Profile_Sample = Profile_Sample + SampleNominal;
                  if (!WithStatGamma)
                     Profile_Error2_Sample = Profile_Error2_Sample + samp.second->GetError2();
                  auto findobs2 = Variation_MorphingRange_Map->find(obs.second);
                  if (findobs2 != Variation_MorphingRange_Map->end())
                  {
                     auto findsamp = findobs2->second.find(samp.second);
                     if (findsamp != findobs2->second.end())
                     {
                        for (auto &morph_cate : (*Variation_MorphingRange_Map)[obs.second][samp.second])
                        {
                           if (morph_cate.first->GetCache() == nullptr)
                              morph_cate.first->BookCache();

                           int npar = morph_cate.first->GetNParameters();
                           std::vector<double> val_vec = morph_cate.first->GetParameterValues();
                           bool getvariation = false;
                           Eigen::VectorXd Var;
                           Eigen::VectorXd Impact;
                           for (int i = 0; i < morph_cate.second.size(); i++)
                           {
                              if (morph_cate.second[i]->Contains(val_vec))
                              {
                                 Var = morph_cate.second[i]->GetMorphingResult(val_vec);
                                 getvariation = true;
                                 break;
                              }
                           }
                           if (getvariation == false)
                           {
                              cout << morph_cate.first->GetName().TString::Data() << " Out of Range!" << endl;
                              continue;
                           }

                           TString nom_name = TString::Format("%s_%s_Nominal", obs.second->GetName().TString::Data(), samp.second->GetName().TString::Data());
                           if (morph_cate.first->GetCache()->FindCache(nom_name))
                           {
                              Eigen::VectorXd Nom = morph_cate.first->GetCache()->GetCache(nom_name);
                              if (UseRelative)
                                 Impact = ((Var.array() / Nom.array() - 1) * SampleNominal.array()).matrix();
                              else
                                 Impact = Var - Nom;
                              if (SumImpact)
                                 Profile_Sample = Profile_Sample + Impact;
                              else
                                 Profile_Sample = (Var.array() / Nom.array() * Profile_Sample.array()).matrix();
                           }
                           else
                           {
                              val_vec = morph_cate.first->GetParameterNominals();
                              for (int i = 0; i < morph_cate.second.size(); i++)
                              {
                                 if (morph_cate.second[i]->Contains(val_vec))
                                 {
                                    Eigen::VectorXd nominal = morph_cate.second[i]->GetMorphingResult(val_vec);
                                    if (UseRelative)
                                       Impact = ((Var.array() / nominal.array() - 1) * SampleNominal.array()).matrix();
                                    else
                                       Impact = Var - nominal;
                                    if (SumImpact)
                                       Profile_Sample = Profile_Sample + Impact;
                                    else
                                       Profile_Sample = (Var.array() / nominal.array() * Profile_Sample.array()).matrix();
                                    morph_cate.first->GetCache()->SaveCache(nom_name, nominal);
                                    break;
                                 }
                              }
                           }
                        }
                     }
                  }

                  if (WithCorr)
                     Profile = Profile + norm * Profile_Sample;
                  else
                  {
                     if (WithStatGamma)
                     {
                        Eigen::VectorXd gamma = norm * samp.second->GetGammaValue();
                        Profile = Profile + (Profile_Sample.array() * gamma.array()).matrix();
                     }
                     else
                     {
                        Profile = Profile + norm * Profile_Sample;
                        Profile_Error2 = Profile_Error2 + pow(norm, 2) * Profile_Error2_Sample;
                     }
                  }
               }
            }
            if (WithCorr)
               prof_map.emplace(std::pair<std::shared_ptr<Observable>, Eigen::VectorXd>(obs.second, Profile));
            else
            {
               if (WithStatGamma)
                  prob = prob + obs.second->GetLikelihood(Profile);
               else
                  prob = prob + obs.second->GetLikelihood_Gaus(Profile, Profile_Error2);
            }
         }
      }

      if (WithCorr)
      {
         for (int i = 0; i < Corr_Cate_Vec->size(); i++)
         {
            std::vector<Eigen::VectorXd> prof_vec;
            for (int j = 0; j < (*Corr_Cate_Vec)[i]->GetNObservable(); j++)
               prof_vec.push_back(prof_map[(*Corr_Cate_Vec)[i]->GetObservable(j)]);
            prob = prob + (*Corr_Cate_Vec)[i]->GetLikelihood(prof_vec);
         }
      }

      if (NCall == 0)
      {
         MSGUser()->MSG_INFO("First -Log(LH): ", -prob);
         Max_Log_Likelihood = -prob;
      }
      else if ((-prob) < Max_Log_Likelihood)
      {
         MSGUser()->MSG_INFO("New Minimum -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
         Max_Log_Likelihood = -prob;
      }
      else if ((NCall + 1) % 1000 == 0)
         MSGUser()->MSG_DEBUG("New -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
      NCall = NCall + 1;
      return -prob;
   }
   else
   {
#ifdef PHOENIX_ENABLE_METAL
      if (WithMetal)
      {
         if (!buff_booked)
         {
            MSGUser()->MSG_INFO("Book Buffer for Metal Boost");
            InitializeBuffer();
            MSGUser()->MSG_INFO("Calculate Cache Parameters");
            InitializeAllCache();
            MSGUser()->MSG_INFO("Fill Buffer for Metal Boost");
            FillBuffer();
            MSGUser()->MSG_INFO("Define ", n_metal_units, " Metal Units for Metal Boost Profile");
         }
         UpdateBuffer(vpara);
         metal_pool->Enqueue(n_metal_units);
         // Start Profile The Samples so that CPU can do something else in the same time

         int index = 0;
         double prob = 0;
         std::shared_ptr<NAGASH::ThreadPool> pool = std::make_shared<NAGASH::ThreadPool>(Fit_Nthread);

         index = 0;
         std::vector<std::future<double>> vfuture_prob;
         for (auto &par : *Par_Map)
         {
            par.second->SetValue(vpara[index]);
            vfuture_prob.emplace_back(pool->enqueue(Parameter::GetLikelihood_Thread, par.second));
            index = index + 1;
         }

         std::vector<std::future<Eigen::VectorXd>> vfuture_profile_err2;
         if (!WithStatGamma)
         {
            for (auto &obs : *Sample_Map)
               vfuture_profile_err2.emplace_back(pool->enqueue(Observable::GetProfileError2_Thread, obs.first, Sample_Map));
         }

         // Now Wait for GPU to Finish the Profile
         metal_pool->Join();
         CollectMetalResult();

         std::vector<std::future<Eigen::VectorXd>> vfuture_sumimpact;
         for (auto &obs : *Sample_Map)
         {
            for (auto &samp : obs.second)
               vfuture_sumimpact.emplace_back(pool->enqueue(PLH_FCN::SumOverMorphingCategory, samp.second, metal_result_map[obs.first][samp.second], SeparatePOI, SumImpact));
         }
         // Calculate all the total impact

         std::vector<std::future<Eigen::VectorXd>> vfuture_sample_profile;
         index = 0;
         for (auto &obs : *Sample_Map)
         {
            for (auto &samp : obs.second)
            {
               vfuture_sample_profile.emplace_back(pool->enqueue(Sample::GetProfile_Thread, samp.second, vfuture_sumimpact[index].get(), SeparatePOI, UseRelative, SumImpact,
                                                                 (*Variation_MorphingRange_Map)[obs.first][samp.second]));
               index = index + 1;
            }
         }

         std::vector<std::future<Eigen::VectorXd>> vfuture_profile;
         index = 0;
         for (auto &obs : *Sample_Map)
         {
            std::vector<Eigen::VectorXd> res_samp;
            for (auto &samp : obs.second)
            {
               res_samp.push_back(vfuture_sample_profile[index].get());
               index = index + 1;
            }
            vfuture_profile.emplace_back(pool->enqueue(PLH_FCN::SumOverSample, obs.first, res_samp));
         }
         // Get the final profiled result for each sample

         std::map<std::shared_ptr<Observable>, Eigen::VectorXd> prof_map;
         index = 0;
         for (auto &obs : *Sample_Map)
         {
            if (WithCorr)
               prof_map.emplace(std::pair<std::shared_ptr<Observable>, Eigen::VectorXd>(obs.first, vfuture_profile[index].get()));
            else
            {
               if (WithStatGamma)
                  vfuture_prob.emplace_back(pool->enqueue(Observable::GetLikelihood_Thread, obs.first, vfuture_profile[index].get()));
               else
                  vfuture_prob.emplace_back(pool->enqueue(Observable::GetLikelihood_Gaus_Thread, obs.first, vfuture_profile[index].get(), vfuture_profile_err2[index].get()));
            }
            index = index + 1;
         }

         if (WithCorr)
         {
            for (int i = 0; i < Corr_Cate_Vec->size(); i++)
            {
               std::vector<Eigen::VectorXd> prof_vec;
               for (int j = 0; j < (*Corr_Cate_Vec)[i]->GetNObservable(); j++)
                  prof_vec.push_back(prof_map[(*Corr_Cate_Vec)[i]->GetObservable(j)]);
               vfuture_prob.emplace_back(pool->enqueue(CorrelationCategory::GetLikelihood_Thread_Vec, (*Corr_Cate_Vec)[i], prof_vec));
            }
         }

         for (int i = 0; i < vfuture_prob.size(); i++)
            prob = prob + vfuture_prob[i].get();

         if (NCall == 0)
         {
            MSGUser()->MSG_INFO("First -Log(LH): ", -prob);
            Max_Log_Likelihood = -prob;
         }
         else if ((-prob) < Max_Log_Likelihood)
         {
            MSGUser()->MSG_INFO("New Minimum -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
            Max_Log_Likelihood = -prob;
         }
         else if ((NCall + 1) % 1000 == 0)
            MSGUser()->MSG_INFO("New -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
         NCall = NCall + 1;
         return -prob;
      }
      else
      {
#endif
         int index = 0;
         double prob = 0;
         std::shared_ptr<NAGASH::ThreadPool> pool = std::make_shared<NAGASH::ThreadPool>(Fit_Nthread);

         index = 0;
         std::vector<std::future<double>> vfuture_prob;
         for (auto &par : *Par_Map)
         {
            par.second->SetValue(vpara[index]);
            vfuture_prob.emplace_back(pool->enqueue(Parameter::GetLikelihood_Thread, par.second));
            index = index + 1;
         }
         std::map<std::shared_ptr<Observable>, Eigen::VectorXd> prof_map;
         std::vector<std::future<Eigen::VectorXd>> vfuture_profile;
         std::vector<std::future<Eigen::VectorXd>> vfuture_profile_err2;
         for (auto &obs : *Obs_Map)
         {
            vfuture_profile.emplace_back(pool->enqueue(Observable::GetProfile_Thread, obs.second, Sample_Map, Variation_MorphingRange_Map, &metal_result_map, WithMetal, UseRelative,
                                                       SumImpact, SeparatePOI));
            if (!WithStatGamma)
               vfuture_profile_err2.emplace_back(pool->enqueue(Observable::GetProfileError2_Thread, obs.second, Sample_Map));
         }

         index = 0;
         for (auto &obs : *Obs_Map)
         {
            if (WithCorr)
               prof_map.emplace(std::pair<std::shared_ptr<Observable>, Eigen::VectorXd>(obs.second, vfuture_profile[index].get()));
            else
            {
               if (WithStatGamma)
                  vfuture_prob.emplace_back(pool->enqueue(Observable::GetLikelihood_Thread, obs.second, vfuture_profile[index].get()));
               else
                  vfuture_prob.emplace_back(pool->enqueue(Observable::GetLikelihood_Gaus_Thread, obs.second, vfuture_profile[index].get(), vfuture_profile_err2[index].get()));
            }
            index = index + 1;
         }

         if (WithCorr)
         {
            for (int i = 0; i < Corr_Cate_Vec->size(); i++)
            {
               std::vector<Eigen::VectorXd> prof_vec;
               for (int j = 0; j < (*Corr_Cate_Vec)[i]->GetNObservable(); j++)
                  prof_vec.push_back(prof_map[(*Corr_Cate_Vec)[i]->GetObservable(j)]);
               vfuture_prob.emplace_back(pool->enqueue(CorrelationCategory::GetLikelihood_Thread_Vec, (*Corr_Cate_Vec)[i], prof_vec));
            }
         }

         for (int i = 0; i < vfuture_prob.size(); i++)
            prob = prob + vfuture_prob[i].get();

         if (NCall == 0)
         {
            MSGUser()->MSG_INFO("First -Log(LH): ", -prob);
            Max_Log_Likelihood = -prob;
         }
         else if ((-prob) < Max_Log_Likelihood)
         {
            MSGUser()->MSG_INFO("New Minimum -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
            Max_Log_Likelihood = -prob;
         }
         else if ((NCall + 1) % 1000 == 0)
            MSGUser()->MSG_INFO("New -Log(LH): ", -prob, " ( NCall = ", NCall + 1, " )");
         NCall = NCall + 1;
         return -prob;
#ifdef PHOENIX_ENABLE_METAL
      }
#endif
   }
}

void PLH_FCN::InitializeBuffer() const
{
#ifdef PHOENIX_ENABLE_METAL
   // Calculate Size
   unsigned int variation_range_par_size = 0;
   unsigned int variation_value_value_size = 0;
   unsigned int variation_value_par_size = 0;
   unsigned int cache_size = 0;
   unsigned int par_size = 0;
   unsigned int common_size = 0;

   for (auto &obs : *Variation_MorphingRange_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
         {
            for (unsigned int i = 0; i < samp.first->GetNBins(); i++)
            {
               common_size = common_size + 1;
               par_size = par_size + cate.first->GetNParameters();
               if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::FULL_LINEAR)
               {
                  cache_size = cache_size + cate.second.size();
                  variation_range_par_size = variation_range_par_size + 2 * cate.second.size();
                  variation_value_par_size = variation_value_par_size + 2 * cate.second.size();
                  variation_value_value_size = variation_value_value_size + 2 * cate.second.size();
               }
               else if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::POL6_EXP)
               {
                  cache_size = cache_size + 8;
                  variation_range_par_size = variation_range_par_size + 2 * cate.second.size();
                  variation_value_par_size = variation_value_par_size + 3 * cate.second.size();
                  variation_value_value_size = variation_value_value_size + 3 * cate.second.size();
               }
               else if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::TRIANGLE_LINEAR)
               {
                  cache_size = cache_size + 3 * cate.second.size();
                  variation_range_par_size = variation_range_par_size + 6 * cate.second.size();
                  variation_value_par_size = variation_value_par_size + 6 * cate.second.size();
                  variation_value_value_size = variation_value_value_size + 3 * cate.second.size();
               }
            }
         }
      }
   }

   par_value_buff = metal_pool->BookBuffer("Par_Value", par_size * sizeof(float), 0, 0);
   par_start_index_buff = metal_pool->BookBuffer("Par_Start_Index", common_size * sizeof(unsigned int), 0, 1);
   morphing_type_buff = metal_pool->BookBuffer("Morphing_Type", common_size * sizeof(unsigned int), 0, 2);
   range_number_buff = metal_pool->BookBuffer("Range_Number", common_size * sizeof(unsigned int), 0, 3);
   variation_range_par_value_buff = metal_pool->BookBuffer("Variation_Range_Par_Value", variation_range_par_size * sizeof(float), 0, 4);
   variation_range_par_start_index_buff = metal_pool->BookBuffer("Variation_Range_Par_Start_Index", common_size * sizeof(unsigned int), 0, 5);
   variation_value_par_value_buff = metal_pool->BookBuffer("Variation_Value_Par_Value", variation_value_par_size * sizeof(float), 0, 6);
   variation_value_par_start_index_buff = metal_pool->BookBuffer("Variation_Value_Par_Start_Index", common_size * sizeof(unsigned int), 0, 7);
   variation_value_value_buff = metal_pool->BookBuffer("Variation_Value_Value", variation_value_value_size * sizeof(float), 0, 8);
   variation_value_value_start_index_buff = metal_pool->BookBuffer("Variation_Value_Value_Start_Index", common_size * sizeof(unsigned int), 0, 9);
   cache_value_buff = metal_pool->BookBuffer("Cache_Value", cache_size * sizeof(float), 0, 10);
   cache_start_index_buff = metal_pool->BookBuffer("Cache_Start_Index", common_size * sizeof(unsigned int), 0, 11);
   base_value_buff = metal_pool->BookBuffer("Base_Value", common_size * sizeof(float), 0, 12);
   userelative_buff = metal_pool->BookBuffer("UseRelative", common_size * sizeof(bool), 0, 13);
   sumimpact_buff = metal_pool->BookBuffer("SumImpact", common_size * sizeof(bool), 0, 14);
   result_buff = metal_pool->BookBuffer("Result", common_size * sizeof(float), 0, 15);
   result_grad_buff = metal_pool->BookBuffer("Result_Grad", par_size * sizeof(float), 0, 16);
   n_metal_units = common_size;
   buff_booked = true;
#endif
}

void PLH_FCN::UpdateBuffer(const std::vector<double> &vpara) const
{
   for (unsigned int i = 0; i < par_index_vec.size(); i++)
   {
      par_value_ptr[i] = (float)(vpara[par_index_vec[i]]);
      result_ptr[i] = 0;
   }
}
void PLH_FCN::UpdateBuffer() const
{
   for (unsigned int i = 0; i < par_ptr_vec.size(); i++)
   {
      par_value_ptr[i] = (float)(par_ptr_vec[i]->GetValue());
      result_ptr[i] = 0;
   }
}

void PLH_FCN::FillBuffer() const
{
#ifdef PHOENIX_ENABLE_METAL
   par_ptr_vec.clear();

   // Fill These Buffers
   par_value_ptr = (float*)par_value_buff->contents();
   par_start_index_ptr = (unsigned int*)par_start_index_buff->contents();
   morphing_type_ptr = (unsigned int*) morphing_type_buff->contents();
   range_number_ptr = (unsigned int*) range_number_buff->contents();
   variation_range_par_value_ptr = (float*) variation_range_par_value_buff->contents();
   variation_range_par_start_index_ptr = (unsigned int*) variation_range_par_start_index_buff->contents();
   variation_value_par_value_ptr = (float*) variation_value_par_value_buff->contents();
   variation_value_par_start_index_ptr = (unsigned int*)variation_value_par_start_index_buff->contents();
   variation_value_value_ptr = (float*)variation_value_value_buff->contents();
   variation_value_value_start_index_ptr = (unsigned int*)variation_value_value_start_index_buff->contents();
   cache_value_ptr = (float*)cache_value_buff->contents();
   cache_start_index_ptr = (unsigned int*)cache_start_index_buff->contents();
   base_value_ptr = (float*)base_value_buff->contents();
   userelative_ptr = (bool*)userelative_buff->contents();
   sumimpact_ptr = (bool*)sumimpact_buff->contents();
   result_ptr = (float*)result_buff->contents();
   result_grad_ptr = (float*)result_grad_buff->contents();

   // All the index needed in the buffer
   unsigned int variation_range_par_index = 0;
   unsigned int variation_value_value_index = 0;
   unsigned int variation_value_par_index = 0;
   unsigned int cache_index = 0;
   unsigned int par_index = 0;
   unsigned int common_index = 0;

   for (auto &obs : *Variation_MorphingRange_Map)
   {
      std::map<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>> empty_secondary;
      std::map<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>>> empty_secondary_grad;
      metal_result_map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>>(obs.first, empty_secondary));
      metal_result_grad_map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>>>>(obs.first,
                                    empty_secondary_grad));
      for (auto &samp : obs.second)
      {
         std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd> empty_secondary_2;
         std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>> empty_secondary_grad_2;
         metal_result_map[obs.first].emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>(samp.first, empty_secondary_2));
         metal_result_grad_map[obs.first].emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>>>(samp.first, empty_secondary_grad_2));
         for (auto &cate : samp.second)
         {
            metal_result_map[obs.first][samp.first].emplace(std::pair<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>(cate.first, Eigen::VectorXd::Zero(samp.first->GetNBins())));
            std::vector<Eigen::VectorXd> empty_grad_vec;
            for (int i = 0; i < cate.first->GetNParameters(); i++)
               empty_grad_vec.push_back(Eigen::VectorXd::Zero(samp.first->GetNBins()));
            metal_result_grad_map[obs.first][samp.first].emplace(std::pair<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>>(cate.first, empty_grad_vec));
            for (unsigned int i = 0; i < samp.first->GetNBins(); i++)
            {
               TString nom_name = TString::Format("%s_%s_Nominal", obs.first->GetName().TString::Data(), samp.first->GetName().TString::Data());
               base_value_ptr[common_index] = (float)((cate.first->GetCache()->GetCache(nom_name))(i));
               userelative_ptr[common_index] = (bool)(UseRelative);
               sumimpact_ptr[common_index] = (bool)(SumImpact);
               binindex_vec.push_back(i);
               samp_ptr_vec.push_back(samp.first);
               obs_ptr_vec.push_back(obs.first);
               cate_ptr_vec.push_back(cate.first);
               for (unsigned int j = 0; j < cate.first->GetNParameters(); j++)
               {
                  par_value_ptr[par_index + j] = (float)(cate.first->GetParameter(j)->GetValue());
                  par_ptr_vec.push_back(cate.first->GetParameter(j));
                  int ipar = 0;
                  for (auto &par_ptr : *Par_Map)
                  {
                     if (par_ptr.second == cate.first->GetParameter(j))
                        break;
                     ipar = ipar + 1;
                  }
                  par_index_vec.push_back(ipar);
               }

               par_start_index_ptr[common_index] = (unsigned int)par_index;
               range_number_ptr[common_index] = (unsigned int)(cate.second.size());
               if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::FULL_LINEAR)
               {
                  morphing_type_ptr[common_index] = 1;
                  for (unsigned int k = 0; k < cate.second.size(); k++)
                  {
                     if (cate.second[k]->RetriveRangeMorphingValue(0) == nullptr)
                        variation_range_par_value_ptr[variation_range_par_index + k * 2] = -999;
                     else
                        variation_range_par_value_ptr[variation_range_par_index + k * 2] = (float)(cate.second[k]->RetriveRangeMorphingValue(0)->GetValue(0));
                     if (cate.second[k]->RetriveRangeMorphingValue(1) == nullptr)
                        variation_range_par_value_ptr[variation_range_par_index + k * 2 + 1] = -999;
                     else
                        variation_range_par_value_ptr[variation_range_par_index + k * 2 + 1] = (float)(cate.second[k]->RetriveRangeMorphingValue(1)->GetValue(0));
                     variation_value_par_value_ptr[variation_value_par_index + k * 2] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetValue(0));
                     variation_value_par_value_ptr[variation_value_par_index + k * 2 + 1] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetValue(0));

                     variation_value_value_ptr[variation_value_value_index + k * 2] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetVariation()->GetValue(i));
                     variation_value_value_ptr[variation_value_value_index + k * 2 + 1] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetVariation()->GetValue(i));

                     cache_value_ptr[cache_index + k] = (float)((cate.second[k]->GetCache()->GetCache("K"))(i));
                  }
                  variation_range_par_start_index_ptr[common_index] = (unsigned int)(variation_range_par_index);
                  variation_value_par_start_index_ptr[common_index] = (unsigned int)(variation_value_par_index);
                  variation_value_value_start_index_ptr[common_index] = (unsigned int)(variation_value_value_index);
                  cache_start_index_ptr[common_index] = (unsigned int)(cache_index);

                  cache_index = cache_index + cate.second.size();
                  variation_range_par_index = variation_range_par_index + 2 * cate.second.size();
                  variation_value_par_index = variation_value_par_index + 2 * cate.second.size();
                  variation_value_value_index = variation_value_value_index + 2 * cate.second.size();
               }
               else if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::POL6_EXP)
               {
                  morphing_type_ptr[common_index] = 2;
                  double lowbase;
                  double A1;
                  double A2;
                  double A3;
                  double A4;
                  double A5;
                  double A6;
                  double highbase;
                  for (unsigned int k = 0; k < cate.second.size(); k++)
                  {
                     if (cate.second[k]->RetriveRangeMorphingValue(0) == nullptr)
                     {
                        variation_range_par_value_ptr[variation_range_par_index + k * 2] = -999;
                        lowbase = (cate.second[k]->GetCache()->GetCache("Base"))(i);
                     }
                     else
                        variation_range_par_value_ptr[variation_range_par_index + k * 2] = (float)(cate.second[k]->RetriveRangeMorphingValue(0)->GetValue(0));
                     if (cate.second[k]->RetriveRangeMorphingValue(1) == nullptr)
                     {
                        variation_range_par_value_ptr[variation_range_par_index + k * 2 + 1] = -999;
                        highbase = (cate.second[k]->GetCache()->GetCache("Base"))(i);
                     }
                     else
                        variation_range_par_value_ptr[variation_range_par_index + k * 2 + 1] = (float)(cate.second[k]->RetriveRangeMorphingValue(1)->GetValue(0));

                     if ((cate.second[k]->RetriveRangeMorphingValue(0) != nullptr) && (cate.second[k]->RetriveRangeMorphingValue(1) != nullptr))
                     {
                        A1 = (cate.second[k]->GetCache()->GetCache("A1"))(i);
                        A2 = (cate.second[k]->GetCache()->GetCache("A2"))(i);
                        A3 = (cate.second[k]->GetCache()->GetCache("A3"))(i);
                        A4 = (cate.second[k]->GetCache()->GetCache("A4"))(i);
                        A5 = (cate.second[k]->GetCache()->GetCache("A5"))(i);
                        A6 = (cate.second[k]->GetCache()->GetCache("A6"))(i);
                     }

                     if (cate.second[k]->RetriveMorphingValue(0) != nullptr)
                     {
                        variation_value_par_value_ptr[variation_value_par_index + k * 3] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetValue(0));
                        variation_value_value_ptr[variation_value_value_index + k * 3] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetVariation()->GetValue(i));
                     }
                     else
                     {
                        variation_value_par_value_ptr[variation_value_par_index + k * 3] = -999;
                        variation_value_value_ptr[variation_value_value_index + k * 3] = -999;
                     }
                     if (cate.second[k]->RetriveMorphingValue(2) != nullptr)
                     {
                        variation_value_par_value_ptr[variation_value_par_index + k * 3 + 2] = (float)(cate.second[k]->RetriveMorphingValue(2)->GetValue(0));
                        variation_value_value_ptr[variation_value_value_index + k * 3 + 2] = (float)(cate.second[k]->RetriveMorphingValue(2)->GetVariation()->GetValue(i));
                     }
                     else
                     {
                        variation_value_par_value_ptr[variation_value_par_index + k * 3 + 2] = -999;
                        variation_value_value_ptr[variation_value_value_index + k * 3 + 2] = -999;
                     }
                     variation_value_par_value_ptr[variation_value_par_index + k * 3 + 1] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetValue(0));
                     variation_value_value_ptr[variation_value_value_index + k * 3 + 1] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetVariation()->GetValue(i));
                  }
                  cache_value_ptr[cache_index] = (float)(lowbase);
                  cache_value_ptr[cache_index + 1] = (float)(A1);
                  cache_value_ptr[cache_index + 2] = (float)(A2);
                  cache_value_ptr[cache_index + 3] = (float)(A3);
                  cache_value_ptr[cache_index + 4] = (float)(A4);
                  cache_value_ptr[cache_index + 5] = (float)(A5);
                  cache_value_ptr[cache_index + 6] = (float)(A6);
                  cache_value_ptr[cache_index + 7] = (float)(highbase);
                  variation_range_par_start_index_ptr[common_index] = (unsigned int)(variation_range_par_index);
                  variation_value_par_start_index_ptr[common_index] = (unsigned int)(variation_value_par_index);
                  variation_value_value_start_index_ptr[common_index] = (unsigned int)(variation_value_value_index);
                  cache_start_index_ptr[common_index] = (unsigned int)(cache_index);

                  cache_index = cache_index + 8;
                  variation_range_par_index = variation_range_par_index + 2 * cate.second.size();
                  variation_value_par_index = variation_value_par_index + 3 * cate.second.size();
                  variation_value_value_index = variation_value_value_index + 3 * cate.second.size();
               }
               else if (cate.first->GetMorphingMode() == Phoenix::MorphingMode::TRIANGLE_LINEAR)
               {
                  morphing_type_ptr[common_index] = 3;
                  for (unsigned int k = 0; k < cate.second.size(); k++)
                  {
                     variation_range_par_value_ptr[variation_range_par_index + k * 6] = (float)(cate.second[k]->RetriveRangeMorphingValue(0)->GetValue(0));
                     variation_range_par_value_ptr[variation_range_par_index + k * 6 + 1] = (float)(cate.second[k]->RetriveRangeMorphingValue(0)->GetValue(1));
                     variation_range_par_value_ptr[variation_range_par_index + k * 6 + 2] = (float)(cate.second[k]->RetriveRangeMorphingValue(1)->GetValue(0));
                     variation_range_par_value_ptr[variation_range_par_index + k * 6 + 3] = (float)(cate.second[k]->RetriveRangeMorphingValue(1)->GetValue(1));
                     variation_range_par_value_ptr[variation_range_par_index + k * 6 + 4] = (float)(cate.second[k]->RetriveRangeMorphingValue(2)->GetValue(0));
                     variation_range_par_value_ptr[variation_range_par_index + k * 6 + 5] = (float)(cate.second[k]->RetriveRangeMorphingValue(2)->GetValue(1));

                     variation_value_par_value_ptr[variation_value_par_index + k * 6] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetValue(0));
                     variation_value_par_value_ptr[variation_value_par_index + k * 6 + 1] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetValue(1));
                     variation_value_par_value_ptr[variation_value_par_index + k * 6 + 2] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetValue(0));
                     variation_value_par_value_ptr[variation_value_par_index + k * 6 + 3] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetValue(1));
                     variation_value_par_value_ptr[variation_value_par_index + k * 6 + 4] = (float)(cate.second[k]->RetriveMorphingValue(2)->GetValue(0));
                     variation_value_par_value_ptr[variation_value_par_index + k * 6 + 5] = (float)(cate.second[k]->RetriveMorphingValue(2)->GetValue(1));

                     variation_value_value_ptr[variation_value_value_index + k * 3] = (float)(cate.second[k]->RetriveMorphingValue(0)->GetVariation()->GetValue(i));
                     variation_value_value_ptr[variation_value_value_index + k * 3 + 1] = (float)(cate.second[k]->RetriveMorphingValue(1)->GetVariation()->GetValue(i));
                     variation_value_value_ptr[variation_value_value_index + k * 3 + 2] = (float)(cate.second[k]->RetriveMorphingValue(2)->GetVariation()->GetValue(i));

                     cache_value_ptr[cache_index + k * 3] = (float)((cate.second[k]->GetCache()->GetCache("A"))(i));
                     cache_value_ptr[cache_index + k * 3 + 1] = (float)((cate.second[k]->GetCache()->GetCache("B"))(i));
                     cache_value_ptr[cache_index + k * 3 + 2] = (float)((cate.second[k]->GetCache()->GetCache("C"))(i));
                  }
                  variation_range_par_start_index_ptr[common_index] = (unsigned int)(variation_range_par_index);
                  variation_value_par_start_index_ptr[common_index] = (unsigned int)(variation_value_par_index);
                  variation_value_value_start_index_ptr[common_index] = (unsigned int)(variation_value_value_index);
                  cache_start_index_ptr[common_index] = (unsigned int)(cache_index);

                  cache_index = cache_index + cate.second.size();
                  variation_range_par_index = variation_range_par_index + 6 * cate.second.size();
                  variation_value_par_index = variation_value_par_index + 6 * cate.second.size();
                  variation_value_value_index = variation_value_value_index + 3 * cate.second.size();
               }

               par_index = par_index + cate.first->GetNParameters();
               common_index = common_index + 1 ;
            }
         }
      }
   }

   for (unsigned int i = 0; i < cache_index; i++)
      if (!isfinite(cache_value_ptr[i]))
         MSGUser()->MSG_ERROR("Abnormal Cache!");
#endif
}

void PLH_FCN::InitializeAllCache() const
{
   for (auto &obs : *Variation_MorphingRange_Map)
   {
      Observable::GetProfile_Thread(obs.first, Sample_Map, Variation_MorphingRange_Map, &metal_result_map, false, UseRelative, SumImpact, SeparatePOI);
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
         {
            for (int i = 0; i < cate.second.size(); i++)
            {
               std::vector<double> center = cate.second[i]->GetCentral();
               cate.second[i]->GetMorphingResult(center);
            }
         }
      }
   }
}

void PLH_FCN::CollectMetalResult() const
{
   for (unsigned int i = 0; i < n_metal_units; i++)
      metal_result_map[obs_ptr_vec[i]][samp_ptr_vec[i]][cate_ptr_vec[i]](binindex_vec[i]) = result_ptr[i];
}
void PLH_FCN::CollectMetalResult_Gradient() const
{
   for (unsigned int i = 0; i < n_metal_units; i++)
      for (unsigned int j = 0; j < cate_ptr_vec[i]->GetNParameters() ; j++)
         metal_result_grad_map[obs_ptr_vec[i]][samp_ptr_vec[i]][cate_ptr_vec[i]][j](binindex_vec[i]) = result_grad_ptr[par_start_index_ptr[i] + j];
}

std::vector<double> PLH_FCN::Gradient(const std::vector<double> &vpara) const
{
   std::vector<double> grad(vpara.size(), 0);

   // Sum -Log(L) Gradient
   // For each sample: (i is the bin index)
   // - Data(i) * TMath::Log(Profiled(i)) + Profiled(i) + TMath::LnGamma(Data(i) + 1.0)
   // Gradient:
   // - Data(i) / Profiled(i) * dProfiled(i) / dpar + dProfiled(i) / dpar
   // If WithStatGamma == false, in this case we use Gauss instead
   // (pow(Data(i) - Profiled(i), 2) / 2 / sigma2) + TMath::Log(sqrt(2 * TMath::Pi()) * sigma)
   // Gradient:
   // - (Data(i) - Profiled(i)) / sigma2 * dProfiled(i) / dpar

   // The gradient of the profiled sample:
   // For each sample:
   // If SumImpact == true && UseRelative == false
   // Profiled(i) = Nominal(i) + Impact(i, par1) + Impact(i, par2) ...
   // dProfiled(i) / dpar = dImpact(i, par) / dpar
   // Else SumImpact == false && UseRelative == true
   // Profiled(i) = Nominal(i) * Impact(i, par1) * Impact(i, par2) ...
   // dProfiled(i) / dpar = Profiled(i) / Impact(i, par) * dImpact(i, par) / dpar
   // Else SumImpact == true && UseRelative == true
   // Profiled(i) = Nominal(i) * (1 + Impact(i, par1) + Impact(i, par2))
   // dProfiled(i) / dpar = Nominal(i) * dImpact(i, par) / dpar

   // If Separate POI
   // If SumImpact == true && UseRelative == false
   // 1D : Profiled = K * par + B = K0 * par + B0 + Impact
   // For POI              : dProfile/dpar = K0
   // For Other Parameter  : dProfile/dpar = dK/dpar * POI + dB/dpar = dB/dpar = dImpact/dpar = dImpact(par)/dpar
   // Since it is SumImpact, K = (Var(1) + Impact - Var(2) - Impact) / (X1 - X2), dK/dpar = 0
   // 2D : Profiled = A * par1 + B * par2 + C = A0 * par1 + B0 * par2 + C0 + Impact
   // For par1             : dProfile/dpar = A0
   // For par2             : dProfile/dpar = B0
   // For Other Parameter  : dProfile/dpar = dA/dpar * POI1 + dB/dpar * POI2 + dC/dpar = dImpact(par)/dpar
   // Similar Situation for High demention

   // If SumImpact == false && UseRelative == true
   // 1D : Profiled = K * par + B = ( K0 * par + B0 ) * Impact
   // For POI              : dProfile/dpar = K0 * Impact
   // For Other Parameter  : dProfile/dpar = dK/dpar * POI + dB/dpar
   // K = (Var(1) - Var(2)) * Impact / (X1 -X2), dK/dpar = K0 * dImpact/dpar
   // Impact * Var(1) = K * X1 + B -> dB/dpar = Var(1) * dImpact/dpar - X1 * dK/dpar
   // = Var(1) * dImpact/dpar - X1 * K0 * dImpact/dpar
   // = (Var(1) - K0 * X1) * dImpact/dpar = B0 * dImpact/dpar
   // dProfile/dpar = K0 * dImpact/dpar * POI + B0 * dImpact/dpar = Profile0 * dImpact/dpar
   // 2D : Profiled = A * par1 + B * par2 + C = ( A0 * par1 + B0 * par2 + C0 ) * Impact
   // For par1             : dProfile/dpar = A0 * Impact
   // For par2             : dProfile/dpar = B0 * Impact
   // For Other Parameter  : dProfile/dpar = Profile0 * dImpact/dpar

   // If SumImpact == true && UseRelative == true
   // 1D : Profile = K * par + B = ( K0 * par + B0 ) * (1 + Impact)
   // For POI              : dProfile/dpar = K0 * (1 + Impact)
   // For Other Parameter  : dProfile/dpar = Profile0 * (1 + dImpact/dpar)
   // 2D : Profiled = A * par1 + B * par2 + C = ( A0 * par1 + B0 * par2 + C0 ) * (1 + Impact)
   // For par1             : dProfile/dpar = A0 * (1 + Impact)
   // For par2             : dProfile/dpar = B0 * (1 + Impact)
   // For Other Parameter  : dProfile/dpar = Profile0 * (1 + dImpact/dpar)

   if (Fit_Nthread == 1)
   {
      // Single Thread Mode
   }
   else
   {
      // Multi Thread Mode
      if (WithMetal)
      {
         // Multi Thread with Metal Boost
         if (!buff_booked)
         {
            MSGUser()->MSG_INFO("Book Buffer for Metal Boost");
            InitializeBuffer();
            MSGUser()->MSG_INFO("Calculate Cache Parameters");
            InitializeAllCache();
            MSGUser()->MSG_INFO("Fill Buffer for Metal Boost");
            FillBuffer();
            MSGUser()->MSG_INFO("Define ", n_metal_units, " Metal Units for Metal Boost Profile");
         }
         UpdateBuffer(vpara);
#ifdef PHOENIX_ENABLE_METAL
         metal_pool->Enqueue(n_metal_units);
#endif
         // Start Profile The Samples so that CPU can do something else in the same time

         int index = 0;
         std::shared_ptr<NAGASH::ThreadPool> pool = std::make_shared<NAGASH::ThreadPool>(Fit_Nthread);

         index = 0;
         std::vector<std::future<double>> vfuture_par_grad;
         for (auto &par : *Par_Map)
         {
            par.second->SetValue(vpara[index]);
            vfuture_par_grad.emplace_back(pool->enqueue(Parameter::GetLikelihood_Gradient_Thread, par.second));
            grad[index] = 0;
            index = index + 1;
         }

         std::vector<std::future<Eigen::VectorXd>> vfuture_err2;
         std::vector<std::future<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>>> vfuture_err2_grad;
         if (!WithStatGamma)
         {
            for (auto &obs : *Sample_Map)
            {
               vfuture_err2.emplace_back(pool->enqueue(Observable::GetProfileError2_Thread, obs.first, Sample_Map));
               vfuture_err2_grad.emplace_back(pool->enqueue(Observable::GetProfileError2_Gradient_Thread, obs.first, Sample_Map));
            }
         }

         // Now Wait for GPU to Finish the Profile
#ifdef PHOENIX_ENABLE_METAL
         metal_pool->Join();
#endif
         CollectMetalResult();

         std::vector<std::future<Eigen::VectorXd>> vfuture_sumimpact;
         for (auto &obs : *Sample_Map)
         {
            for (auto &samp : obs.second)
               vfuture_sumimpact.emplace_back(pool->enqueue(PLH_FCN::SumOverMorphingCategory, samp.second, metal_result_map[obs.first][samp.second], SeparatePOI, SumImpact));
         }
         // Calculate all the total impact

         CollectMetalResult_Gradient();

         std::map<std::shared_ptr<Sample>, Eigen::VectorXd> samp_total_impact_map;
         std::vector<std::future<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>>> vfuture_sumimpact_grad;
         if (SumImpact)
         {
            for (auto &obs : *Sample_Map)
            {
               for (auto &samp : obs.second)
                  vfuture_sumimpact_grad.emplace_back(pool->enqueue(PLH_FCN::SumOverMorphingCategory_Gradient_SumImpact, samp.second, metal_result_grad_map[obs.first][samp.second], SeparatePOI));
            }
            index = 0;
            for (auto &obs : *Sample_Map)
            {
               for (auto &samp : obs.second)
               {
                  samp_total_impact_map.emplace(std::pair<std::shared_ptr<Sample>, Eigen::VectorXd>(samp.second, vfuture_sumimpact[index].get()));
                  index = index + 1;
               }
            }
         }
         else
         {
            index = 0;
            for (auto &obs : *Sample_Map)
            {
               for (auto &samp : obs.second)
               {
                  samp_total_impact_map.emplace(std::pair<std::shared_ptr<Sample>, Eigen::VectorXd>(samp.second, vfuture_sumimpact[index].get()));
                  index = index + 1;
               }
            }
            for (auto &obs : *Sample_Map)
            {
               for (auto &samp : obs.second)
                  vfuture_sumimpact_grad.emplace_back(pool->enqueue(PLH_FCN::SumOverMorphingCategory_Gradient_NotSumImpact, samp.second,
                                                                    metal_result_map[obs.first][samp.second], metal_result_grad_map[obs.first][samp.second], samp_total_impact_map[samp.second], SeparatePOI));
            }
         }

         std::vector<std::future<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>>> vfuture_prof_grad;
         std::vector<std::future<Eigen::VectorXd>> vfuture_prof;

         index = 0;
         for (auto &obs : *Sample_Map)
         {
            for (auto &samp : obs.second)
            {
               vfuture_prof_grad.emplace_back(pool->enqueue(Sample::GetProfile_Gradient_Thread, samp.second, samp_total_impact_map[samp.second], vfuture_sumimpact_grad[index].get(),
                                                            SeparatePOI, UseRelative, SumImpact, (*Variation_MorphingRange_Map)[obs.first][samp.second]));
               vfuture_prof.emplace_back(pool->enqueue(Sample::GetProfile_Thread, samp.second, samp_total_impact_map[samp.second],
                                                       SeparatePOI, UseRelative, SumImpact, (*Variation_MorphingRange_Map)[obs.first][samp.second]));
               index = index + 1;
            }
         }

         std::vector<std::future<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>>> vfuture_obs_grad;
         std::vector<std::future<Eigen::VectorXd>> vfuture_obs;
         index = 0;
         for (auto &obs : *Sample_Map)
         {
            std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> samp_grad_vec;
            std::vector<Eigen::VectorXd> samp_vec;
            for (auto &samp : obs.second)
            {
               samp_grad_vec.push_back(vfuture_prof_grad[index].get());
               samp_vec.push_back(vfuture_prof[index].get());
               index = index + 1;
            }
            vfuture_obs_grad.push_back(pool->enqueue(PLH_FCN::SumOverSample_Gradient, obs.first, samp_grad_vec ));
            vfuture_obs.push_back(pool->enqueue(PLH_FCN::SumOverSample, obs.first, samp_vec));
         }

         std::map<std::shared_ptr<Parameter>, double> l_grad;
         if (WithStatGamma)
         {
            index = 0;
            std::vector<std::future<std::map<std::shared_ptr<Parameter>, double>>> vfuture_subl_grad;
            for (auto &obs : *Sample_Map)
            {
               vfuture_subl_grad.push_back(pool->enqueue(Observable::GetLikelihood_Gradient_Thread, obs.first, vfuture_obs[index].get(), vfuture_obs_grad[index].get()));
               index = index + 1;
            }
            index = 0;
            std::vector<std::map<std::shared_ptr<Parameter>, double>> subl_grad;
            for (auto &obs : *Sample_Map)
            {
               subl_grad.push_back(vfuture_subl_grad[index].get());
               index = index + 1;
            }
            l_grad = PLH_FCN::SumOverObservable_Gradient(subl_grad);
         }
         else
         {
            if (WithCorr)
            {
               index = 0;
               std::map<std::shared_ptr<Observable>, Eigen::VectorXd> prof_map;
               std::map<std::shared_ptr<Observable>, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> prof_grad_map;
               for (auto &obs : *Sample_Map)
               {
                  prof_map.emplace(std::pair<std::shared_ptr<Observable>, Eigen::VectorXd>(obs.first, vfuture_obs[index].get()));
                  prof_grad_map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>>(obs.first, vfuture_obs_grad[index].get()));
                  index = index + 1;
               }
               std::vector<std::future<std::map<std::shared_ptr<Parameter>, double>>> vfuture_subl_grad;
               for (int i = 0; i < Corr_Cate_Vec->size(); i++)
               {
                  std::vector<Eigen::VectorXd> prof_vec;
                  std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> prof_grad_vec;
                  for (int j = 0; j < (*Corr_Cate_Vec)[i]->GetNObservable(); j++)
                  {
                     prof_vec.push_back(prof_map[(*Corr_Cate_Vec)[i]->GetObservable(j)]);
                     prof_grad_vec.push_back(prof_grad_map[(*Corr_Cate_Vec)[i]->GetObservable(j)]);
                  }
                  vfuture_subl_grad.push_back(pool->enqueue(CorrelationCategory::GetLikelihood_Gradient_Thread, (*Corr_Cate_Vec)[i], prof_vec, prof_grad_vec));
               }
               std::vector<std::map<std::shared_ptr<Parameter>, double>> subl_grad;
               for (auto &obs : *Sample_Map)
               {
                  subl_grad.push_back(vfuture_subl_grad[index].get());
                  index = index + 1;
               }
               l_grad = PLH_FCN::SumOverObservable_Gradient(subl_grad);
            }
            else
            {
               index = 0;
               std::vector<std::future<std::map<std::shared_ptr<Parameter>, double>>> vfuture_subl_grad;
               for (auto &obs : *Sample_Map)
               {
                  vfuture_subl_grad.push_back(pool->enqueue(Observable::GetLikelihood_Gaus_Gradient_Thread, obs.first,
                                                            vfuture_obs[index].get(), vfuture_err2[index].get(),
                                                            vfuture_obs_grad[index].get(), vfuture_err2_grad[index].get()));
                  index = index + 1;
               }
               std::vector<std::map<std::shared_ptr<Parameter>, double>> subl_grad;
               for (auto &obs : *Sample_Map)
               {
                  subl_grad.push_back(vfuture_subl_grad[index].get());
                  index = index + 1;
               }
               l_grad = PLH_FCN::SumOverObservable_Gradient(subl_grad);
            }
         }

         index = 0;
         for (auto &par : *Par_Map)
         {
            grad[index] = vfuture_par_grad[index].get();
            auto findpar = l_grad.find(par.second);
            if (findpar != l_grad.end())
               grad[index] = grad[index] + findpar->second;
            std::vector<double> vpara_copy = vpara;
            vpara_copy[index] = vpara_copy[index] + 1e-3;
            double sim_grad = ((*this)(vpara_copy) - (*this)(vpara)) / (1e-3);
            MSGUser()->MSG_INFO(par.second->GetName().TString::Data(), "    ", grad[index], "   ", sim_grad);
            index = index + 1;
         }
      }
      else
      {
         // Multi Thread without Metal Boost
      }
   }

   NCall_Gradient = NCall_Gradient + 1;
   // if (NCall_Gradient % 1000 == 0)
   MSGUser()->MSG_INFO("Call Gradient For ", NCall_Gradient, " Times!");

   return grad;
}

Eigen::VectorXd PLH_FCN::SumOverMorphingCategory(std::shared_ptr<Sample> samp, std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd> r_map, bool SeparatePOI, bool SumImpact)
{
   int nbins = samp->GetNBins();
   Eigen::VectorXd total_impact;
   if (SumImpact)
      total_impact = Eigen::VectorXd::Zero(nbins);
   else
      total_impact = Eigen::VectorXd::Ones(nbins);

   std::shared_ptr<MorphingCategory> center_cate = nullptr;

   if (SeparatePOI)
   {
      bool findcentral = false;
      for (auto &cate : r_map)
      {
         if ((cate.first->ContainsPOI()) && (findcentral == false))
         {
            findcentral = true;
            center_cate = cate.first;
         }
         else if ((cate.first->ContainsPOI()) && (findcentral == true))
         {
            std::cout << "Already found 1 morphing category contains POI! Cannot Separate POI with NP when POIs are separated in different categories." << std::endl;
            return total_impact;
         }
      }
   }

   for (auto &cate : r_map)
   {
      if (cate.first == center_cate)
         continue;
      if (SumImpact)
         total_impact = total_impact + cate.second;
      else
         total_impact = (total_impact.array() * cate.second.array() ).matrix();
   }

   return total_impact;
}

Eigen::VectorXd PLH_FCN::SumOverSample(std::shared_ptr<Observable> obs, std::vector<Eigen::VectorXd> v_samp )
{
   Eigen::VectorXd prof = Eigen::VectorXd::Zero(obs->GetNBins());

   for (int i = 0; i < v_samp.size(); i++)
      prof = prof + v_samp[i];

   return prof;
}
std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> PLH_FCN::SumOverMorphingCategory_Gradient_NotSumImpact(std::shared_ptr<Sample> samp,
                                                                                                             std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd> r_map,
                                                                                                             std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>> r_grad_map,
                                                                                                             Eigen::VectorXd total_impact, bool SeparatePOI)
{
   int nbins = samp->GetNBins();
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> total_impact_grad;

   std::shared_ptr<MorphingCategory> center_cate = nullptr;

   if (SeparatePOI)
   {
      bool findcentral = false;
      for (auto &cate : r_grad_map)
      {
         if ((cate.first->ContainsPOI()) && (findcentral == false))
         {
            findcentral = true;
            center_cate = cate.first;
         }
         else if ((cate.first->ContainsPOI()) && (findcentral == true))
         {
            std::cout << "Already found 1 morphing category contains POI! Cannot Separate POI with NP when POIs are separated in different categories." << std::endl;
            return total_impact_grad;
         }
      }
   }
   for (auto &cate : r_grad_map)
   {
      if (cate.first == center_cate)
         continue;
      for (int i = 0; i < cate.first->GetNParameters(); i++)
         total_impact_grad.emplace(cate.first->GetParameter(i), (total_impact.array() / (r_map[cate.first].array()) * cate.second[i].array() ).matrix());
   }

   return total_impact_grad;
}
std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> PLH_FCN::SumOverMorphingCategory_Gradient_SumImpact(std::shared_ptr<Sample> samp,
                                                                                                          std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>> r_grad_map,
                                                                                                          bool SeparatePOI)
{
   int nbins = samp->GetNBins();
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> total_impact_grad;

   std::shared_ptr<MorphingCategory> center_cate = nullptr;

   if (SeparatePOI)
   {
      bool findcentral = false;
      for (auto &cate : r_grad_map)
      {
         if ((cate.first->ContainsPOI()) && (findcentral == false))
         {
            findcentral = true;
            center_cate = cate.first;
         }
         else if ((cate.first->ContainsPOI()) && (findcentral == true))
         {
            std::cout << "Already found 1 morphing category contains POI! Cannot Separate POI with NP when POIs are separated in different categories." << std::endl;
            return total_impact_grad;
         }
      }
   }
   for (auto &cate : r_grad_map)
   {
      if (cate.first == center_cate)
         continue;
      for (int i = 0; i < cate.first->GetNParameters(); i++)
         total_impact_grad.emplace(cate.first->GetParameter(i), cate.second[i]);
   }

   return total_impact_grad;
}
std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> PLH_FCN::SumOverSample_Gradient(std::shared_ptr<Observable> obs, std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> v_samp_grad )
{
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad;

   for (int i = 0; i < v_samp_grad.size(); i++)
   {
      for (auto &par : v_samp_grad[i])
      {
         auto findpar = prof_grad.find(par.first);
         if (findpar == prof_grad.end())
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(par.first, par.second) );
         else
            findpar->second = findpar->second + par.second;
      }
   }

   return prof_grad;
}
std::map<std::shared_ptr<Parameter>, double> PLH_FCN::SumOverObservable_Gradient(std::vector<std::map<std::shared_ptr<Parameter>, double>> v_obs_grad)
{
   std::map<std::shared_ptr<Parameter>, double> lgrad;
   for (int i = 0; i < v_obs_grad.size(); i++)
   {
      for (auto &par : v_obs_grad[i])
      {
         auto findpar = lgrad.find(par.first);
         if (findpar == lgrad.end())
            lgrad.emplace(std::pair<std::shared_ptr<Parameter>, double>(par.first, par.second));
         else
            findpar->second = findpar->second + par.second;
      }
   }
   return lgrad;
}
