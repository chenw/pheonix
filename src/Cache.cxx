#ifndef PHOENIX_CACHE_SOURCE
#define PHOENIX_CACHE_SOURCE

#include "Phoenix/Cache.h"

using namespace Phoenix;

std::vector<std::shared_ptr<MorphingCategory>> ProfileRecord::cate_vec;

ProfileRecord::ProfileRecord(std::map<TString, std::shared_ptr<Parameter>>* par_map,
                             std::map<std::shared_ptr<Observable>,
                             std::map<std::shared_ptr<Sample>,
                             std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> r_map)
{
   for (auto &par : *par_map)
      par_value_map.emplace(std::pair<std::shared_ptr<Parameter>, double>(par.second, par.second->GetValue()));
   result_map = r_map;
   if (cate_vec.size() == 0)
   {
      for (auto &obs : r_map)
      {
         for (auto &samp : obs.second)
         {
            for (auto &cate : samp.second)
            {
               bool found = false;

               if (cate_vec.size() > 0)
                  for (int i = 0; i < cate_vec.size(); i++)
                     if (cate_vec[i] == cate.first)
                     {
                        found = true;
                        break;
                     }
               if (!found)
                  cate_vec.push_back(cate.first);
            }
         }
      }
   }
}
#endif
