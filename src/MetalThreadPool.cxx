#ifndef METALTHREADPOOL_SOURCE
#define METALTHREADPOOL_SOURCE

#ifdef PHOENIX_ENABLE_METAL
#include "MetalThreadPool.h"
#include <SingleHeader/Metal.hpp>

MetalThreadPool::MetalThreadPool(std::shared_ptr<NAGASH::MSGTool> msg)
{
   m_pool = NS::AutoreleasePool::alloc()->init();
   m_device = MTL::CreateSystemDefaultDevice();
   m_msg = msg;
}
MetalThreadPool::~MetalThreadPool()
{
}

void MetalThreadPool::LoadFunction(TString filename, TString funcname)
{

   auto libpath = NS::String::string(filename.TString::Data(), NS::ASCIIStringEncoding);
   auto m_library = m_device->newLibrary(libpath, &m_error);
   if (!m_library)
      MSGUser()->MSG_ERROR("Failed to find the library: ", filename.TString::Data());

   auto functionname = NS::String::string(funcname.TString::Data(), NS::ASCIIStringEncoding);
   auto m_function = m_library->newFunction(functionname);
   if (!m_function)
      MSGUser()->MSG_ERROR("Failed to find the function: ", funcname.TString::Data());

   m_functionPSO = m_device->newComputePipelineState(m_function, &m_error);
   if (!m_functionPSO)
      MSGUser()->MSG_ERROR("Failed to creat the pipeline state object!");

   m_queue = m_device->newCommandQueue();
   if (!m_queue)
      MSGUser()->MSG_ERROR("Failed to creat the command queue!");
}
MTL::Buffer* MetalThreadPool::BookBuffer(TString name, int size, int offset, int post)
{
   MTL::Buffer* newbuffer = m_device->newBuffer(size, MTL::ResourceStorageModeShared);
   m_Buffer_map.emplace(std::pair<TString, MTL::Buffer*>(name, newbuffer));
   m_Buffer_Offset_map.emplace(std::pair<TString, int>(name, offset));
   m_Buffer_Position_map.emplace(std::pair<TString, int>(name, post));
   m_Buffer_Size_map.emplace(std::pair<TString, int>(name, size));
   return newbuffer;
}
MTL::Buffer* MetalThreadPool::GetBuffer(TString name)
{
   auto findbuffer = m_Buffer_map.find(name);
   if (findbuffer != m_Buffer_map.end())
      return findbuffer->second;
   return nullptr;
}

void MetalThreadPool::Enqueue(int nth)
{
   // Create a command buffer to hold commands.
   m_commandbuffer = m_queue->commandBuffer();
   assert(m_commandbuffer != nullptr);

   // Start a compute pass.
   MTL::ComputeCommandEncoder* m_encoder = m_commandbuffer->computeCommandEncoder();
   assert(m_encoder != nullptr);

   // Encode the pipeline state object and its parameters.
   m_encoder->setComputePipelineState(m_functionPSO);
   for (auto ibuffer : m_Buffer_map)
      m_encoder->setBuffer(ibuffer.second, m_Buffer_Offset_map[ibuffer.first], m_Buffer_Position_map[ibuffer.first]);

   MTL::Size gridSize = MTL::Size(nth, 1, 1);

   // Calculate a threadgroup size.
   NS::UInteger threadGroupSize = m_functionPSO->maxTotalThreadsPerThreadgroup();
   if (threadGroupSize > nth)
      threadGroupSize = nth;
   MTL::Size threadgroupSize = MTL::Size(threadGroupSize, 1, 1);

   // Encode the compute command.
   m_encoder->dispatchThreads(gridSize, threadgroupSize);

   // End the compute pass.
   m_encoder->endEncoding();

   // Execute the command.
   m_commandbuffer->commit();
}
#endif 

#endif
