#include "Phoenix/Fitter.h"
#include "Phoenix/PLH.h"
#include "Phoenix/Morphing2D.h"

#include "Minuit2/FCNBase.h"
#include "Minuit2/FCNGradientBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnSimplex.h"
#include "Minuit2/MnHesse.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnScan.h"
#include "Minuit2/MnContours.h"
#include "Minuit2/MnPlot.h"

using namespace Phoenix;
using namespace std;

Fitter::Fitter(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> config) : Tool(MSG)
{
   FIT_RANGE_STEP = config->GetPar<double>("FIT_RANGE_STEP");
   FIT_MAX_NUISANCE = config->GetPar<double>("FIT_MAX_NUISANCE");
   FIT_NEED_STAT_GAMMA = config->GetPar<bool>("FIT_NEED_STAT_GAMMA");
   FIT_USE_RELATIVE_IMPACT = config->GetPar<bool>("FIT_USE_RELATIVE_IMPACT");
   FIT_SUM_IMPACT = config->GetPar<bool>("FIT_SUM_IMPACT");
   FIT_SEPARATE_POI = config->GetPar<bool>("FIT_SEPARATE_POI");
   FIT_RUN_MINOS = config->GetPar<bool>("FIT_RUN_MINOS");
   FIT_ENABLE_SPEEDUP = config->GetPar<bool>("FIT_ENABLE_SPEEDUP");
   FIT_ENABLE_METAL_BOOST = config->GetPar<bool>("FIT_ENABLE_METAL_BOOST");
   FIT_NTHREAD = config->GetPar<int>("FIT_NTHREAD");
}

int Fitter::GetNParameters()
{
   return Par_Map.size();
}

std::shared_ptr<Parameter> Fitter::GetParameter(int index)
{
   if (index < 0 || index >= GetNParameters())
      return nullptr;
   int count = 0;
   for (auto &par : Par_Map)
   {
      if (count == index)
         return par.second;
      count = count + 1;
   }
   return nullptr;
}

void Fitter::FixParameter(TString name, double val)
{
   std::shared_ptr<Parameter> par = GetParameter(name);
   par->Fix();
   par->SetValue(val);
}

std::shared_ptr<Parameter> Fitter::BookParameter(TString name, double norm, double min, double max, ParameterType type)
{
   auto findpar = Par_Map.find(name);
   if (findpar != Par_Map.end())
   {
      MSGUser()->MSG_WARNING("This Parameter (", name.TString::Data(), ") has alread been defined! The original pointer is returned.");
      return findpar->second;
   }
   else
   {
      if (max <= min)
      {
         MSGUser()->MSG_ERROR("The region for Parameter (", name.TString::Data(), ") is not defined correctly! Return nullptr.");
         return nullptr;
      }
      if (norm <= min || norm >= max)
      {
         MSGUser()->MSG_ERROR("The nominal value for Parameter (", name.TString::Data(), ") is not defined within its region! Return nullptr.");
         return nullptr;
      }
      std::shared_ptr<Parameter> newpar = std::make_shared<Parameter>(name, norm, min, max, type);
      Par_Map.emplace(std::pair<TString, std::shared_ptr<Parameter>>(name, newpar));
      return newpar;
   }
}

std::shared_ptr<Parameter> Fitter::BookNP(TString name)
{
   return BookNuisanceParameter(name);
}

std::shared_ptr<Parameter> Fitter::BookNuisanceParameter(TString name)
{
   return BookParameter(name, 0, -fabs(FIT_MAX_NUISANCE), fabs(FIT_MAX_NUISANCE), ParameterType::Nuisance);
}

std::shared_ptr<Parameter> Fitter::BookPOI(TString name, double norm, double min, double max)
{
   return BookParameterOfInterest(name, norm, min, max);
}

std::shared_ptr<Parameter> Fitter::BookParameterOfInterest(TString name, double norm, double min, double max)
{
   return BookParameter(name, norm, min, max, ParameterType::Interest);
}

std::shared_ptr<Parameter> Fitter::BookNormalizationParameter(TString name, double norm, double min, double max)
{
   return BookParameter(name, norm, min, max, ParameterType::Normalize);
}

void Fitter::LinkNormalizationParameterToSample(TString pname, TString obsname, TString samplename)
{
   std::shared_ptr<Parameter> par = GetParameter(pname);
   if (par->GetType() != ParameterType::Normalize)
      return;

   std::shared_ptr<Sample> samp = GetSample(obsname, samplename);
   samp->SetNormalizationParameter(par);
}

void Fitter::LinkNormalizationParameterToSample(TString pname, TString samplename)
{
   std::shared_ptr<Parameter> par = GetParameter(pname);
   if (par->GetType() != ParameterType::Normalize)
      return;

   for (auto &obs : Sample_Map)
   {
      for (auto &samp : obs.second)
      {
         if (samp.first.TString::EqualTo(samplename))
            samp.second->SetNormalizationParameter(par);
      }
   }
}

void Fitter::SetGammaPrefix(TString prefix, TString obsname, TString samplename)
{
   std::shared_ptr<Sample> samp = GetSample(obsname, samplename);
   samp->SetGammaPrefix(prefix);
}

void Fitter::SetGammaPrefix(TString prefix, TString samplename)
{
   for (auto &obs : Sample_Map)
   {
      for (auto &samp : obs.second)
      {
         if (samp.first.TString::EqualTo(samplename))
            samp.second->SetGammaPrefix(prefix);
      }
   }
}

std::shared_ptr<Parameter> Fitter::GetParameter(TString name)
{
   auto findpar = Par_Map.find(name);
   if (findpar != Par_Map.end())
      return findpar->second;
   else
   {
      MSGUser()->MSG_ERROR("This Parameter (", name.TString::Data(), ") is not defined! Return nullptr.");
      return nullptr;
   }
}

std::shared_ptr<MorphingCategory> Fitter::GetMorphingCategory(TString name)
{
   auto findgroup = Morph_Map.find(name);
   if (findgroup != Morph_Map.end())
      return findgroup->second;
   else
   {
      MSGUser()->MSG_ERROR("This MorphingCategory (", name.TString::Data(), ") is not defined! Return nullptr.");
      return nullptr;
   }
}

std::shared_ptr<MorphingCategory> Fitter::BookMorphingCategory(TString name)
{
   auto findgroup = Morph_Map.find(name);
   if (findgroup != Morph_Map.end())
   {
      MSGUser()->MSG_WARNING("This MorphingCategory (", name.TString::Data(), ") has alread been defined! The original pointer is returned.");
      return findgroup->second;
   }
   else
   {
      std::shared_ptr<MorphingCategory> newgroup = std::make_shared<MorphingCategory>(name);
      Morph_Map.emplace(std::pair<TString, std::shared_ptr<MorphingCategory>>(name, newgroup));
      return newgroup;
   }
}

void Fitter::AddParameterToMorphingCategory(TString pname, TString gname)
{
   std::shared_ptr<Parameter> par = GetParameter(pname);
   std::shared_ptr<MorphingCategory> group = GetMorphingCategory(gname);

   if (par == nullptr || group == nullptr)
      return;
   else
      group->AddParameter(par);
}

void Fitter::InitializeParameters()
{
   MSGUser()->MSG_INFO("//-----------------------//");
   MSGUser()->MSG_INFO("// Initialize Parameters //");
   MSGUser()->MSG_INFO("//-----------------------//");

   // Book Morphing Category
   std::map<TString, std::shared_ptr<Parameter>> Temp_Par_Map = Par_Map;

   for (auto &group : Morph_Map)
   {
      if (group.second->GetNParameters() > 0)
      {
         for (int i = 0; i < group.second->GetNParameters(); i++)
         {
            TString name = group.second->GetParameter(i)->GetName();
            auto findpar = Temp_Par_Map.find(name);
            if (findpar != Temp_Par_Map.end())
               Temp_Par_Map.erase(name);
         }
      }
   }

   for (auto &par : Temp_Par_Map)
   {
      if (par.second->GetType() == ParameterType::Normalize)
         continue;
      std::shared_ptr<MorphingCategory> group = BookMorphingCategory(par.first);
      group->AddParameter(par.second);
   }

   MSGUser()->MSG_INFO("");
   MSGUser()->MSG_INFO("Parameter List");
   int index = 0;
   for (auto &group : Morph_Map)
   {
      index++;
      group.second->ConfigMorphingMode();
      MSGUser()->MSG_INFO("Group No.", index, ": ", group.second->GetName().TString::Data());
      for (int i = 0; i < group.second->GetNParameters(); i++)
         MSGUser()->MSG_INFO("   -Parameter No.", i, ": ", group.second->GetParameter(i)->GetName().TString::Data());
   }
   MSGUser()->MSG_INFO("");
   MSGUser()->MSG_INFO("//------------------------//");
   MSGUser()->MSG_INFO("// Parameters Initialized //");
   MSGUser()->MSG_INFO("//------------------------//");
}

std::shared_ptr<Observable> Fitter::GetObservable(TString name)
{
   auto findobs = Obs_Map.find(name);
   if (findobs != Obs_Map.end())
      return findobs->second;
   else
   {
      MSGUser()->MSG_ERROR("This Observable (", name.TString::Data(), ") is not defined! Return nullptr.");
      return nullptr;
   }
}
std::shared_ptr<Observable> Fitter::BookObservable(TString name, const std::vector<double> &val_vec, const std::vector<double> &err_vec)
{
   if (val_vec.size() != err_vec.size())
   {
      MSGUser()->MSG_ERROR("Inconsistent number of central value and error for Observable ", name.TString::Data(), "! Return nullptr");
      return nullptr;
   }
   auto findobs = Obs_Map.find(name);
   if (findobs != Obs_Map.end())
   {
      MSGUser()->MSG_WARNING("This Observable (", name.TString::Data(), ") has alread been defined! The original pointer is returned.");
      return findobs->second;
   }
   else
   {
      std::shared_ptr<Observable> obs = std::make_shared<Observable>(name, val_vec, err_vec);
      Obs_Map.emplace(std::pair<TString, std::shared_ptr<Observable>>(name, obs));
      return obs;
   }
}

std::shared_ptr<Observable> Fitter::BookObservable(TString name, TString fname, TString hname, int start, int end, int rebin)
{
   std::shared_ptr<NAGASH::TFileHelper> fhelper = std::make_shared<NAGASH::TFileHelper>(MSGUser(), fname.TString::Data());
   std::shared_ptr<Observable> obs_ptr = BookObservable(name, fhelper, hname, start, end, rebin);
   fhelper->Close();
   return obs_ptr;
}
std::shared_ptr<Observable> Fitter::BookObservable(TString name, std::shared_ptr<NAGASH::TFileHelper> fptr, TString hname, int start, int end, int rebin)
{
   TH1* hptr = fptr->Get(hname.TString::Data());
   return BookObservable(name, fptr, start, end, rebin);
}
std::shared_ptr<Observable> Fitter::BookObservable(TString name, TH1* hptr, int start, int end, int rebin)
{
   TH1* temp = (TH1*)hptr->Clone("_temp_");
   if (rebin > 1)
      temp->Rebin(rebin);
   std::vector<double> val_vec;
   std::vector<double> err_vec;
   int start_index = 1;
   int end_index = temp->GetXaxis()->GetNbins();
   if (start > 0)
      start_index = start;
   if (end > 0)
      end_index = end;
   if (start_index <= end_index)
   {
      for (int i = start_index; i <= end_index; i++)
      {
         val_vec.push_back(temp->GetBinContent(i));
         err_vec.push_back(temp->GetBinError(i));
      }
   }
   std::shared_ptr<Observable> obs_ptr = BookObservable(name, val_vec, err_vec);
   delete temp;
   return obs_ptr;
}
std::shared_ptr<Sample> Fitter::GetSample(TString obsname, TString samplename)
{
   std::shared_ptr<Observable> obs = GetObservable(obsname);

   auto findmap = Sample_Map.find(obs);
   if (findmap != Sample_Map.end())
   {
      auto findsample = findmap->second.find(samplename);
      if (findsample != findmap->second.end())
         return findsample->second;
      else
      {
         MSGUser()->MSG_ERROR("Sample ", samplename.TString::Data(), " is not booked for Observable ", obsname.TString::Data(), "! Return nullptr");
         return nullptr;
      }
   }
   else
   {
      MSGUser()->MSG_ERROR("No sample is booked for Observable ", obsname.TString::Data(), "! Return nullptr");
      return nullptr;
   }
}

std::shared_ptr<Sample> Fitter::BookSample(TString obsname, TString samplename, const std::vector<double> &val_vec, const std::vector<double> &err_vec)
{
   if (val_vec.size() != err_vec.size())
   {
      MSGUser()->MSG_ERROR("Inconsistent number of central value and error for Sample ", samplename.TString::Data(), "! Return nullptr");
      return nullptr;
   }

   std::shared_ptr<Observable> obs = GetObservable(obsname);

   if (obs == nullptr)
   {
      MSGUser()->MSG_ERROR("Observable ", obsname.TString::Data(), " is not defined! Cannot define its Sample first! Return nullptr");
      return nullptr;
   }

   auto findmap = Sample_Map.find(obs);
   if (findmap != Sample_Map.end())
   {
      auto findsample = findmap->second.find(samplename);
      if (findsample != findmap->second.end())
      {
         MSGUser()->MSG_ERROR("Sample ", samplename.TString::Data(), " has been already booked for Observable ", obsname.TString::Data(), "! Return the original pointer");
         return findsample->second;
      }
      else
      {
         std::shared_ptr<Sample> samp = std::make_shared<Sample>(samplename, obs, val_vec, err_vec);
         findmap->second.emplace(std::pair<TString, std::shared_ptr<Sample>>(samplename, samp));
         return samp;
      }
   }
   else
   {
      std::shared_ptr<Sample> samp = std::make_shared<Sample>(samplename, obs, val_vec, err_vec);
      std::map<TString, std::shared_ptr<Sample>> map_sample;
      map_sample.emplace(std::pair<TString, std::shared_ptr<Sample>>(samplename, samp));
      Sample_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>(obs, map_sample));
      return samp;
   }
}
std::shared_ptr<Sample> Fitter::BookSample(TString obsname, TString samplename, TH1* hptr, int start, int end, int rebin)
{
   TH1* temp = (TH1*)hptr->Clone("_temp_");
   if (rebin > 1)
      temp->Rebin(rebin);
   std::vector<double> val_vec;
   std::vector<double> err_vec;
   int start_index = 1;
   int end_index = temp->GetXaxis()->GetNbins();
   if (start > 0)
      start_index = start;
   if (end > 0)
      end_index = end;
   if (start_index <= end_index)
   {
      for (int i = start_index; i <= end_index; i++)
      {
         val_vec.push_back(temp->GetBinContent(i));
         err_vec.push_back(temp->GetBinError(i));
      }
   }
   std::shared_ptr<Sample> samp_ptr = BookSample(obsname, samplename, val_vec, err_vec);
   delete temp;
   return samp_ptr;
}
std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, const std::vector<double> &morph_vec, TH1* hptr, int start, int end, int rebin)
{
   TH1* temp = (TH1*)hptr->Clone("_temp_");
   if (rebin > 1)
      temp->Rebin(rebin);
   std::vector<double> val_vec;
   std::vector<double> err_vec;
   int start_index = 1;
   int end_index = temp->GetXaxis()->GetNbins();
   if (start > 0)
      start_index = start;
   if (end > 0)
      end_index = end;
   if (start_index <= end_index)
   {
      for (int i = start_index; i <= end_index; i++)
      {
         val_vec.push_back(temp->GetBinContent(i));
         err_vec.push_back(temp->GetBinError(i));
      }
   }
   std::shared_ptr<Variation> var_ptr = BookVariation(obsname, samplename, gname, morph_vec, val_vec, err_vec);
   delete temp;
   return var_ptr;
}

std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, const std::vector<double> &morph_vec, const std::vector<double> &val_vec,
                                                 const std::vector<double> &err_vec)
{
   std::shared_ptr<Observable> obs = GetObservable(obsname);
   std::shared_ptr<MorphingCategory> morph_cate = GetMorphingCategory(gname);
   std::shared_ptr<Sample> samp = GetSample(obsname, samplename);

   std::shared_ptr<Variation> var = std::make_shared<Variation>(obs, samp, morph_cate, val_vec, err_vec);
   std::shared_ptr<MorphingValue> val = std::make_shared<MorphingValue>(morph_vec, var, morph_cate);

   auto findobs = Variation_Map.find(obs);
   if (findobs != Variation_Map.end())
   {
      auto findsample = findobs->second.find(samp);
      if (findsample != findobs->second.end())
      {
         auto findmorph = findsample->second.find(morph_cate);
         if (findmorph != findsample->second.end())
            findmorph->second.push_back(var);
         else
         {
            std::vector<std::shared_ptr<Variation>> empty_var_vec;
            empty_var_vec.push_back(var);
            findsample->second.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                                       std::vector<std::shared_ptr<Variation>>>(morph_cate, empty_var_vec));
         }
      }
      else
      {
         std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<Variation>>>
         empty_morph_map;
         std::vector<std::shared_ptr<Variation>> empty_var_vec;
         empty_var_vec.push_back(var);
         empty_morph_map.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                                 std::vector<std::shared_ptr<Variation>>>(morph_cate, empty_var_vec));
         findobs->second.emplace(std::pair<std::shared_ptr<Sample>,
                                 std::map<std::shared_ptr<MorphingCategory>,
                                 std::vector<std::shared_ptr<Variation>>>>(samp, empty_morph_map));
      }
   }
   else
   {
      std::map<std::shared_ptr<Sample>,
          std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<Variation>>>>
          empty_sample_map;
      std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<Variation>>>
      empty_morph_map;
      std::vector<std::shared_ptr<Variation>> empty_var_vec;
      empty_var_vec.push_back(var);
      empty_morph_map.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                              std::vector<std::shared_ptr<Variation>>>(morph_cate, empty_var_vec));
      empty_sample_map.emplace(std::pair<std::shared_ptr<Sample>,
                               std::map<std::shared_ptr<MorphingCategory>,
                               std::vector<std::shared_ptr<Variation>>>>(samp, empty_morph_map));
      Variation_Map.emplace(std::pair<std::shared_ptr<Observable>,
                            std::map<std::shared_ptr<Sample>,
                            std::map<std::shared_ptr<MorphingCategory>,
                            std::vector<std::shared_ptr<Variation>>>>>(obs, empty_sample_map));
   }

   auto findobs2 = Variation_Sigma_Map.find(obs);
   if (findobs2 != Variation_Sigma_Map.end())
   {
      auto findsample = findobs2->second.find(samp);
      if (findsample != findobs2->second.end())
      {
         auto findmorph = findsample->second.find(morph_cate);
         if (findmorph != findsample->second.end())
            findmorph->second.push_back(val);
         else
         {
            std::vector<std::shared_ptr<MorphingValue>> empty_val_vec;
            empty_val_vec.push_back(val);
            findsample->second.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                                       std::vector<std::shared_ptr<MorphingValue>>>(morph_cate, empty_val_vec));
         }
      }
      else
      {
         std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>
         empty_morph_map;
         std::vector<std::shared_ptr<MorphingValue>> empty_val_vec;
         empty_val_vec.push_back(val);
         empty_morph_map.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                                 std::vector<std::shared_ptr<MorphingValue>>>(morph_cate, empty_val_vec));
         findobs2->second.emplace(std::pair<std::shared_ptr<Sample>,
                                  std::map<std::shared_ptr<MorphingCategory>,
                                  std::vector<std::shared_ptr<MorphingValue>>>>(samp, empty_morph_map));
      }
   }
   else
   {
      std::map<std::shared_ptr<Sample>,
          std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>>
          empty_sample_map;
      std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>
      empty_morph_map;
      std::vector<std::shared_ptr<MorphingValue>> empty_val_vec;
      empty_val_vec.push_back(val);
      empty_morph_map.emplace(std::pair<std::shared_ptr<MorphingCategory>,
                              std::vector<std::shared_ptr<MorphingValue>>>(morph_cate, empty_val_vec));
      empty_sample_map.emplace(std::pair<std::shared_ptr<Sample>,
                               std::map<std::shared_ptr<MorphingCategory>,
                               std::vector<std::shared_ptr<MorphingValue>>>>(samp, empty_morph_map));
      Variation_Sigma_Map.emplace(std::pair<std::shared_ptr<Observable>,
                                  std::map<std::shared_ptr<Sample>,
                                  std::map<std::shared_ptr<MorphingCategory>,
                                  std::vector<std::shared_ptr<MorphingValue>>>>>(obs, empty_sample_map));
   }

   return var;
}

std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, double morph_val, const std::vector<double> &val_vec, const std::vector<double> &err_vec)
{
   std::vector<double> morph_vec;
   morph_vec.push_back(morph_val);
   return BookVariation(obsname, samplename, gname, morph_vec, val_vec, err_vec);
}
std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, double morph_val, TH1* hptr, int start, int end, int rebin)
{
   TH1* temp = (TH1*)hptr->Clone("_temp_");
   if (rebin > 1)
      temp->Rebin(rebin);
   std::vector<double> val_vec;
   std::vector<double> err_vec;
   int start_index = 1;
   int end_index = temp->GetXaxis()->GetNbins();
   if (start > 0)
      start_index = start;
   if (end > 0)
      end_index = end;
   if (start_index <= end_index)
   {
      for (int i = start_index; i <= end_index; i++)
      {
         val_vec.push_back(temp->GetBinContent(i));
         err_vec.push_back(temp->GetBinError(i));
      }
   }
   std::shared_ptr<Variation> var_ptr = BookVariation(obsname, samplename, gname, morph_val, val_vec, err_vec);
   delete temp;
   return var_ptr;
}

std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, double morph_val1, double morph_val2, const std::vector<double> &val_vec,
                                                 const std::vector<double> &err_vec)
{
   std::vector<double> morph_vec;
   morph_vec.push_back(morph_val1);
   morph_vec.push_back(morph_val2);
   return BookVariation(obsname, samplename, gname, morph_vec, val_vec, err_vec);
}
std::shared_ptr<Variation> Fitter::BookVariation(TString obsname, TString samplename, TString gname, double morph_val1, double morph_val2, TH1* hptr, int start, int end, int rebin)
{
   TH1* temp = (TH1*)hptr->Clone("_temp_");
   if (rebin > 1)
      temp->Rebin(rebin);
   std::vector<double> val_vec;
   std::vector<double> err_vec;
   int start_index = 1;
   int end_index = temp->GetXaxis()->GetNbins();
   if (start > 0)
      start_index = start;
   if (end > 0)
      end_index = end;
   if (start_index <= end_index)
   {
      for (int i = start_index; i <= end_index; i++)
      {
         val_vec.push_back(temp->GetBinContent(i));
         err_vec.push_back(temp->GetBinError(i));
      }
   }
   std::shared_ptr<Variation> var_ptr = BookVariation(obsname, samplename, gname, morph_val1, morph_val2, val_vec, err_vec);
   delete temp;
   return var_ptr;
}

void Fitter::InitializeMorphing()
{
   MSGUser()->MSG_INFO("//---------------------//");
   MSGUser()->MSG_INFO("// Initialize Morphing //");
   MSGUser()->MSG_INFO("//---------------------//");
   // Book Gamma Parameters
   if (FIT_NEED_STAT_GAMMA && (!FIT_WITH_CORRELATION))
   {
      MSGUser()->MSG_INFO("");
      MSGUser()->MSG_INFO("Book Stat Gamma:");
      for (auto &obs : Sample_Map)
      {
         for (auto &samp : obs.second)
         {
            for (int i = 0; i < samp.second->GetNBins(); i++)
            {
               TString gparname = samp.second->GetGammaParameterName(i);
               auto findpar = Par_Map.find(gparname);
               if (findpar != Par_Map.end())
                  samp.second->SetGammaParameter(i, findpar->second);
               else
               {
                  // Book New Gamma Parameter
                  // Calculate the sigma of this parameter
                  double sum = 0;
                  double sum_err2 = 0;
                  for (auto &samp2 : obs.second)
                  {
                     if (samp2.second->GetGammaParameterName(i).TString::EqualTo(gparname))
                     {
                        sum = sum + samp2.second->GetValue(i);
                        sum_err2 = sum_err2 + pow(samp2.second->GetError(i), 2);
                     }
                  }
                  double sigma = sqrt(sum_err2) / sum;

                  auto gpar = BookParameter(gparname, 1, 1 - FIT_MAX_NUISANCE * sigma, 1 + FIT_MAX_NUISANCE * sigma, ParameterType::StatGamma);
                  MSGUser()->MSG_INFO("   -", gparname.TString::Data());
                  gpar->SetGammaSigma(sigma);
                  samp.second->SetGammaParameter(i, gpar);
               }
            }
         }
      }
      MSGUser()->MSG_INFO("");
   }

   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &morph_cate : samp.second)
         {
            int npar = morph_cate.first->GetNParameters();
            std::vector<std::shared_ptr<MorphingRange>> range_vec;
            if (npar == 1)
               range_vec = InitializeMorphingRange1D(
                                    Variation_Sigma_Map[obs.first][samp.first][morph_cate.first]);
            else if (npar == 2)
               range_vec = InitializeMorphingRange2D(
                                    Variation_Sigma_Map[obs.first][samp.first][morph_cate.first]);
            else
               range_vec = InitializeMorphingRangeXD(
                                    Variation_Sigma_Map[obs.first][samp.first][morph_cate.first]);

            auto findobs = Variation_MorphingRange_Map.find(obs.first);
            if (findobs != Variation_MorphingRange_Map.end())
            {
               auto findsamp = findobs->second.find(samp.first);
               if (findsamp != findobs->second.end())
                  findsamp->second.emplace(std::pair<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>(morph_cate.first, range_vec));
               else
               {
                  std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>
                  empty_cate_map;
                  empty_cate_map.emplace(std::pair<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>(morph_cate.first, range_vec));
                  findobs->second.emplace(std::pair<std::shared_ptr<Sample>,
                                          std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>(samp.first, empty_cate_map));
               }
            }
            else
            {
               std::map<std::shared_ptr<Sample>,
                   std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>
                   empty_sample_map;
               std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>
               empty_cate_map;

               empty_cate_map.emplace(std::pair<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>(morph_cate.first, range_vec));
               empty_sample_map.emplace(std::pair<std::shared_ptr<Sample>,
                                        std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>(samp.first, empty_cate_map));
               Variation_MorphingRange_Map.emplace(std::pair<std::shared_ptr<Observable>,
                                                   std::map<std::shared_ptr<Sample>,
                                                   std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>(obs.first, empty_sample_map));
            }
         }
      }
   }

   MSGUser()->MSG_INFO("//----------------------//");
   MSGUser()->MSG_INFO("// Morphing Initialized //");
   MSGUser()->MSG_INFO("//----------------------//");
}

std::vector<std::shared_ptr<MorphingRange>> Fitter::InitializeMorphingRange1D(const std::vector<std::shared_ptr<MorphingValue>> &val_vec)
{
   std::vector<std::shared_ptr<MorphingRange>> range_vec;
   std::vector<std::shared_ptr<MorphingValue>> val_temp_vec = val_vec;
   if (val_temp_vec.size() < 2)
   {
      MSGUser()->MSG_ERROR("Less Than 2 Variations, Morphing Range cannot be defined!");
      return range_vec;
   }
   for (int i = 0; i < val_temp_vec.size() - 1; i++)
   {
      for (int j = i + 1; j < val_temp_vec.size(); j++)
      {
         if (val_temp_vec[i]->GetValue() > val_temp_vec[j]->GetValue())
         {
            auto temp_val = val_temp_vec[i];
            val_temp_vec[i] = val_temp_vec[j];
            val_temp_vec[j] = temp_val;
         }
      }
   }
   // Sort from Small to Large
   std::shared_ptr<MorphingCategory> morph = val_temp_vec[0]->GetMorphingCategory();

   if (morph->GetMorphingMode() == MorphingMode::FULL_LINEAR)
      return InitializeMorphingRange1D_FULL_LINEAR(val_temp_vec);
   else if (morph->GetMorphingMode() == MorphingMode::POL6_EXP)
      return InitializeMorphingRange1D_POL6_EXP(val_temp_vec);
   else
      return range_vec; // This is an empty vec
}

std::vector<std::shared_ptr<MorphingRange>> Fitter::InitializeMorphingRange1D_FULL_LINEAR(const std::vector<std::shared_ptr<MorphingValue>> &val_vec)
{
   std::vector<std::shared_ptr<MorphingRange>> range_vec;
   std::shared_ptr<MorphingCategory> morph = val_vec[0]->GetMorphingCategory();
   std::shared_ptr<MorphingRange> range;
   std::shared_ptr<Observable> obs = val_vec[0]->GetVariation()->RetriveObservable();
   std::shared_ptr<Sample> samp = val_vec[0]->GetVariation()->RetriveSample();
   std::vector<std::shared_ptr<MorphingValue>> my_range_vec;
   std::vector<std::shared_ptr<MorphingValue>> my_val_vec;
   my_range_vec.push_back(nullptr);
   my_range_vec.push_back(val_vec[0]);
   my_val_vec.push_back(val_vec[0]);
   my_val_vec.push_back(val_vec[1]);
   range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
   range_vec.push_back(range);
   for (int i = 0; i < val_vec.size() - 1; i++)
   {
      my_range_vec.clear();
      my_val_vec.clear();
      my_range_vec.push_back(val_vec[i]);
      my_range_vec.push_back(val_vec[i + 1]);
      my_val_vec.push_back(val_vec[i]);
      my_val_vec.push_back(val_vec[i + 1]);

      range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
      range_vec.push_back(range);
   }
   my_range_vec.clear();
   my_val_vec.clear();
   my_range_vec.push_back(val_vec[val_vec.size() - 1]);
   my_range_vec.push_back(nullptr);
   my_val_vec.push_back(val_vec[val_vec.size() - 2]);
   my_val_vec.push_back(val_vec[val_vec.size() - 1]);
   range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
   range_vec.push_back(range);
   return range_vec;
}

std::vector<std::shared_ptr<MorphingRange>> Fitter::InitializeMorphingRange1D_POL6_EXP(const std::vector<std::shared_ptr<MorphingValue>> &val_vec)
{
   std::vector<std::shared_ptr<MorphingRange>> range_vec;
   if (val_vec.size() != 3 && val_vec.size() != 2)
   {
      MSGUser()->MSG_ERROR("It should be 3 or 2 Variations for POL6_EXP, Morphing Range cannot be defined!");
      return range_vec;
   }
   std::vector<std::shared_ptr<MorphingValue>> my_val_vec;
   std::vector<std::shared_ptr<MorphingValue>> val_temp_vec = val_vec;
   std::shared_ptr<MorphingCategory> morph = val_vec[0]->GetMorphingCategory();
   if (val_temp_vec.size() == 2)
   {
      double nominal_val = (morph->GetParameterNominals())[0];
      if (fabs(val_vec[0]->GetValue(0) - nominal_val) < FIT_PARAMETER_PRECISION)
      {
         std::shared_ptr<MorphingValue> vir_edge = std::make_shared<MorphingValue>(val_vec[0]->GetValue(0) * 2 - val_vec[1]->GetValue(), nullptr, morph);
         val_temp_vec.clear();
         val_temp_vec.push_back(vir_edge);
         val_temp_vec.push_back(val_vec[0]);
         val_temp_vec.push_back(val_vec[1]);

         my_val_vec.push_back(nullptr);
         my_val_vec.push_back(val_vec[0]);
         my_val_vec.push_back(val_vec[1]);
      }
      else if (fabs(val_vec[1]->GetValue(0) - nominal_val) < FIT_PARAMETER_PRECISION)
      {
         std::shared_ptr<MorphingValue> vir_edge = std::make_shared<MorphingValue>(val_vec[1]->GetValue(0) * 2 - val_vec[0]->GetValue(), nullptr, morph);
         val_temp_vec.clear();
         val_temp_vec.push_back(val_vec[0]);
         val_temp_vec.push_back(val_vec[1]);
         val_temp_vec.push_back(vir_edge);

         my_val_vec.push_back(val_vec[0]);
         my_val_vec.push_back(val_vec[1]);
         my_val_vec.push_back(nullptr);
      }
      else
         MSGUser()->MSG_ERROR("Nominal Variation Not Found!");
   }
   else
   {
      my_val_vec.push_back(val_vec[0]);
      my_val_vec.push_back(val_vec[1]);
      my_val_vec.push_back(val_vec[2]);
   }

   std::shared_ptr<MorphingRange> range;
   std::shared_ptr<Observable> obs = val_vec[0]->GetVariation()->RetriveObservable();
   std::shared_ptr<Sample> samp = val_vec[0]->GetVariation()->RetriveSample();
   std::vector<std::shared_ptr<MorphingValue>> my_range_vec;
   my_range_vec.push_back(nullptr);
   my_range_vec.push_back(val_temp_vec[0]);
   range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
   range_vec.push_back(range);

   my_range_vec.clear();
   my_range_vec.push_back(val_temp_vec[0]);
   my_range_vec.push_back(val_temp_vec[2]);
   range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
   range_vec.push_back(range);

   my_range_vec.clear();
   my_range_vec.push_back(val_temp_vec[2]);
   my_range_vec.push_back(nullptr);
   range = std::make_shared<MorphingRange>(my_val_vec, my_range_vec, morph, samp, obs);
   range_vec.push_back(range);
   return range_vec;
}

std::vector<std::shared_ptr<MorphingRange>> Fitter::InitializeMorphingRange2D(const std::vector<std::shared_ptr<MorphingValue>> &val_vec)
{
   std::vector<std::shared_ptr<MorphingRange>> range_vec;

   if (val_vec.size() < 3)
   {
      MSGUser()->MSG_ERROR("It should be more than 3 Variations for 2D Morphing, Morphing Range cannot be defined!");
      return range_vec;
   }

   std::shared_ptr<Phoenix::Graph2DTool> mytool = std::make_shared<Phoenix::Graph2DTool>(MSGUser(), FIT_PARAMETER_PRECISION);

   std::vector<Phoenix::Point> point_vec;
   for (int i = 0; i < val_vec.size(); i++)
      point_vec.push_back(Phoenix::Point(val_vec[i]));

   TRandom3* myrnd = new TRandom3(12345);
   for (int i = 0; i < val_vec.size(); i++)
   {
      point_vec[i].SetX(point_vec[i].X() + myrnd->Gaus(0, 1) * 100 * FIT_PARAMETER_PRECISION);
      point_vec[i].SetY(point_vec[i].Y() + myrnd->Gaus(0, 1) * 100 * FIT_PARAMETER_PRECISION);
   }
   delete myrnd;

   std::vector<Phoenix::Triangle> tri_vec = mytool->DelaunayTriangulation(point_vec);
   std::shared_ptr<MorphingRange> range;
   std::shared_ptr<Observable> obs = val_vec[0]->GetVariation()->RetriveObservable();
   std::shared_ptr<Sample> samp = val_vec[0]->GetVariation()->RetriveSample();
   std::shared_ptr<MorphingCategory> morph = val_vec[0]->GetMorphingCategory();
   std::vector<std::shared_ptr<MorphingValue>> my_range_vec;

   for (int i = 0; i < tri_vec.size(); i++)
   {
      my_range_vec.clear();
      my_range_vec.push_back(tri_vec[i].Vertex(0).GetMorphingValue());
      my_range_vec.push_back(tri_vec[i].Vertex(1).GetMorphingValue());
      my_range_vec.push_back(tri_vec[i].Vertex(2).GetMorphingValue());
      if ((fabs(tri_vec[i].Vertex(0).GetMorphingValue()->GetValueX() - tri_vec[i].Vertex(1).GetMorphingValue()->GetValueX()) < FIT_PARAMETER_PRECISION) &&
            (fabs(tri_vec[i].Vertex(0).GetMorphingValue()->GetValueX() - tri_vec[i].Vertex(2).GetMorphingValue()->GetValueX()) < FIT_PARAMETER_PRECISION))
         continue;
      if ((fabs(tri_vec[i].Vertex(0).GetMorphingValue()->GetValueY() - tri_vec[i].Vertex(1).GetMorphingValue()->GetValueY()) < FIT_PARAMETER_PRECISION) &&
            (fabs(tri_vec[i].Vertex(0).GetMorphingValue()->GetValueY() - tri_vec[i].Vertex(2).GetMorphingValue()->GetValueY()) < FIT_PARAMETER_PRECISION))
         continue;
      range = std::make_shared<MorphingRange>(my_range_vec, my_range_vec, morph, samp, obs);
      range_vec.push_back(range);
   }
   MSGUser()->MSG_INFO("Initialize ", obs->GetName().TString::Data(), " ", samp->GetName().TString::Data(), " ", morph->GetName().TString::Data(), " Morphing Range");
   MSGUser()->MSG_INFO("Number of Variations: ", point_vec.size(), " Number of Triangles: ", tri_vec.size());

   mytool->CheckDelaunayTriangylation(point_vec, tri_vec, TString::Format("%s_%s_%s_MorphingRange", obs->GetName().TString::Data(), samp->GetName().TString::Data(), morph->GetName().TString::Data()),
                                      true);

   return range_vec;
}

std::vector<std::shared_ptr<MorphingRange>> Fitter::InitializeMorphingRangeXD(const std::vector<std::shared_ptr<MorphingValue>> &val_vec)
{
   std::vector<std::shared_ptr<MorphingRange>> range_vec;
   return range_vec;
}

void Fitter::Fit(FittingMethod method)
{
   if (method == FittingMethod::PLH)
      Fit_PLH();
   else if (method == FittingMethod::ACS)
      Fit_ACS();
}

void Fitter::Fit_PLH()
{
   MSGUser()->MSG_INFO("//---------------------------//");
   MSGUser()->MSG_INFO("// Profile Likelihood Method //");
   MSGUser()->MSG_INFO("//---------------------------//");

   PLH_FCN fcn(MSGUser(), &Par_Map, &Obs_Map, &Sample_Map, &Variation_MorphingRange_Map, &Corr_Cate_Vec, FIT_NEED_STAT_GAMMA, FIT_WITH_CORRELATION, FIT_USE_RELATIVE_IMPACT, FIT_SUM_IMPACT,
               FIT_SEPARATE_POI, FIT_ENABLE_METAL_BOOST, FIT_NTHREAD);
   // Define Parameters
   ROOT::Minuit2::MnUserParameters upar;
   for (auto &p : Par_Map)
   {
      upar.Add(p.second->GetName().TString::Data(),
               p.second->GetValue(),
               (p.second->GetMax() - p.second->GetMin()) / FIT_RANGE_STEP / 2,
               p.second->GetMin(),
               p.second->GetMax());

      if (p.second->IsFixed())
         upar.Fix(p.second->GetName().TString::Data());
   }

   // Pre Run
   MSGUser()->MSG_INFO("Scan all nuisance parameters");
   int index = 0;
   std::vector<int> NuisanceIndex_vec;
   std::vector<double> DeltaLH_vec;
   for (auto &p : Par_Map)
   {
      if ((p.second->GetType() == ParameterType::Nuisance) &&
            (!p.second->IsFixed()))
      {
         ROOT::Minuit2::MnScan scan(fcn, upar, 2);
         auto res = scan.Scan(index, 3, -1, 1);
         double delta_lh = res[1].second - res[2].second;
         if (fabs(delta_lh - 0.5) < 1e-5)
         {
            upar.Fix(index);
            MSGUser()->MSG_INFO(p.second->GetName().TString::Data(), " Fixed to 0 ! ( ", fabs(delta_lh - 0.5), " )");
         }
         else
         {
            NuisanceIndex_vec.push_back(index);
            DeltaLH_vec.push_back(fabs(delta_lh - 0.5));
         }
      }
      index = index + 1;
   }

   if (DeltaLH_vec.size() > 1)
   {
      for (int i = 0; i < DeltaLH_vec.size() - 1; i++)
      {
         for (int j = 1; j < DeltaLH_vec.size(); j++)
         {
            if (DeltaLH_vec[i] < DeltaLH_vec[j])
            {
               int temp_index = NuisanceIndex_vec[i];
               NuisanceIndex_vec[i] = NuisanceIndex_vec[j];
               NuisanceIndex_vec[j] = temp_index;
               double temp_delta = DeltaLH_vec[i];
               DeltaLH_vec[i] = DeltaLH_vec[j];
               DeltaLH_vec[j] = temp_delta;
            }
         }
      }
   }

   MSGUser()->MSG_INFO("Number of Nuisance Parameters to Fit: ", NuisanceIndex_vec.size());
   fcn.Reset();

   ROOT::Minuit2::MnStrategy Fit_Strategy(0);
   std::vector<ROOT::Minuit2::MnUserParameterState> par_state_vec;
   // MSGUser()->MSG_INFO("Run Hesse to provide starting point");
   // ROOT::Minuit2::MnHesse hesse(Fit_Strategy);
   // par_state_vec.push_back(hesse(fcn, upar, 0));

   if (FIT_ENABLE_SPEEDUP)
   {
      index = 0;
      for (auto &p : Par_Map)
      {
         if (p.second->GetType() == ParameterType::StatGamma)
            upar.Fix(index);
         index = index + 1;
      }
      ROOT::Minuit2::MnMigrad speedup_migrad(fcn, upar, Fit_Strategy);
      MSGUser()->MSG_INFO("Start Speed Up Fit (Migrad)");
      par_state_vec.push_back(speedup_migrad(0, 0.1).UserState());
      MSGUser()->MSG_INFO("EDM After Speed Up Fit: ", par_state_vec.back().Edm());
      MSGUser()->MSG_INFO("FVAL After Speed Up Fit: ", par_state_vec.back().Fval());
      index = 0;
      for (auto &p : Par_Map)
      {
         if (p.second->GetType() == ParameterType::StatGamma)
         {
            upar.Release(index);
            par_state_vec.back().Release(index);
         }
         index = index + 1;
      }
   }

   if (FIT_ENABLE_SPEEDUP)
   {
      ROOT::Minuit2::MnSimplex simplex(fcn, par_state_vec.back(), Fit_Strategy);
      par_state_vec.push_back(simplex(0, 0.1).UserState());
   }
   else
   {
      ROOT::Minuit2::MnSimplex simplex(fcn, upar, Fit_Strategy);
      par_state_vec.push_back(simplex(0, 0.1).UserState());
   }
   MSGUser()->MSG_INFO("EDM After Simplex: ", par_state_vec.back().Edm());
   MSGUser()->MSG_INFO("FVAL After Simplex: ", par_state_vec.back().Fval());
   MSGUser()->MSG_INFO("Start Migrad");

   // Final Fit
   ROOT::Minuit2::MnMigrad migrad(fcn, par_state_vec.back(), Fit_Strategy);
   auto finalresult = migrad(0, 0.01);
   par_state_vec.push_back(finalresult.UserState());
   MSGUser()->MSG_INFO("EDM After Migrad: ", par_state_vec.back().Edm());
   MSGUser()->MSG_INFO("FVAL After Migrad: ", par_state_vec.back().Fval());

   if (FIT_RUN_MINOS)
   {
      index = 0;
      ROOT::Minuit2::MnMinos minos(fcn, finalresult, Fit_Strategy);
      for (auto &p : Par_Map)
      {
         if (!p.second->IsFixed() && (p.second->GetMinosRequirement()))
         {
            MSGUser()->MSG_INFO("Run Minos for Parameter: ", p.second->GetName().TString::Data());
            p.second->SetMinosUncertainty(minos(index));
         }
         index = index + 1;
      }
   }

   index = 0;
   for (auto &p : Par_Map)
   {
      p.second->SetValue(par_state_vec.back().Value(index));
      p.second->SetError(par_state_vec.back().Error(index));
      index = index + 1;
   }

   MSGUser()->MSG_INFO("//------------------------------------//");
   MSGUser()->MSG_INFO("// Profile Likelihood Method Finished //");
   MSGUser()->MSG_INFO("//------------------------------------//");
}

void Fitter::PrintResults()
{
   for (auto &par : Par_Map)
      MSGUser()->MSG_INFO(par.first.TString::Data(), "   ", par.second->GetValue(), "    ", par.second->GetError(), "   ", par.second->GetErrorUp(), "   ", par.second->GetErrorDown());
}

void Fitter::SmoothVariations(TString obsname, TString samplename, TString parname, int level)
{
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   std::shared_ptr<Sample> samp_ptr = GetSample(obsname, samplename);
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   auto findobs = Variation_Map.find(obs_ptr);
   if (findobs != Variation_Map.end())
   {
      auto findsamp = findobs->second.find(samp_ptr);
      if (findsamp != findobs->second.end())
      {
         auto findpar = findsamp->second.find(cate_ptr);
         if (findpar != findsamp->second.end())
            Smooth_Var(findpar->second, Variation_Sigma_Map[obs_ptr][samp_ptr][cate_ptr], level);
         else
            MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data(), " Parameter ", parname.TString::Data());
      }
      else
         MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data());
   }
   else
      MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data());
}

void Fitter::SmoothVariations(TString parname, int level)
{
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         auto findpar = samp.second.find(cate_ptr);
         if (findpar != samp.second.end())
            Smooth_Var(findpar->second, Variation_Sigma_Map[obs.first][samp.first][cate_ptr], level);
      }
   }
}

void Fitter::SymmetrizeVariations(TString obsname, TString samplename, TString parname)
{
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   std::shared_ptr<Sample> samp_ptr = GetSample(obsname, samplename);
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   auto findobs = Variation_Map.find(obs_ptr);
   if (findobs != Variation_Map.end())
   {
      auto findsamp = findobs->second.find(samp_ptr);
      if (findsamp != findobs->second.end())
      {
         auto findpar = findsamp->second.find(cate_ptr);
         if (findpar != findsamp->second.end())
            Symmetrize_Var(findpar->second, Variation_Sigma_Map[obs_ptr][samp_ptr][cate_ptr]);
         else
            MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data(), " Parameter ", parname.TString::Data());
      }
      else
         MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data());
   }
   else
      MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data());
}

void Fitter::SymmetrizeVariations(TString parname)
{
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         auto findpar = samp.second.find(cate_ptr);
         if (findpar != samp.second.end())
            Symmetrize_Var(findpar->second, Variation_Sigma_Map[obs.first][samp.first][cate_ptr]);
      }
   }
}

void Fitter::SynchronizeNominals(TString obsname, TString samplename, TString parname)
{
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   std::shared_ptr<Sample> samp_ptr = GetSample(obsname, samplename);
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   auto findobs = Variation_Map.find(obs_ptr);
   if (findobs != Variation_Map.end())
   {
      auto findsamp = findobs->second.find(samp_ptr);
      if (findsamp != findobs->second.end())
      {
         auto findpar = findsamp->second.find(cate_ptr);
         if (findpar != findsamp->second.end())
            Synchronize_Var(findpar->second, Variation_Sigma_Map[obs_ptr][samp_ptr][cate_ptr], samp_ptr);
         else
            MSGUser()->MSG_INFO("No Variation for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data(), " Parameter ", parname.TString::Data());
      }
      else
         MSGUser()->MSG_INFO("No Variation for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data());
   }
   else
      MSGUser()->MSG_INFO("No Variation for Observable ", obsname.TString::Data());
}

void Fitter::SynchronizeNominals(TString parname)
{
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         auto findpar = samp.second.find(cate_ptr);
         if (findpar != samp.second.end())
            Synchronize_Var(findpar->second, Variation_Sigma_Map[obs.first][samp.first][cate_ptr], samp.first);
      }
   }
}

void Fitter::SynchronizeNominals()
{
   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
            Synchronize_Var(cate.second, Variation_Sigma_Map[obs.first][samp.first][cate.first], samp.first);
      }
   }
}

void Fitter::NormalizeVariations(TString obsname, TString samplename, TString parname)
{
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   std::shared_ptr<Sample> samp_ptr = GetSample(obsname, samplename);
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   auto findobs = Variation_Map.find(obs_ptr);
   if (findobs != Variation_Map.end())
   {
      auto findsamp = findobs->second.find(samp_ptr);
      if (findsamp != findobs->second.end())
      {
         auto findpar = findsamp->second.find(cate_ptr);
         if (findpar != findsamp->second.end())
            Normalize_Var(findpar->second, Variation_Sigma_Map[obs_ptr][samp_ptr][cate_ptr]);
         else
            MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data(), " Parameter ", parname.TString::Data());
      }
      else
         MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data(), " Sample ", samplename.TString::Data());
   }
   else
      MSGUser()->MSG_INFO("No Varaition for Observable ", obsname.TString::Data());
}

void Fitter::NormalizeVariations(TString parname)
{
   std::shared_ptr<MorphingCategory> cate_ptr = GetMorphingCategory(parname);

   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         auto findpar = samp.second.find(cate_ptr);
         if (findpar != samp.second.end())
            Normalize_Var(findpar->second, Variation_Sigma_Map[obs.first][samp.first][cate_ptr]);
      }
   }
}

void Fitter::NormalizeVariations()
{
   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
            Normalize_Var(cate.second, Variation_Sigma_Map[obs.first][samp.first][cate.first]);
      }
   }
}

void Fitter::Normalize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec)
{
   if (val_vec.size() != var_vec.size())
   {
      MSGUser()->MSG_ERROR("The number of Variations is not the same as the number of the Morphing Values!");
      return;
   }
   // Get Nominal Value
   if (var_vec.size() == 0)
   {
      MSGUser()->MSG_ERROR("No Variation Available!");
      return;
   }
   std::shared_ptr<MorphingCategory> cate_ptr = var_vec[0]->RetriveMorphingCategory();
   std::vector<double> nom_val = cate_ptr->GetParameterNominals();

   // Find Nominal Variation
   std::shared_ptr<Variation> Nom_Var = nullptr;
   for (int i = 0; i < val_vec.size(); i++)
   {
      if ((nom_val.size()) != (val_vec[i]->GetNValues()))
         continue;
      else
      {
         bool allequal = true;
         for (int j = 0; j < nom_val.size(); j++)
         {
            if (fabs(nom_val[j] - val_vec[i]->GetValue(j)) > FIT_PARAMETER_PRECISION)
            {
               allequal = false;
               break;
            }
         }
         if (allequal)
            Nom_Var = var_vec[i];
      }
   }
   if (Nom_Var == nullptr)
   {
      MSGUser()->MSG_ERROR("Nominal Variation not found!");
      return;
   }

   for (int i = 0; i < var_vec.size(); i++)
   {
      if (var_vec[i] != Nom_Var)
         Normalize_Var(var_vec[i], Nom_Var);
   }
}

void Fitter::Normalize_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm)
{
   if (var->GetCache() == nullptr)
      var->BookCache();
   if (norm->GetCache() == nullptr)
      norm->BookCache();

   if ((norm->GetNBins()) != (var->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the variation is not consistent with that in the nominal!");
      return;
   }

   double sum_init = 0;
   for (int i = 0; i < norm->GetNBins(); i++)
      sum_init = sum_init + var->GetValue(i);

   double sum_new = 0;
   for (int i = 0; i < norm->GetNBins(); i++)
      sum_new = sum_new + var->GetValue(i);

   double scale = sum_init / sum_new;
   for (int i = 0; i < norm->GetNBins(); i++)
      var->SetValue(i, var->GetValue(i) * scale);

   MSGUser()->MSG_INFO("Normalize Variation for Observable: ", norm->RetriveObservable()->GetName().TString::Data(),
                       " Sample: ", norm->RetriveSample()->GetName().TString::Data(),
                       " Parameter: ", norm->RetriveMorphingCategory()->GetName().TString::Data());
}

void Fitter::Smooth_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec, int level)
{
   if (val_vec.size() != var_vec.size())
   {
      MSGUser()->MSG_ERROR("The number of Variations is not the same as the number of the Morphing Values!");
      return;
   }
   // Get Nominal Value
   if (var_vec.size() == 0)
   {
      MSGUser()->MSG_ERROR("No Variation Available!");
      return;
   }
   std::shared_ptr<MorphingCategory> cate_ptr = var_vec[0]->RetriveMorphingCategory();
   std::vector<double> nom_val = cate_ptr->GetParameterNominals();

   // Find Nominal Variation
   std::shared_ptr<Variation> Nom_Var = nullptr;
   for (int i = 0; i < val_vec.size(); i++)
   {
      if ((nom_val.size()) != (val_vec[i]->GetNValues()))
         continue;
      else
      {
         bool allequal = true;
         for (int j = 0; j < nom_val.size(); j++)
         {
            if (fabs(nom_val[j] - val_vec[i]->GetValue(j)) > FIT_PARAMETER_PRECISION)
            {
               allequal = false;
               break;
            }
         }
         if (allequal)
            Nom_Var = var_vec[i];
      }
   }
   if (Nom_Var == nullptr)
   {
      MSGUser()->MSG_ERROR("Nominal Variation not found!");
      return;
   }

   for (int i = 0; i < var_vec.size(); i++)
   {
      if (var_vec[i] != Nom_Var)
         Smooth_Var(var_vec[i], Nom_Var, level);
   }
}

void Fitter::Smooth_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm, int level)
{
   if (var->GetCache() == nullptr)
      var->BookCache();
   if (norm->GetCache() == nullptr)
      norm->BookCache();

   if (level <= 0)
      return;
   if ((norm->GetNBins()) != (var->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the variation is not consistent with that in the nominal!");
      return;
   }
   if (norm->GetNBins() > 2)
   {
      double sum_init = 0;
      for (int i = 0; i < norm->GetNBins(); i++)
         sum_init = sum_init + var->GetValue(i);
      vector<double> new_ratio_vec;
      for (int i = 1; i < norm->GetNBins() - 1; i++)
      {
         double ratio_p = var->GetValue(i - 1) / norm->GetValue(i - 1);
         double ratio_n = var->GetValue(i + 1) / norm->GetValue(i + 1);

         double ratio = var->GetValue(i) / norm->GetValue(i);

         if (!isfinite(ratio_p))
            ratio_p = 0;
         if (!isfinite(ratio_n))
            ratio_n = 0;
         if (!isfinite(ratio))
            ratio = 0;

         double new_ratio = (level * ratio + ratio_p + ratio_n) / (level + 2);

         new_ratio_vec.push_back(new_ratio);
      }

      for (int i = 1; i < norm->GetNBins() - 1; i++)
         var->SetValue(i, norm->GetValue(i) * new_ratio_vec[i - 1]);

      double sum_new = 0;
      for (int i = 0; i < norm->GetNBins(); i++)
         sum_new = sum_new + var->GetValue(i);

      double scale = sum_init / sum_new;
      for (int i = 0; i < norm->GetNBins(); i++)
         var->SetValue(i, var->GetValue(i) * scale);

      MSGUser()->MSG_INFO("Smooth Variation for Observable: ", norm->RetriveObservable()->GetName().TString::Data(),
                          " Sample: ", norm->RetriveSample()->GetName().TString::Data(),
                          " Parameter: ", norm->RetriveMorphingCategory()->GetName().TString::Data(),
                          " Level: ", level);
   }
}

void Fitter::Symmetrize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec)
{
   if (val_vec.size() != var_vec.size())
   {
      MSGUser()->MSG_ERROR("The number of Variations is not the same as the number of the Morphing Values!");
      return;
   }
   // Get Nominal Value
   if (var_vec.size() == 0)
   {
      MSGUser()->MSG_ERROR("No Variation Available!");
      return;
   }
   std::shared_ptr<MorphingCategory> cate_ptr = var_vec[0]->RetriveMorphingCategory();
   std::vector<double> nom_val = cate_ptr->GetParameterNominals();

   // Find Nominal Variation
   std::shared_ptr<Variation> Nom_Var = nullptr;
   for (int i = 0; i < val_vec.size(); i++)
   {
      if ((nom_val.size()) != (val_vec[i]->GetNValues()))
         continue;
      else
      {
         bool allequal = true;
         for (int j = 0; j < nom_val.size(); j++)
         {
            if (fabs(nom_val[j] - val_vec[i]->GetValue(j)) > FIT_PARAMETER_PRECISION)
            {
               allequal = false;
               break;
            }
         }
         if (allequal)
            Nom_Var = var_vec[i];
      }
   }
   if (Nom_Var == nullptr)
   {
      MSGUser()->MSG_ERROR("Nominal Variation not found!");
      return;
   }

   if (nom_val.size() == 1)
   {
      if (val_vec.size() < 3)
         return;
      std::vector<bool> hassym;
      for (int i = 0; i < val_vec.size(); i++)
         hassym.push_back(false);
      for (int i = 0; i < val_vec.size() - 1; i++)
      {
         if ((val_vec[i]->GetNValues()) != 1)
            continue;
         for (int j = i + 1; j < val_vec.size(); j++)
         {
            if ((val_vec[j]->GetNValues()) != 1)
               continue;
            if (fabs(val_vec[i]->GetValue(0) + val_vec[j]->GetValue(0) - 2 * nom_val[0]) < FIT_PARAMETER_PRECISION)
            {
               if ((hassym[i] == false) && (hassym[j] == false) && var_vec[i] != Nom_Var && var_vec[j] != Nom_Var)
               {
                  Symmetrize_Var(var_vec[i], var_vec[j], Nom_Var);
                  hassym[i] = true;
                  hassym[j] = true;
               }
            }
         }
      }
   }
   else
      MSGUser()->MSG_ERROR("No Symmetrization Strategy for Multi Dimentions!");
}

void Fitter::Symmetrize_Var(std::shared_ptr<Variation> up, std::shared_ptr<Variation> down, std::shared_ptr<Variation> norm)
{
   if (up->GetCache() == nullptr)
      up->BookCache();
   if (down->GetCache() == nullptr)
      down->BookCache();
   if (norm->GetCache() == nullptr)
      norm->BookCache();

   if ((norm->GetNBins()) != (up->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the up variation is not consistent with that in the nominal!");
      return;
   }
   if ((norm->GetNBins()) != (down->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the down variation is not consistent with that in the nominal!");
      return;
   }
   for (int i = 0; i < norm->GetNBins(); i++)
   {
      double delta_up = up->GetValue(i) - norm->GetValue(i);
      double delta_down = down->GetValue(i) - norm->GetValue(i);

      double new_delta_up = (delta_up - delta_down) / 2;
      double new_delta_down = -new_delta_up;

      up->SetValue(i, norm->GetValue(i) + new_delta_up);
      down->SetValue(i, norm->GetValue(i) + new_delta_down);

      if (FIT_NEGATIVE_PROTECTION)
      {
         if (up->GetValue(i) < 0)
            up->SetValue(i, 0);
         if (down->GetValue(i) < 0)
            down->SetValue(i, 0);
      }
   }
   MSGUser()->MSG_INFO("Symmetrize Variation for Observable: ", norm->RetriveObservable()->GetName().TString::Data(),
                       " Sample: ", norm->RetriveSample()->GetName().TString::Data(),
                       " Parameter: ", norm->RetriveMorphingCategory()->GetName().TString::Data());
}

void Fitter::Synchronize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec, std::shared_ptr<Sample> samp)
{
   if (val_vec.size() != var_vec.size())
   {
      MSGUser()->MSG_ERROR("The number of Variations is not the same as the number of the Morphing Values!");
      return;
   }
   // Get Nominal Value
   if (var_vec.size() == 0)
   {
      MSGUser()->MSG_ERROR("No Variation Available!");
      return;
   }
   std::shared_ptr<MorphingCategory> cate_ptr = var_vec[0]->RetriveMorphingCategory();
   std::vector<double> nom_val = cate_ptr->GetParameterNominals();

   // Find Nominal Variation
   std::shared_ptr<Variation> Nom_Var = nullptr;
   for (int i = 0; i < val_vec.size(); i++)
   {
      if ((nom_val.size()) != (val_vec[i]->GetNValues()))
         continue;
      else
      {
         bool allequal = true;
         for (int j = 0; j < nom_val.size(); j++)
         {
            if (fabs(nom_val[j] - val_vec[i]->GetValue(j)) > FIT_PARAMETER_PRECISION)
            {
               allequal = false;
               break;
            }
         }
         if (allequal)
            Nom_Var = var_vec[i];
      }
   }
   if (Nom_Var == nullptr)
   {
      MSGUser()->MSG_ERROR("Nominal Variation not found!");
      return;
   }

   for (int i = 0; i < var_vec.size(); i++)
   {
      if (var_vec[i] != Nom_Var)
         Synchronize_Var(var_vec[i], Nom_Var, samp);
   }

   if (Nom_Var->GetCache() == nullptr)
      Nom_Var->BookCache();
   for (int i = 0; i < Nom_Var->GetNBins(); i++)
   {
      Nom_Var->SetValue(i, samp->GetValue(i));
      Nom_Var->SetError(i, samp->GetError(i));
   }
}

void Fitter::Synchronize_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm, std::shared_ptr<Sample> samp)
{
   if (var->GetCache() == nullptr)
      var->BookCache();
   if (norm->GetCache() == nullptr)
      norm->BookCache();

   if ((var->GetNBins()) != (samp->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the variation is not consistent with that in the sample nominal!");
      return;
   }
   if ((norm->GetNBins()) != (samp->GetNBins()))
   {
      MSGUser()->MSG_ERROR("The number of bins in the variation nominal is not consistent with that in the sample nominal!");
      return;
   }

   for (int i = 0; i < norm->GetNBins(); i++)
   {
      double impact;
      if (FIT_USE_RELATIVE_IMPACT)
         impact = (var->GetValue(i) / norm->GetValue(i) - 1) * samp->GetValue(i);
      else
         impact = var->GetValue(i) - norm->GetValue(i);

      var->SetValue(i, impact + samp->GetValue(i));
   }

   MSGUser()->MSG_INFO(" Synchronize Variation for Observable: ", norm->RetriveObservable()->GetName().TString::Data(),
                       " Sample: ", norm->RetriveSample()->GetName().TString::Data(),
                       " Parameter: ", norm->RetriveMorphingCategory()->GetName().TString::Data());
}

void Fitter::Rebin(int num)
{
   if (num > 1)
   {
      for (auto &obs : Obs_Map)
         Rebin(obs.first, num);
   }
}

void Fitter::SetRange(int start, int end)
{
   for (auto &obs : Obs_Map)
      SetRange(obs.first, start, end);
}

void Fitter::Rebin(TString obsname, int num)
{
   if (num < 2)
      return;
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   if (obs_ptr->GetCache() == nullptr)
      obs_ptr->BookCache();
   obs_ptr->Rebin(num);

   // Rebin Samples
   auto findobs = Sample_Map.find(obs_ptr);
   if (findobs != Sample_Map.end())
   {
      for (auto &samp : findobs->second)
      {
         if (samp.second->GetCache() == nullptr)
            samp.second->BookCache();
         samp.second->Rebin(num);
      }
   }

   // Rebin Variations
   auto findobs2 = Variation_Map.find(obs_ptr);
   if (findobs2 != Variation_Map.end())
   {
      for (auto &samp : findobs2->second)
         for (auto &cate : samp.second)
            for (int i = 0; i < cate.second.size(); i++)
            {
               if (cate.second[i]->GetCache() == nullptr)
                  cate.second[i]->BookCache();
               cate.second[i]->Rebin(num);
            }
   }

   if (FIT_WITH_CORRELATION)
   {
      for (auto &obs_x : Corr_Obs_Map)
      {
         for (auto &obs_y : obs_x.second)
         {
            if (obs_x.first == obs_ptr)
               obs_y.second->RebinX(num);
            if (obs_y.first == obs_ptr)
               obs_y.second->RebinY(num);
         }
      }

      for (auto &samp_x : Corr_Sample_Map)
      {
         for (auto &samp_y : samp_x.second)
         {
            if (samp_x.first->RetriveObservable() == obs_ptr)
               samp_y.second->RebinX(num);
            if (samp_y.first->RetriveObservable() == obs_ptr)
               samp_y.second->RebinY(num);
         }
      }
   }
}

void Fitter::Fit(TString method)
{
   Fit(TStringToFittingMethod(method));
}

FittingMethod Fitter::TStringToFittingMethod(TString name)
{
   if (name.TString::EqualTo("PLH") ||
         name.TString::EqualTo("plh") ||
         name.TString::EqualTo("ProfileLikelihood") ||
         name.TString::EqualTo("profilelikelihood"))
      return FittingMethod::PLH;
   if (name.TString::EqualTo("ACS") ||
         name.TString::EqualTo("acs") ||
         name.TString::EqualTo("AnalyticalChi2") ||
         name.TString::EqualTo("AnalyticalChiSquare") ||
         name.TString::EqualTo("analyticalchisquare") ||
         name.TString::EqualTo("analyticalchi2"))
      return FittingMethod::ACS;

   return FittingMethod::PLH;
}

void Fitter::SetRange(TString obsname, int start, int end)
{
   std::shared_ptr<Observable> obs_ptr = GetObservable(obsname);
   if (obs_ptr->GetCache() == nullptr)
      obs_ptr->BookCache();
   obs_ptr->SetRange(start, end);

   // SetRange Samples
   auto findobs = Sample_Map.find(obs_ptr);
   if (findobs != Sample_Map.end())
   {
      for (auto &samp : findobs->second)
      {
         if (samp.second->GetCache() == nullptr)
            samp.second->BookCache();
         samp.second->SetRange(start, end);
      }
   }

   // SetRange Variations
   auto findobs2 = Variation_Map.find(obs_ptr);
   if (findobs2 != Variation_Map.end())
   {
      for (auto &samp : findobs2->second)
         for (auto &cate : samp.second)
            for (int i = 0; i < cate.second.size(); i++)
            {
               if (cate.second[i]->GetCache() == nullptr)
                  cate.second[i]->BookCache();
               cate.second[i]->SetRange(start, end);
            }
   }

   if (FIT_WITH_CORRELATION)
   {
      for (auto &obs_x : Corr_Obs_Map)
      {
         for (auto &obs_y : obs_x.second)
         {
            if (obs_x.first == obs_ptr)
               obs_y.second->SetRangeX(start, end);
            if (obs_y.first == obs_ptr)
               obs_y.second->SetRangeY(start, end);
         }
      }

      for (auto &samp_x : Corr_Sample_Map)
      {
         for (auto &samp_y : samp_x.second)
         {
            if (samp_x.first->RetriveObservable() == obs_ptr)
               samp_y.second->SetRangeX(start, end);
            if (samp_y.first->RetriveObservable() == obs_ptr)
               samp_y.second->SetRangeY(start, end);
         }
      }
   }
}

void Fitter::RecoverModifications()
{
   for (auto &obs : Obs_Map)
      obs.second->RecoverModifications();

   for (auto &obs : Sample_Map)
   {
      for (auto &samp : obs.second)
         samp.second->RecoverModifications();
   }

   for (auto obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
         {
            for (int i = 0; i < cate.second.size(); i++)
               cate.second[i]->RecoverModifications();
         }
      }
   }
}

void Fitter::Fit_ACS()
{
#ifndef KRONOS
   // Eigen Elements for ACS
   Eigen::MatrixXd Gamma_Matrix;
   Eigen::MatrixXd V_Matrix;
   Eigen::MatrixXd Q_Matrix;
   Eigen::MatrixXd S_Matrix;
   Eigen::MatrixXd C_Matrix;
   Eigen::MatrixXd H_Matrix;
   Eigen::MatrixXd Y0_Matrix;
   Eigen::MatrixXd Lambda_Matrix;
   Eigen::MatrixXd Rho_Matrix;
   Eigen::MatrixXd Epsilon_Matrix;

   Eigen::MatrixXd POI_Shift;
   Eigen::MatrixXd Nuisance_Shift;

   Eigen::MatrixXd POI_Cov_Matrix;
   Eigen::MatrixXd Nuisance_Cov_Matrix;

   // Calculate Gamma Matrix
   // Sensitivity Matrix : dt_i / da_l
   int Total_Template_BinNum = 0;
   for (auto &obs : Obs_Map)
      Total_Template_BinNum = Total_Template_BinNum + obs.second->GetNBins();

   int NP_Num = 0;
   int POI_Num = 0;
   for (auto &par : Par_Map)
   {
      if (par.second->IsFixed())
         continue;

      if (par.second->GetType() == ParameterType::Interest)
         POI_Num++;
      if (par.second->GetType() == ParameterType::Nuisance)
         NP_Num++;
      if (par.second->GetType() == ParameterType::Normalize)
         POI_Num++;
   }

   Gamma_Matrix = Eigen::MatrixXd(Total_Template_BinNum, NP_Num);
   V_Matrix = Eigen::MatrixXd(Total_Template_BinNum, Total_Template_BinNum);
   H_Matrix = Eigen::MatrixXd(Total_Template_BinNum, POI_Num);
   Y0_Matrix = Eigen::MatrixXd(Total_Template_BinNum, 1);

   int bin_index = 0;
   int par_index = 0;
   int poi_index = 0;
   for (auto &obs : Obs_Map)
   {
      for (int index = 0; index < obs.second->GetNBins(); index++)
      {
         par_index = 0;
         poi_index = 0;
         for (auto &par : Par_Map)
         {
            if (par.second->IsFixed())
               continue;

            if ((par.second->GetType() == ParameterType::Nuisance) ||
                  (par.second->GetType() == ParameterType::Interest))
            {
               std::shared_ptr<Parameter> par_ptr = par.second;
               double sum_k = 0;
               auto findobs = Sample_Map.find(obs.second);
               if (findobs != Sample_Map.end())
               {
                  for (auto &samp : findobs->second)
                  {
                     auto findobs2 = Variation_Map.find(obs.second);
                     if (findobs2 != Variation_Map.end())
                     {
                        auto findsample2 = findobs2->second.find(samp.second);
                        if (findsample2 != findobs2->second.end())
                        {
                           for (auto &cate : findsample2->second)
                           {
                              for (int i = 0; i < cate.first->GetNParameters(); i++)
                              {
                                 if (cate.first->GetParameter(i) == par_ptr)
                                 {
                                    double k = 0;

                                    // Average values
                                    double ax = 0;
                                    double ay = 0;
                                    for (int v = 0; v < Variation_Sigma_Map[obs.second][samp.second][cate.first].size(); v++)
                                    {
                                       ax = ax + Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i);
                                       ay = ay + Variation_Map[obs.second][samp.second][cate.first][v]->GetValue(index);
                                    }
                                    ax = ax / Variation_Sigma_Map[obs.second][samp.second][cate.first].size();
                                    ay = ay / Variation_Sigma_Map[obs.second][samp.second][cate.first].size();

                                    double top = 0;
                                    double bottom = 0;
                                    for (int v = 0; v < Variation_Sigma_Map[obs.second][samp.second][cate.first].size(); v++)
                                    {
                                       top = top + (Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i) - ax) * (Variation_Map[obs.second][samp.second][cate.first][v]->GetValue(index) - ay);
                                       bottom = bottom + pow(Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i) - ax, 2);
                                    }

                                    k = top / bottom;

                                    sum_k = sum_k + k;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               if (!isfinite(sum_k))
                  MSGUser()->MSG_INFO("Sensitivity : ", sum_k);
               if (par.second->GetType() == ParameterType::Nuisance)
               {
                  Gamma_Matrix(bin_index, par_index) = sum_k;
                  par_index++;
               }
               if (par.second->GetType() == ParameterType::Interest)
               {
                  H_Matrix(bin_index, poi_index) = sum_k;
                  poi_index++;
               }
            }

            if (par.second->GetType() == ParameterType::Normalize)
            {
               std::shared_ptr<Parameter> par_ptr = par.second;
               double sum_k = 0;
               auto findobs = Sample_Map.find(obs.second);
               if (findobs != Sample_Map.end())
               {
                  for (auto &samp : findobs->second)
                  {
                     if (samp.second->GetNormalizationParameter() == par_ptr)
                        sum_k = sum_k + samp.second->GetValue(index);
                  }
               }

               if (!isfinite(sum_k))
                  MSGUser()->MSG_INFO("Sensitivity : ", sum_k);

               H_Matrix(bin_index, poi_index) = sum_k;
               poi_index++;
            }
         }

         for (int i = 0; i < Total_Template_BinNum; i++)
         {
            V_Matrix(bin_index, i) = 0;
            V_Matrix(i, bin_index) = 0;
         }
         double stat_err = pow(obs.second->GetError(index), 2);
         double y0 = obs.second->GetValue(index);
         auto findobs = Sample_Map.find(obs.second);
         if (findobs != Sample_Map.end())
         {
            for (auto &samp : findobs->second)
            {
               stat_err = stat_err + pow(samp.second->GetError(index), 2);
               y0 = y0 - samp.second->GetValue(index);
            }
         }
         V_Matrix(bin_index, bin_index) = stat_err;
         Y0_Matrix(bin_index, 0) = y0;
         if (!isfinite(stat_err))
            MSGUser()->MSG_INFO("Stat_Err^2 : ", stat_err);

         bin_index++;
      }
   }

   Q_Matrix = (Eigen::MatrixXd::Identity(NP_Num, NP_Num) + Gamma_Matrix.transpose() * V_Matrix.inverse() * Gamma_Matrix).inverse() * (Gamma_Matrix.transpose() * V_Matrix.inverse());
   S_Matrix = V_Matrix.inverse() * (Eigen::MatrixXd::Identity(Total_Template_BinNum, Total_Template_BinNum) - Gamma_Matrix * Q_Matrix);
   C_Matrix = V_Matrix + Gamma_Matrix * Gamma_Matrix.transpose();
   Lambda_Matrix = (-1) * (H_Matrix.transpose() * S_Matrix * H_Matrix).inverse() * H_Matrix.transpose() * S_Matrix;
   POI_Shift = Lambda_Matrix * Y0_Matrix;
   Nuisance_Shift = Q_Matrix * (Eigen::MatrixXd::Identity(Total_Template_BinNum, Total_Template_BinNum) + H_Matrix * Lambda_Matrix) * Y0_Matrix;

   POI_Cov_Matrix = (H_Matrix.transpose() * S_Matrix * H_Matrix).inverse();

   Rho_Matrix = (H_Matrix.transpose() * V_Matrix.inverse() * H_Matrix).inverse() * (H_Matrix.transpose() * V_Matrix.inverse());
   Epsilon_Matrix = H_Matrix * Rho_Matrix - Eigen::MatrixXd::Identity(Total_Template_BinNum, Total_Template_BinNum);
   Nuisance_Cov_Matrix = (Eigen::MatrixXd::Identity(NP_Num, NP_Num) + (Epsilon_Matrix * Gamma_Matrix).transpose() * V_Matrix.inverse() * (Epsilon_Matrix * Gamma_Matrix)).inverse();

   poi_index = 0;
   par_index = 0;
   for (auto &par : Par_Map)
   {
      if (par.second->IsFixed())
         continue;
      if (par.second->GetType() == ParameterType::Nuisance)
      {
         par.second->SetValue(Nuisance_Shift(par_index, 0));
         par.second->SetError(sqrt(Nuisance_Cov_Matrix(par_index, par_index)));
         par_index++;
      }
      if (par.second->GetType() == ParameterType::Normalize)
      {
         par.second->SetValue(par.second->GetNominal() - POI_Shift(poi_index, 0));
         par.second->SetError(sqrt(POI_Cov_Matrix(poi_index, poi_index)));
         poi_index++;
      }
      if (par.second->GetType() == ParameterType::Interest)
      {
         par.second->SetValue(par.second->GetNominal() - POI_Shift(poi_index, 0));
         par.second->SetError(sqrt(POI_Cov_Matrix(poi_index, poi_index)));
         poi_index++;
      }
   }
#endif

#ifdef KRONOS

   std::vector<TString> Syst_Name;
   std::vector<TString> POI_Name;

   int Total_Template_BinNum = 0;
   for (auto &obs : Obs_Map)
      Total_Template_BinNum = Total_Template_BinNum + obs.second->GetNBins();

   int NP_Num = 0;
   int POI_Num = 0;
   for (auto &par : Par_Map)
   {
      if (par.second->IsFixed())
         continue;

      if (par.second->GetType() == ParameterType::Interest)
      {
         POI_Num++;
         POI_Name.push_back(par.second->GetName());
      }
      if (par.second->GetType() == ParameterType::Nuisance)
      {
         NP_Num++;
         Syst_Name.push_back(par.second->GetName());
      }
      if (par.second->GetType() == ParameterType::Normalize)
      {
         POI_Num++;
         POI_Name.push_back(par.second->GetName());
      }
   }
   TMatrixD Gamma_Matrix(Total_Template_BinNum, NP_Num);
   TMatrixD V_Matrix(Total_Template_BinNum, Total_Template_BinNum);
   TMatrixD H_Matrix(Total_Template_BinNum, POI_Num);
   TVectorD T0_Vector(Total_Template_BinNum);
   TVectorD Y0_Vector(Total_Template_BinNum);

   int bin_index = 0;
   int par_index = 0;
   int poi_index = 0;
   for (auto &obs : Obs_Map)
   {
      for (int index = 0; index < obs.second->GetNBins(); index++)
      {
         par_index = 0;
         poi_index = 0;
         for (auto &par : Par_Map)
         {
            if (par.second->IsFixed())
               continue;

            if ((par.second->GetType() == ParameterType::Nuisance) ||
                  (par.second->GetType() == ParameterType::Interest))
            {
               std::shared_ptr<Parameter> par_ptr = par.second;
               double sum_k = 0;
               auto findobs = Sample_Map.find(obs.second);
               if (findobs != Sample_Map.end())
               {
                  for (auto &samp : findobs->second)
                  {
                     auto findobs2 = Variation_Map.find(obs.second);
                     if (findobs2 != Variation_Map.end())
                     {
                        auto findsample2 = findobs2->second.find(samp.second);
                        if (findsample2 != findobs2->second.end())
                        {
                           for (auto &cate : findsample2->second)
                           {
                              for (int i = 0; i < cate.first->GetNParameters(); i++)
                              {
                                 if (cate.first->GetParameter(i) == par_ptr)
                                 {
                                    double k = 0;

                                    // Average values
                                    double ax = 0;
                                    double ay = 0;
                                    for (int v = 0; v < Variation_Sigma_Map[obs.second][samp.second][cate.first].size(); v++)
                                    {
                                       ax = ax + Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i);
                                       ay = ay + Variation_Map[obs.second][samp.second][cate.first][v]->GetValue(index);
                                    }
                                    ax = ax / Variation_Sigma_Map[obs.second][samp.second][cate.first].size();
                                    ay = ay / Variation_Sigma_Map[obs.second][samp.second][cate.first].size();

                                    double top = 0;
                                    double bottom = 0;
                                    for (int v = 0; v < Variation_Sigma_Map[obs.second][samp.second][cate.first].size(); v++)
                                    {
                                       top = top + (Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i) - ax) * (Variation_Map[obs.second][samp.second][cate.first][v]->GetValue(index) - ay);
                                       bottom = bottom + pow(Variation_Sigma_Map[obs.second][samp.second][cate.first][v]->GetValue(i) - ax, 2);
                                    }

                                    k = top / bottom;

                                    sum_k = sum_k + k;
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               if (!isfinite(sum_k))
                  MSGUser()->MSG_INFO("Sensitivity : ", sum_k);
               if (par.second->GetType() == ParameterType::Nuisance)
               {
                  Gamma_Matrix(bin_index, par_index) = sum_k;
                  par_index++;
               }
               if (par.second->GetType() == ParameterType::Interest)
               {
                  H_Matrix(bin_index, poi_index) = sum_k;
                  poi_index++;
               }
            }

            if (par.second->GetType() == ParameterType::Normalize)
            {
               std::shared_ptr<Parameter> par_ptr = par.second;
               double sum_k = 0;
               auto findobs = Sample_Map.find(obs.second);
               if (findobs != Sample_Map.end())
               {
                  for (auto &samp : findobs->second)
                  {
                     if (samp.second->GetNormalizationParameter() == par_ptr)
                        sum_k = sum_k + samp.second->GetValue(index);
                  }
               }

               if (!isfinite(sum_k))
                  MSGUser()->MSG_INFO("Sensitivity : ", sum_k);

               H_Matrix(bin_index, poi_index) = sum_k;
               poi_index++;
            }
         }

         for (int i = 0; i < Total_Template_BinNum; i++)
         {
            V_Matrix(bin_index, i) = 0;
            V_Matrix(i, bin_index) = 0;
         }
         double stat_err = pow(obs.second->GetError(index), 2);
         double y0 = obs.second->GetValue(index);
         auto findobs = Sample_Map.find(obs.second);
         if (findobs != Sample_Map.end())
         {
            for (auto &samp : findobs->second)
            {
               stat_err = stat_err + pow(samp.second->GetError(index), 2);
               y0 = y0 - samp.second->GetValue(index);
            }
         }
         V_Matrix(bin_index, bin_index) = stat_err;
         Y0_Vector(bin_index) = y0;
         T0_Vector(bin_index) = 0;
         if (!isfinite(stat_err))
            MSGUser()->MSG_INFO("Stat_Err^2 : ", stat_err);

         bin_index++;
      }
   }

   kfitter = new KronosFit();
   MSGUser()->MSG_INFO("Initialize Kronos Fit");
   kfitter->InitializeFitModel(Gamma_Matrix, V_Matrix, H_Matrix, Y0_Vector, T0_Vector, Syst_Name, POI_Name, "stat+sys", false, true);
   MSGUser()->MSG_INFO("Perform the fit");
   kfitter->PerformTFit();

   std::vector<TString> POI_Result_Names;
   std::vector<double> POI_Result_Values;
   kfitter->GetPOIFitResults(POI_Result_Names, POI_Result_Values);

   std::vector<TString> NP_Result_Names;
   std::vector<double> NP_Result_Values;
   kfitter->GetNPsFitResults(NP_Result_Names, NP_Result_Values);

   for (int i = 0; i < POI_Result_Names.size(); i++)
   {
      if (POI_Result_Names[i].TString::BeginsWith("Total Unc"))
      {
         TString name = (POI_Result_Names[i])(18, POI_Result_Names[i].Length() - 1);
         std::shared_ptr<Parameter> par_ptr = GetParameter(name);
         par_ptr->SetError(POI_Result_Values[i]);
      }
      else if (POI_Result_Names[i].TString::BeginsWith("Best value"))
      {
         std::shared_ptr<Parameter> par_ptr = GetParameter(POI_Result_Names[i](19, POI_Result_Names[i].Length() - 1));
         par_ptr->SetValue(par_ptr->GetNominal() + POI_Result_Values[i]);
      }
   }
   for (int i = 0; i < NP_Result_Names.size(); i++)
   {
      if (NP_Result_Names[i].TString::BeginsWith("total Unc"))
      {
         TString name = (NP_Result_Names[i])(16, NP_Result_Names[i].Length() - 1);
         std::shared_ptr<Parameter> par_ptr = GetParameter(name);
         par_ptr->SetError(NP_Result_Values[i]);
      }
      else
      {
         std::shared_ptr<Parameter> par_ptr = GetParameter(NP_Result_Names[i]);
         par_ptr->SetValue(NP_Result_Values[i]);
      }
   }

#endif
}

#ifdef KRONOS
KronosFit* Fitter::GetKronosFitter()
{
   return kfitter;
}
#endif

std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString obsname_y)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Observable> obs_ptr_x = GetObservable(obsname_x);
   std::shared_ptr<Observable> obs_ptr_y = GetObservable(obsname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(obs_ptr_x, obs_ptr_y);
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(obs_ptr_y, obs_ptr_x);
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Obs_Map.find(obs_ptr_x);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_y);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), " and ", obsname_y.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_x, emp_map));
   }

   findx = Corr_Obs_Map.find(obs_ptr_y);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_x);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_y.TString::Data(), " and ", obsname_x.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_y, emp_map));
   }
   return newcorr;
}
std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString obsname_y, Eigen::MatrixXd m)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Observable> obs_ptr_x = GetObservable(obsname_x);
   std::shared_ptr<Observable> obs_ptr_y = GetObservable(obsname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(obs_ptr_x, obs_ptr_y, m);
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(obs_ptr_y, obs_ptr_x, m.transpose());
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Obs_Map.find(obs_ptr_x);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_y);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), " and ", obsname_y.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_x, emp_map));
   }

   findx = Corr_Obs_Map.find(obs_ptr_y);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_x);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_y.TString::Data(), " and ", obsname_x.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_y, emp_map));
   }
   return newcorr;
}
std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString obsname_y, TH2D* m)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Observable> obs_ptr_x = GetObservable(obsname_x);
   std::shared_ptr<Observable> obs_ptr_y = GetObservable(obsname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(obs_ptr_x, obs_ptr_y, m);
   Eigen::MatrixXd tm = Eigen::MatrixXd(m->GetYaxis()->GetNbins(), m->GetXaxis()->GetNbins());
   for (int i = 0; i < m->GetYaxis()->GetNbins(); i++)
   {
      for (int j = 0; j < m->GetXaxis()->GetNbins(); j++)
         tm(i, j) = m->GetBinContent(j + 1, i + 1);
   }
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(obs_ptr_y, obs_ptr_x, tm);
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Obs_Map.find(obs_ptr_x);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_y);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), " and ", obsname_y.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_y, newcorr));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_x, emp_map));
   }

   findx = Corr_Obs_Map.find(obs_ptr_y);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_x);
      if (findy != findx->second.end())
         MSGUser()->MSG_ERROR("Correlation between ", obsname_y.TString::Data(), " and ", obsname_x.TString::Data(), " has already booked!");
      else
         findx->second.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>(obs_ptr_x, newcorrT));
      Corr_Obs_Map.emplace(std::pair<std::shared_ptr<Observable>, std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>(obs_ptr_y, emp_map));
   }
   return newcorr;
}
std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Sample> samp_ptr_x = GetSample(obsname_x, sampname_x);
   std::shared_ptr<Sample> samp_ptr_y = GetSample(obsname_y, sampname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(samp_ptr_x, samp_ptr_y);
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(samp_ptr_y, samp_ptr_x);
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Sample_Map.find(samp_ptr_x);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_y);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_x, emp_map));
   }

   findx = Corr_Sample_Map.find(samp_ptr_y);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_x);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_y, emp_map));
   }
   return newcorr;
}
std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y, Eigen::MatrixXd m)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Sample> samp_ptr_x = GetSample(obsname_x, sampname_x);
   std::shared_ptr<Sample> samp_ptr_y = GetSample(obsname_y, sampname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(samp_ptr_x, samp_ptr_y, m);
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(samp_ptr_y, samp_ptr_x, m.transpose());
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Sample_Map.find(samp_ptr_x);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_y);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_x, emp_map));
   }

   findx = Corr_Sample_Map.find(samp_ptr_y);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_x);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_y, emp_map));
   }
   return newcorr;
}
std::shared_ptr<Correlation> Fitter::BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y, TH2D* m)
{
   FIT_WITH_CORRELATION = true;
   std::shared_ptr<Sample> samp_ptr_x = GetSample(obsname_x, sampname_x);
   std::shared_ptr<Sample> samp_ptr_y = GetSample(obsname_y, sampname_y);
   std::shared_ptr<Correlation> newcorr = std::make_shared<Correlation>(samp_ptr_x, samp_ptr_y, m);
   Eigen::MatrixXd tm = Eigen::MatrixXd(m->GetYaxis()->GetNbins(), m->GetXaxis()->GetNbins());
   for (int i = 0; i < m->GetYaxis()->GetNbins(); i++)
   {
      for (int j = 0; j < m->GetXaxis()->GetNbins(); j++)
         tm(i, j) = m->GetBinContent(j + 1, i + 1);
   }
   std::shared_ptr<Correlation> newcorrT = std::make_shared<Correlation>(samp_ptr_y, samp_ptr_x, tm);
   newcorr->SetTransposeCorrelation(newcorrT);
   newcorrT->SetTransposeCorrelation(newcorr);
   auto findx = Corr_Sample_Map.find(samp_ptr_x);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_y);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_y, newcorr));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_x, emp_map));
   }

   findx = Corr_Sample_Map.find(samp_ptr_y);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_x);
      if (findy != findx->second.end())
      {
         MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                              " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " has already booked!");
      }
      else
         findx->second.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
   }
   else
   {
      std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>> emp_map;
      emp_map.emplace(std::pair<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>(samp_ptr_x, newcorrT));
      Corr_Sample_Map.emplace(std::pair<std::shared_ptr<Sample>, std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>(samp_ptr_y, emp_map));
   }

   return newcorr;
}
std::shared_ptr<Correlation> Fitter::GetCorrelation(TString obsname_x, TString obsname_y)
{
   std::shared_ptr<Observable> obs_ptr_x = GetObservable(obsname_x);
   std::shared_ptr<Observable> obs_ptr_y = GetObservable(obsname_y);

   auto findx = Corr_Obs_Map.find(obs_ptr_x);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_ptr_y);
      if (findy != findx->second.end())
         return findy->second;
   }
   MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), " and ", obsname_y.TString::Data(), " not defined");
   return nullptr;
}
std::shared_ptr<Correlation> Fitter::GetCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y)
{
   std::shared_ptr<Sample> samp_ptr_x = GetSample(obsname_x, sampname_x);
   std::shared_ptr<Sample> samp_ptr_y = GetSample(obsname_y, sampname_y);

   auto findx = Corr_Sample_Map.find(samp_ptr_x);
   if (findx != Corr_Sample_Map.end())
   {
      auto findy = findx->second.find(samp_ptr_y);
      if (findy != findx->second.end())
         return findy->second;
   }
   MSGUser()->MSG_ERROR("Correlation between ", obsname_x.TString::Data(), "   ", sampname_x.TString::Data(),
                        " and ", obsname_y.TString::Data(), "   ", sampname_y.TString::Data(), " not defined");
   return nullptr;
}

void Fitter::InitializeCorrelation()
{
   if (FIT_WITH_CORRELATION)
   {
      if (FIT_NEED_STAT_GAMMA)
      {
         MSGUser()->MSG_INFO("Correlation has been defined! Cannot use Possion now! So StatGamma cannot be defined!");
         FIT_NEED_STAT_GAMMA = false;
      }

      MSGUser()->MSG_INFO("//------------------------//");
      MSGUser()->MSG_INFO("// Initialize Correlation //");
      MSGUser()->MSG_INFO("//------------------------//");

      std::vector<std::shared_ptr<Observable>> obs_temp_vec;
      for (auto &obs : Obs_Map)
         obs_temp_vec.push_back(obs.second);

      while (obs_temp_vec.size() > 0)
      {
         std::vector<std::shared_ptr<Observable>> obs_vec;
         if (obs_vec.size() == 0)
         {
            obs_vec.push_back(obs_temp_vec[0]);
            obs_temp_vec.erase(obs_temp_vec.begin());
         }

         if (obs_temp_vec.size() > 0)
         {
            for (int i = 0; i < obs_temp_vec.size(); i++)
            {
               bool isnewobs = false;
               for (int j = 0; j < obs_vec.size(); j++)
               {
                  if (IsCorrelated(obs_temp_vec[i], obs_vec[j]))
                     isnewobs = true;
               }

               if (isnewobs)
               {
                  obs_vec.push_back(obs_temp_vec[i]);
                  obs_temp_vec.erase(obs_temp_vec.begin() + i);
                  i = i - 1;
               }
            }
         }

         int nbins = 0;
         for (int i = 0; i < obs_vec.size(); i++)
            nbins = nbins + obs_vec[i]->GetNBins();
         MSGUser()->MSG_INFO("Total Bin num: ", nbins);

         Eigen::MatrixXd corr = Eigen::MatrixXd::Zero(nbins, nbins);

         int index_x = 0;
         int index_y = 0;
         for (int i = 0; i < obs_vec.size(); i++)
         {
            for (int j = 0; j < obs_vec[i]->GetNBins(); j++)
            {
               index_y = 0;
               for (int k = 0; k < obs_vec.size(); k++)
               {
                  for (int l = 0; l < obs_vec[k]->GetNBins(); l++)
                  {
                     // Get Data Covariance
                     auto findx = Corr_Obs_Map.find(obs_vec[i]);
                     if (findx != Corr_Obs_Map.end())
                     {
                        auto findy = findx->second.find(obs_vec[k]);
                        if (findy != findx->second.end())
                           corr(index_x, index_y) = corr(index_x, index_y) + (findy->second->GetCovariance())(j, l);
                        else
                        {
                           if ((obs_vec[i] == obs_vec[k]) && (j == l))
                              corr(index_x, index_y) = corr(index_x, index_y) + pow(obs_vec[i]->GetError(j), 2);
                        }
                     }
                     else
                     {
                        if ((obs_vec[i] == obs_vec[k]) && (j == l))
                           corr(index_x, index_y) = corr(index_x, index_y) + pow(obs_vec[i]->GetError(j), 2);
                     }

                     // Get MC Covariance
                     auto findx2 = Sample_Map.find(obs_vec[i]);
                     if (findx2 != Sample_Map.end())
                     {
                        for (auto &samp_x2 : Sample_Map[obs_vec[i]])
                        {
                           auto findy2 = Sample_Map.find(obs_vec[k]);
                           if (findy2 != Sample_Map.end())
                           {
                              for (auto &samp_y2 : Sample_Map[obs_vec[k]])
                              {
                                 // Loop over all the combinations of the samples
                                 auto findsamp_x = Corr_Sample_Map.find(samp_x2.second);
                                 if (findsamp_x != Corr_Sample_Map.end())
                                 {
                                    auto findsamp_y = findsamp_x->second.find(samp_y2.second);
                                    if (findsamp_y != findsamp_x->second.end())
                                    {
                                       // Find the correlation record
                                       if ((obs_vec[i] == obs_vec[k]) && (j == l)) // for the same bin
                                       {
                                          if (samp_x2.second == samp_y2.second) // the same sample
                                             corr(index_x, index_y) = corr(index_x, index_y) + (findsamp_y->second->GetCovariance())(j, l);
                                          else // Different sample Var(X1+X2) = Var(X1) + Var(X2) + 2 * Cov(X1, X2)
                                          {
                                             corr(index_x, index_y) = corr(index_x, index_y) + (findsamp_y->second->GetCovariance())(j, l);
                                             auto findsamp_self = findsamp_x->second.find(samp_x2.second);
                                             if (findsamp_self != findsamp_x->second.end())
                                                corr(index_x, index_y) = corr(index_x, index_y) + (findsamp_self->second->GetCovariance())(j, j);
                                             else
                                                corr(index_x, index_y) = corr(index_x, index_y) + pow(samp_x2.second->GetError(j), 2);
                                          }
                                       }
                                       else // for different bins
                                          corr(index_x, index_y) = corr(index_x, index_y) + (findsamp_y->second->GetCovariance())(j, l);
                                    }
                                    else
                                    {
                                       if ((obs_vec[i] == obs_vec[k]) && (j == l) && (samp_x2.second == samp_y2.second))
                                          corr(index_x, index_y) = corr(index_x, index_y) + pow(samp_x2.second->GetError(j), 2);
                                    }
                                 }
                                 else
                                 {
                                    if ((obs_vec[i] == obs_vec[k]) && (j == l) && (samp_x2.second == samp_y2.second))
                                       corr(index_x, index_y) = corr(index_x, index_y) + pow(samp_x2.second->GetError(j), 2);
                                 }
                              }
                           }
                        }
                     }

                     index_y = index_y + 1;
                  }
               }
               index_x = index_x + 1;
            }
         }
         std::shared_ptr<CorrelationCategory> cate_ptr = std::make_shared<CorrelationCategory>(obs_vec, corr);
         Corr_Cate_Vec.push_back(cate_ptr);
         MSGUser()->MSG_INFO("Book Correlation Category ", Corr_Cate_Vec.size() - 1);
         for (int i = 0; i < cate_ptr->GetNObservable(); i++)
            MSGUser()->MSG_INFO("     -", cate_ptr->GetObservable(i)->GetName().TString::Data());
      }

      MSGUser()->MSG_INFO("//-------------------------//");
      MSGUser()->MSG_INFO("// Correlation Initialized //");
      MSGUser()->MSG_INFO("//-------------------------//");
   }
   else
      MSGUser()->MSG_INFO("No Correlation is defined, InitializeCorrelation did nothing!");
}

bool Fitter::IsCorrelated(TString obsname_x, TString obsname_y)
{
   std::shared_ptr<Observable> obs_ptr_x = GetObservable(obsname_x);
   std::shared_ptr<Observable> obs_ptr_y = GetObservable(obsname_y);

   return IsCorrelated(obs_ptr_x, obs_ptr_y);
}

bool Fitter::IsCorrelated(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y)
{
   auto findx = Corr_Obs_Map.find(obs_x);
   if (findx != Corr_Obs_Map.end())
   {
      auto findy = findx->second.find(obs_y);
      if (findy != findx->second.end())
         return true;
   }

   for (auto &samp_x : Corr_Sample_Map)
   {
      std::shared_ptr<Observable> obx = samp_x.first->RetriveObservable();
      for (auto &samp_y : samp_x.second)
      {
         std::shared_ptr<Observable> oby = samp_y.first->RetriveObservable();
         if ((obx == obs_x) && (oby == obs_y))
            return true;
      }
   }

   if (obs_x == obs_y)
      return true;
   else
      return false;
}

void Fitter::GetFitResult(std::shared_ptr<FitResult> res)
{
   for (auto &par : Par_Map)
      res->SaveResult(par.first, par.second->GetValue(), par.second->GetError());
}

void Fitter::SaveWorkSpace(std::shared_ptr<WorkSpace> ws)
{
   ws->SaveParameters(&Par_Map, &Morph_Map);
   ws->SaveObservables(&Obs_Map);
   ws->SaveSamples(&Sample_Map);
   ws->SaveVariations(&Variation_Sigma_Map);
   ws->SaveCorrelations(&Corr_Obs_Map, &Corr_Sample_Map);
}

void Fitter::SaveFigures()
{
   for (auto &obs : Variation_Map)
   {
      for (auto &samp : obs.second)
      {
         for (auto &cate : samp.second)
         {
            // Find Nominal Variation
            std::vector<double> nom_val = cate.first->GetParameterNominals();
            std::shared_ptr<Variation> Nom_Var = nullptr;
            for (int i = 0; i < cate.second.size(); i++)
            {
               if ((nom_val.size()) != (Variation_Sigma_Map[obs.first][samp.first][cate.first][i]->GetNValues()))
                  continue;
               else
               {
                  bool allequal = true;
                  for (int j = 0; j < nom_val.size(); j++)
                  {
                     if (fabs(nom_val[j] - Variation_Sigma_Map[obs.first][samp.first][cate.first][i]->GetValue(j)) > FIT_PARAMETER_PRECISION)
                     {
                        allequal = false;
                        break;
                     }
                  }
                  if (allequal)
                     Nom_Var = (cate.second)[i];
               }
            }

            if (Nom_Var == nullptr)
            {
               MSGUser()->MSG_ERROR("Nominal Variation not found!");
               continue;
            }

            std::shared_ptr<NAGASH::NGFigure> figure = std::make_shared<NAGASH::NGFigure>(MSGUser(),
                                                                                          TString::Format("RatioPlot_%s_%s_%s.eps", obs.first->GetName().TString::Data(), samp.first->GetName().TString::Data(), cate.first->GetName().TString::Data()).TString::Data(),
                                                                                          "Bin Index", "#frac{Variation}{Nominal}");
            figure->SetMode("MULTI");
            std::vector<TH1D*> myhist_vec;
            for (int i = 0; i < cate.second.size(); i++)
            {
               TH1D* hist = new TH1D(TString::Format("_temp_%d", i).TString::Data(), TString::Format("_temp_%d", i).TString::Data(), obs.first->GetNBins(), 0, obs.first->GetNBins());
               myhist_vec.push_back(hist);
               for (int j = 0; j < obs.first->GetNBins(); j++)
               {
                  hist->SetBinContent(j + 1, (cate.second)[i]->GetValue(j) / Nom_Var->GetValue(j));
                  hist->SetBinError(j + 1, 0);
               }
               figure->SetInputHist(hist, TString::Format("%s = %f", cate.first->GetName().TString::Data(), Variation_Sigma_Map[obs.first][samp.first][cate.first][i]->GetValueX()).TString::Data());
            }
            figure->DrawFigure();
            for (int i = 0; i < myhist_vec.size(); i++)
               delete myhist_vec[i];
            myhist_vec.clear();
         }
      }
   }
}
