#include "Phoenix/FitResult.h"

using namespace Phoenix;
using namespace std;

FitResult::FitResult(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> c, const TString &rname, const TString &fname) : Result(MSG, c, rname, fname)
{
}

void FitResult::SaveResult(TString name, double val, double unc)
{
   auto findval = val_map.find(name);
   if (findval != val_map.end())
   {
      MSGUser()->MSG_ERROR("Result for Parameter ", name.TString::Data(), " has already been saved!");
      return;
   }
   else
   {
      val_map.emplace(std::pair<TString, double>(name, val));
      unc_map.emplace(std::pair<TString, double>(name, unc));
   }
}

double FitResult::GetValue(TString name)
{
   auto findval = val_map.find(name);
   if (findval != val_map.end())
      return findval->second;
   else
   {
      MSGUser()->MSG_ERROR("Result for Parameter ", name.TString::Data(), " has not been found!");
      return -999;
   }
}

double FitResult::GetUncertainty(TString name)
{
   auto findunc = unc_map.find(name);
   if (findunc != unc_map.end())
      return findunc->second;
   else
   {
      MSGUser()->MSG_ERROR("Result for Parameter ", name.TString::Data(), " has not been found!");
      return -999;
   }
}

std::pair<double, double> FitResult::GetResult(TString name)
{
   return std::pair<double, double>(GetValue(name), GetUncertainty(name));
}

void FitResult::WriteToFile()
{
   if (GetOutputFileName() == "")
   {
      MSGUser()->MSG_WARNING("input an empty output file name, will be ignored");
      return;
   }

   TFile* file = new TFile(GetOutputFileName(), "UPDATE");
   file->cd();

   TH1D* temp = new TH1D(GetResultName().TString::Data(),
                         GetResultName().TString::Data(),
                         val_map.size(), 0, val_map.size());
   temp->Sumw2();

   int index = 0;
   for (auto &val : val_map)
   {
      temp->SetBinContent(index + 1, val.second);
      temp->SetBinError(index + 1, unc_map[val.first]);
      temp->GetXaxis()->SetBinLabel(index + 1, val.first.TString::Data());
      index = index + 1;
   }

   temp->Write();

   file->Close();
}
