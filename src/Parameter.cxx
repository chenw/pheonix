#include "Phoenix/Parameter.h"
#include "Phoenix/Morphing2D.h"

using namespace Phoenix;
using namespace std;

double Parameter::GetLikelihood()
{
   if (Type == ParameterType::Nuisance)
   {
      if (WithNPConstrain)
         return (-pow(Value - Nuisance_Expectation, 2) / 2 ) - TMath::Log(sqrt(2 * TMath::Pi()));
      else
         return 0;
   }
   if (Type == ParameterType::StatGamma)
   {
      // return TMath::Log(TMath::Gaus(Value, 1, Gamma_Gaus_Sigma, kTRUE));
      // sqrt(Lambda) / Lambda = Gaus_Sigma
      // Lambda = (1/Gaus_Sigma)^2
      double Lambda = pow(Gamma_Gaus_Sigma, -2);
      double N = Lambda * Value;
      return Lambda * TMath::Log(N) - N - TMath::LnGamma(Lambda + 1.0);
   }
   return 0;
}

double Parameter::GetLikelihood_Gradient()
{
   if (Type == ParameterType::Nuisance)
   {
      if (WithNPConstrain)
         return Value - Nuisance_Expectation;
      else
         return 0;
   }
   if (Type == ParameterType::StatGamma)
   {
      double Lambda = pow(Gamma_Gaus_Sigma, -2);
      return -Lambda / Value + Lambda;
   }
   return 0;
}

void Parameter::SetMinosUncertainty(double up, double down)
{
   Error_Up = up;
   Error_Down = down;
}
void Parameter::SetMinosUncertainty(std::pair<double, double> unc_pair)
{
   Error_Up = unc_pair.first;
   Error_Down = unc_pair.second;
}

bool MorphingRange::Contains(const std::vector<double> &val_vec)
{
   int NDim = Morphing_ptr->GetNParameters();
   if (NDim == 1 && Range_Vec.size() == 2)
   {
      if (Range_Vec[0] == nullptr && Range_Vec[1] != nullptr)
      {
         if (val_vec[0] <= Range_Vec[1]->GetValue())
            return true;
         else
            return false;
      }
      else if (Range_Vec[0] != nullptr && Range_Vec[1] == nullptr)
      {
         if (val_vec[0] > Range_Vec[0]->GetValue())
            return true;
         else
            return false;
      }
      else
      {
         if (val_vec[0] > Range_Vec[0]->GetValue() &&
               val_vec[0] <= Range_Vec[1]->GetValue())
            return true;
         else
            return false;
      }
   }
   else if (NDim == 2 && Range_Vec.size() == 3)
   {
      Phoenix::Point p0(Range_Vec[0]);
      Phoenix::Point p1(Range_Vec[1]);
      Phoenix::Point p2(Range_Vec[2]);

      Phoenix::Point p(val_vec[0], val_vec[1]);
      Phoenix::Triangle tri(p0, p1, p2);

      return p.IsInside(tri);
   }

   return false;
}

Eigen::VectorXd MorphingRange::GetMorphingResult(const std::vector<double> &val_vec)
{
   if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::FULL_LINEAR)
      return GetMorphingResult_1D_FULL_LINEAR(val_vec);
   else if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::POL6_EXP)
      return GetMorphingResult_1D_POL6_EXP(val_vec);
   else if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::TRIANGLE_LINEAR)
      return GetMorphingResult_2D_TRIANGLE_LINEAR(val_vec);
   else
   {
      Eigen::VectorXd Profile = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
      return Profile;
   }
}
std::vector<Eigen::VectorXd> MorphingRange::GetMorphingResult_Gradient(const std::vector<double> &val_vec)
{
   if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::FULL_LINEAR)
      return GetMorphingResult_Gradient_1D_FULL_LINEAR(val_vec);
   else if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::POL6_EXP)
      return GetMorphingResult_Gradient_1D_POL6_EXP(val_vec);
   else if (RetriveMorphingCategory()->GetMorphingMode() == MorphingMode::TRIANGLE_LINEAR)
      return GetMorphingResult_Gradient_2D_TRIANGLE_LINEAR(val_vec);
   else
   {
      std::vector<Eigen::VectorXd> prof_grad;
      return prof_grad;
   }
}

Eigen::VectorXd MorphingRange::GetMorphingResult_2D_TRIANGLE_LINEAR(const std::vector<double> &val_vec)
{
   Eigen::VectorXd Profile = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 2)
      {
         Eigen::VectorXd A;
         Eigen::VectorXd B;
         Eigen::VectorXd C;
         // A * x + B * y + C = Z
         if (Cache_ptr != nullptr)
         {
            A = Cache_ptr->GetCache("A");
            B = Cache_ptr->GetCache("B");
            C = Cache_ptr->GetCache("C");
         }
         else
         {
            Cache_ptr = std::make_shared<Cache>();

            double x0 = Val_Vec[0]->GetValueX();
            double x1 = Val_Vec[1]->GetValueX();
            double x2 = Val_Vec[2]->GetValueX();
            double y0 = Val_Vec[0]->GetValueY();
            double y1 = Val_Vec[1]->GetValueY();
            double y2 = Val_Vec[2]->GetValueY();
            Eigen::VectorXd Z0 = Val_Vec[0]->GetVariation()->GetValue();
            Eigen::VectorXd Z1 = Val_Vec[1]->GetVariation()->GetValue();
            Eigen::VectorXd Z2 = Val_Vec[2]->GetVariation()->GetValue();

            A = (-y1 * Z0 + y2 * Z0 + y0 * Z1 - y2 * Z1 - y0 * Z2 + y1 * Z2) / (x1 * y0 - x2 * y0 - x0 * y1 + x2 * y1 + x0 * y2 - x1 * y2);
            B = (x1 * Z0 - x2 * Z0 - x0 * Z1 + x2 * Z1 + x0 * Z2 - x1 * Z2) / (x1 * y0 - x2 * y0 - x0 * y1 + x2 * y1 + x0 * y2 - x1 * y2);
            C = (x2 * y1 * Z0 - x1 * y2 * Z0 - x2 * y0 * Z1 + x0 * y2 * Z1 + x1 * y0 * Z2 - x0 * y1 * Z2) / (x1 * y0 - x2 * y0 - x0 * y1 + x2 * y1 + x0 * y2 - x1 * y2);

            Cache_ptr->SaveCache("A", A);
            Cache_ptr->SaveCache("B", B);
            Cache_ptr->SaveCache("C", C);
         }
         Profile = val_vec[0] * A + val_vec[1] * B + C;
      }
   }

   return Profile;
}
std::vector<Eigen::VectorXd> MorphingRange::GetMorphingResult_Gradient_2D_TRIANGLE_LINEAR(const std::vector<double> &val_vec)
{
   std::vector< Eigen::VectorXd> prof_grad;
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 2)
      {
         if (Cache_ptr == nullptr)
         {
            std::vector<double> cent_val_vec = GetCentral();
            GetMorphingResult_2D_TRIANGLE_LINEAR(cent_val_vec);
         }

         prof_grad.push_back(Cache_ptr->GetCache("A"));
         prof_grad.push_back(Cache_ptr->GetCache("B"));

      }
   }
   return prof_grad;
}
Eigen::VectorXd MorphingRange::GetMorphingResult_1D_POL6_EXP(const std::vector<double> &val_vec)
{
   Eigen::VectorXd Profile = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 1)
      {
         if (Range_Vec[0] == nullptr)
         {
            if (Val_Vec[0] == nullptr)
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[1]->GetValue() - Val_Vec[2]->GetValue());
               Eigen::VectorXd base;
               if (Cache_ptr != nullptr)
                  base = Cache_ptr->GetCache("Base");
               else
               {
                  Cache_ptr = std::make_shared<Cache>();
                  base = ((2 * Val_Vec[1]->GetVariation()->GetValue() - Val_Vec[2]->GetVariation()->GetValue()).array() / Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
                  Cache_ptr->SaveCache("Base", base);
               }
               return (base.array().pow(top) * Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
            }
            else
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[0]->GetValue() - Val_Vec[1]->GetValue());
               Eigen::VectorXd base;
               if (Cache_ptr != nullptr)
                  base = Cache_ptr->GetCache("Base");
               else
               {
                  Cache_ptr = std::make_shared<Cache>();
                  base = (Val_Vec[0]->GetVariation()->GetValue().array() / Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
                  Cache_ptr->SaveCache("Base", base);
               }
               return (base.array().pow(top) * Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
            }
         }
         else if (Range_Vec[1] == nullptr)
         {
            if (Val_Vec[2] == nullptr)
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[1]->GetValue() - Val_Vec[0]->GetValue());
               Eigen::VectorXd base;
               if (Cache_ptr != nullptr)
                  base = Cache_ptr->GetCache("Base");
               else
               {
                  Cache_ptr = std::make_shared<Cache>();
                  base = ((2 * Val_Vec[1]->GetVariation()->GetValue() - Val_Vec[0]->GetVariation()->GetValue()).array() / Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
                  Cache_ptr->SaveCache("Base", base);
               }
               return (base.array().pow(top) * Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
            }
            else
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[2]->GetValue() - Val_Vec[1]->GetValue());
               Eigen::VectorXd base;
               if (Cache_ptr != nullptr)
                  base = Cache_ptr->GetCache("Base");
               else
               {
                  Cache_ptr = std::make_shared<Cache>();
                  base = (Val_Vec[2]->GetVariation()->GetValue().array() / Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
                  Cache_ptr->SaveCache("Base", base);
               }
               return (base.array().pow(top) * Val_Vec[1]->GetVariation()->GetValue().array()).matrix();
            }
         }
         else
         {
            // f  = 1 + (x-x1) a1 + (x-x1)^2 a2 +  (x-x1)^3 a3 +   (x-x1)^4 a4 +   (x-x1)^5 a5 +   (x-x1)^6 a6
            // f1 =            a1 +  2(x-x1) a2 + 3(x-x1)^2 a3 +  4(x-x1)^3 a4 +  5(x-x1)^4 a5 +  6(x-x1)^5 a6
            // f2 =                        2 a2 + 6(x-x1)   a3 + 12(x-x1)^2 a4 + 20(x-x1)^3 a5 + 30(x-x1)^4 a6

            // u  = (y2/y1)^((x-x1)/(x2-x1))
            // u1 = (y2/y1)^((x-x1)/(x2-x1))  ln(y2/y1) / (x2-x1)
            // u2 = (y2/y1)^((x-x1)/(x2-x1)) (ln(y2/y1) / (x2-x1))^2

            // d  = (y0/y1)^((x1-x)/(x1-x0))
            // d1 = (y0/y1)^((x1-x)/(x1-x0))  ln(y0/y1) / (x1-x0)
            // d2 = (y0/y1)^((x1-x)/(x1-x0)) (ln(y0/y1) / (x1-x0))^2
            Eigen::VectorXd A1;
            Eigen::VectorXd A2;
            Eigen::VectorXd A3;
            Eigen::VectorXd A4;
            Eigen::VectorXd A5;
            Eigen::VectorXd A6;
            if (Cache_ptr != nullptr)
            {
               A1 = Cache_ptr->GetCache("A1");
               A2 = Cache_ptr->GetCache("A2");
               A3 = Cache_ptr->GetCache("A3");
               A4 = Cache_ptr->GetCache("A4");
               A5 = Cache_ptr->GetCache("A5");
               A6 = Cache_ptr->GetCache("A6");
            }
            else
            {

               Cache_ptr = std::make_shared<Cache>();
               A1 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
               A2 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
               A3 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
               A4 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
               A5 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
               A6 = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());

               double X0, X1, X2;
               X1 = Val_Vec[1]->GetValue();
               if (Val_Vec[0] == nullptr)
               {
                  X2 = Val_Vec[2]->GetValue();
                  X0 = 2 * X1 - X2;
               }
               else if (Val_Vec[2] == nullptr)
               {
                  X0 = Val_Vec[0]->GetValue();
                  X2 = 2 * X1 - X0;
               }
               else
               {
                  X0 = Val_Vec[0]->GetValue();
                  X2 = Val_Vec[2]->GetValue();
               }
               double Y0, Y1, Y2;

               for (int i = 0; i < Obs_ptr->GetNBins(); i++)
               {
                  Eigen::MatrixXd R = Eigen::MatrixXd(6, 6);
                  Eigen::VectorXd N = Eigen::VectorXd(6);
                  Y1 = (Val_Vec[1]->GetVariation()->GetValue())(i);
                  if (Val_Vec[0] == nullptr)
                  {
                     Y2 = (Val_Vec[2]->GetVariation()->GetValue())(i);
                     Y0 = 2 * Y1 - Y2;
                  }
                  else if (Val_Vec[2] == nullptr)
                  {
                     Y0 = (Val_Vec[0]->GetVariation()->GetValue())(i);
                     Y2 = 2 * Y1 - Y0;
                  }
                  else
                  {
                     Y0 = (Val_Vec[0]->GetVariation()->GetValue())(i);
                     Y2 = (Val_Vec[2]->GetVariation()->GetValue())(i);
                  }

                  // Eq.1 f|x0 = d|x0
                  // (x0-x1) a1 + (x0-x1)^2 a2 +  (x0-x1)^3 a3 +   (x0-x1)^4 a4 +   (x0-x1)^5 a5 +   (x0-x1)^6 a6 = (y0/y1) - 1
                  R(0, 0) = (X0 - X1);
                  R(0, 1) = pow(X0 - X1, 2);
                  R(0, 2) = pow(X0 - X1, 3);
                  R(0, 3) = pow(X0 - X1, 4);
                  R(0, 4) = pow(X0 - X1, 5);
                  R(0, 5) = pow(X0 - X1, 6);
                  N(0) = Y0 / Y1 - 1;
                  // Eq.2 f|x2 = u|x2
                  // (x2-x1) a1 + (x2-x1)^2 a2 +  (x2-x1)^3 a3 +   (x2-x1)^4 a4 +   (x2-x1)^5 a5 +   (x2-x1)^6 a6 = (y2/y1) - 1
                  R(1, 0) = (X2 - X1);
                  R(1, 1) = pow(X2 - X1, 2);
                  R(1, 2) = pow(X2 - X1, 3);
                  R(1, 3) = pow(X2 - X1, 4);
                  R(1, 4) = pow(X2 - X1, 5);
                  R(1, 5) = pow(X2 - X1, 6);
                  N(1) = Y2 / Y1 - 1;
                  // Eq.3 f1|x0 = d1|x0
                  // a1 +  2(x0-x1) a2 + 3(x0-x1)^2 a3 +  4(x0-x1)^3 a4 +  5(x0-x1)^4 a5 +  6(x0-x1)^5 a6 = (y0/y1)  ln(y0/y1) / (x1-x0)
                  R(2, 0) = 1;
                  R(2, 1) = 2 * (X0 - X1);
                  R(2, 2) = 3 * pow(X0 - X1, 2);
                  R(2, 3) = 4 * pow(X0 - X1, 3);
                  R(2, 4) = 5 * pow(X0 - X1, 4);
                  R(2, 5) = 6 * pow(X0 - X1, 5);
                  N(2) = Y0 / Y1 * TMath::Log(Y0 / Y1) / (X0 - X1);
                  // Eq.4 f1|x2 = u1|x2
                  // a1 +  2(x2-x1) a2 + 3(x2-x1)^2 a3 +  4(x2-x1)^3 a4 +  5(x2-x1)^4 a5 +  6(x2-x1)^5 a6 = (y2/y1)  ln(y2/y1) / (x2-x1)
                  R(3, 0) = 1;
                  R(3, 1) = 2 * (X2 - X1);
                  R(3, 2) = 3 * pow(X2 - X1, 2);
                  R(3, 3) = 4 * pow(X2 - X1, 3);
                  R(3, 4) = 5 * pow(X2 - X1, 4);
                  R(3, 5) = 6 * pow(X2 - X1, 5);
                  N(3) = Y2 / Y1 * TMath::Log(Y2 / Y1) / (X2 - X1);
                  // Eq.5 f2|x0 = d2|x0
                  // 2 a2 + 6(x0-x1)   a3 + 12(x0-x1)^2 a4 + 20(x0-x1)^3 a5 + 30(x0-x1)^4 a6 = (y0/y1) (ln(y0/y1) / (x1-x0))^2
                  R(4, 0) = 0;
                  R(4, 1) = 2;
                  R(4, 2) = 6 * (X0 - X1);
                  R(4, 3) = 12 * pow(X0 - X1, 2);
                  R(4, 4) = 20 * pow(X0 - X1, 3);
                  R(4, 5) = 30 * pow(X0 - X1, 4);
                  N(4) = Y0 / Y1 * pow(TMath::Log(Y0 / Y1) / (X1 - X0), 2);
                  // Eq.6 f2|x2 = u2|x2
                  // 2 a2 + 6(x2-x1)   a3 + 12(x2-x1)^2 a4 + 20(x2-x1)^3 a5 + 30(x2-x1)^4 a6 = (y2/y1) (ln(y2/y1) / (x2-x1))^2
                  R(5, 0) = 0;
                  R(5, 1) = 2;
                  R(5, 2) = 6 * (X2 - X1);
                  R(5, 3) = 12 * pow(X2 - X1, 2);
                  R(5, 4) = 20 * pow(X2 - X1, 3);
                  R(5, 5) = 30 * pow(X2 - X1, 4);
                  N(5) = Y2 / Y1 * pow(TMath::Log(Y2 / Y1) / (X2 - X1), 2);

                  Eigen::VectorXd SubA = R.colPivHouseholderQr().solve(N);
                  A1(i) = SubA(0);
                  A2(i) = SubA(1);
                  A3(i) = SubA(2);
                  A4(i) = SubA(3);
                  A5(i) = SubA(4);
                  A6(i) = SubA(5);
               }
               Cache_ptr->SaveCache("A1", A1);
               Cache_ptr->SaveCache("A2", A2);
               Cache_ptr->SaveCache("A3", A3);
               Cache_ptr->SaveCache("A4", A4);
               Cache_ptr->SaveCache("A5", A5);
               Cache_ptr->SaveCache("A6", A6);
            }

            Eigen::VectorXd P = Eigen::VectorXd::Ones(Obs_ptr->GetNBins());
            P = P + (val_vec[0] - Val_Vec[1]->GetValue()) * A1 +
                pow(val_vec[0] - Val_Vec[1]->GetValue(), 2) * A2 +
                pow(val_vec[0] - Val_Vec[1]->GetValue(), 3) * A3 +
                pow(val_vec[0] - Val_Vec[1]->GetValue(), 4) * A4 +
                pow(val_vec[0] - Val_Vec[1]->GetValue(), 5) * A5 +
                pow(val_vec[0] - Val_Vec[1]->GetValue(), 6) * A6;
            return (Val_Vec[1]->GetVariation()->GetValue().array() * P.array()).matrix();
         }
      }
   }

   return Profile;
}
std::vector<Eigen::VectorXd> MorphingRange::GetMorphingResult_Gradient_1D_POL6_EXP(const std::vector<double> &val_vec)
{
   std::vector<Eigen::VectorXd> prof_grad;
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 1)
      {
         if (Cache_ptr == nullptr)
         {
            std::vector<double> cent_val_vec = GetCentral();
            GetMorphingResult_1D_POL6_EXP(cent_val_vec);
         }

         if (Range_Vec[0] == nullptr)
         {
            if (Val_Vec[0] == nullptr)
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[1]->GetValue() - Val_Vec[2]->GetValue());
               double top_grad = 1.0 / (Val_Vec[1]->GetValue() - Val_Vec[2]->GetValue());
               Eigen::VectorXd base = Cache_ptr->GetCache("Base");
               prof_grad.push_back( (base.array().pow(top) * base.array().log() * top_grad * Val_Vec[1]->GetVariation()->GetValue().array()).matrix());
               return prof_grad;
            }
            else
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[0]->GetValue() - Val_Vec[1]->GetValue());
               double top_grad = 1.0 / (Val_Vec[0]->GetValue() - Val_Vec[1]->GetValue());
               Eigen::VectorXd base = Cache_ptr->GetCache("Base");
               prof_grad.push_back( (base.array().pow(top) * base.array().log() * top_grad * Val_Vec[1]->GetVariation()->GetValue().array()).matrix());
               return prof_grad;
            }
         }
         else if (Range_Vec[1] == nullptr)
         {
            if (Val_Vec[2] == nullptr)
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[1]->GetValue() - Val_Vec[0]->GetValue());
               double top_grad = 1.0 / (Val_Vec[1]->GetValue() - Val_Vec[0]->GetValue());
               Eigen::VectorXd base = Cache_ptr->GetCache("Base");
               prof_grad.push_back( (base.array().pow(top) * base.array().log() * top_grad * Val_Vec[1]->GetVariation()->GetValue().array()).matrix());
               return prof_grad;
            }
            else
            {
               double top = (val_vec[0] - Val_Vec[1]->GetValue()) / (Val_Vec[2]->GetValue() - Val_Vec[1]->GetValue());
               double top_grad = 1.0 / (Val_Vec[2]->GetValue() - Val_Vec[1]->GetValue());
               Eigen::VectorXd base = Cache_ptr->GetCache("Base");
               prof_grad.push_back( (base.array().pow(top) * base.array().log() * top_grad * Val_Vec[1]->GetVariation()->GetValue().array()).matrix());
               return prof_grad;
            }
         }
         else
         {
            // f  = 1 + (x-x1) a1 + (x-x1)^2 a2 +  (x-x1)^3 a3 +   (x-x1)^4 a4 +   (x-x1)^5 a5 +   (x-x1)^6 a6
            // f1 =            a1 +  2(x-x1) a2 + 3(x-x1)^2 a3 +  4(x-x1)^3 a4 +  5(x-x1)^4 a5 +  6(x-x1)^5 a6
            // f2 =                        2 a2 + 6(x-x1)   a3 + 12(x-x1)^2 a4 + 20(x-x1)^3 a5 + 30(x-x1)^4 a6

            // u  = (y2/y1)^((x-x1)/(x2-x1))
            // u1 = (y2/y1)^((x-x1)/(x2-x1))  ln(y2/y1) / (x2-x1)
            // u2 = (y2/y1)^((x-x1)/(x2-x1)) (ln(y2/y1) / (x2-x1))^2

            // d  = (y0/y1)^((x1-x)/(x1-x0))
            // d1 = (y0/y1)^((x1-x)/(x1-x0))  ln(y0/y1) / (x1-x0)
            // d2 = (y0/y1)^((x1-x)/(x1-x0)) (ln(y0/y1) / (x1-x0))^2
            Eigen::VectorXd A1 = Cache_ptr->GetCache("A1");
            Eigen::VectorXd A2 = Cache_ptr->GetCache("A2");
            Eigen::VectorXd A3 = Cache_ptr->GetCache("A3");
            Eigen::VectorXd A4 = Cache_ptr->GetCache("A4");
            Eigen::VectorXd A5 = Cache_ptr->GetCache("A5");
            Eigen::VectorXd A6 = Cache_ptr->GetCache("A6");

            Eigen::VectorXd P = A1 +
                                2 * (val_vec[0] - Val_Vec[1]->GetValue()) * A2 +
                                3 * pow(val_vec[0] - Val_Vec[1]->GetValue(), 2) * A3 +
                                4 * pow(val_vec[0] - Val_Vec[1]->GetValue(), 3) * A4 +
                                5 * pow(val_vec[0] - Val_Vec[1]->GetValue(), 4) * A5 +
                                6 * pow(val_vec[0] - Val_Vec[1]->GetValue(), 5) * A6;
            prof_grad.push_back( (Val_Vec[1]->GetVariation()->GetValue().array() * P.array()).matrix());
            return prof_grad;
         }
      }
   }
   return prof_grad;
}
Eigen::VectorXd MorphingRange::GetMorphingResult_1D_FULL_LINEAR(const std::vector<double> &val_vec)
{
   Eigen::VectorXd Profile = Eigen::VectorXd::Zero(Obs_ptr->GetNBins());
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 1)
      {
         Eigen::VectorXd K;
         if (Cache_ptr != nullptr)
            K = Cache_ptr->GetCache("K");
         else
         {
            Cache_ptr = std::make_shared<Cache>();
            double delta = Val_Vec[1]->GetValue() - Val_Vec[0]->GetValue();
            K = (Val_Vec[1]->GetVariation()->GetValue() - Val_Vec[0]->GetVariation()->GetValue()) / delta;
            Cache_ptr->SaveCache("K", K);
         }
         Profile = K * (val_vec[0] - Val_Vec[0]->GetValue()) + Val_Vec[0]->GetVariation()->GetValue();
      }
   }

   return Profile;
}
std::vector<Eigen::VectorXd> MorphingRange::GetMorphingResult_Gradient_1D_FULL_LINEAR(const std::vector<double> &val_vec)
{
   std::vector<Eigen::VectorXd> prof_grad;
   if (Contains(val_vec))
   {
      int NDim = Morphing_ptr->GetNParameters();
      if (NDim == 1)
      {
         if (Cache_ptr == nullptr)
         {
            std::vector<double> cent_val_vec = GetCentral();
            GetMorphingResult_1D_FULL_LINEAR(cent_val_vec);
         }
         prof_grad.push_back( Cache_ptr->GetCache("K"));
      }
   }
   return prof_grad;
}

std::vector<double> MorphingRange::GetCentral()
{
   std::vector<double> central_vec;
   int NDim = Morphing_ptr->GetNParameters();
   if (NDim == 1)
   {
      if (Range_Vec[0] == nullptr)
         central_vec.push_back(Range_Vec[1]->GetValue(0) - 1);
      else if (Range_Vec[1] == nullptr)
         central_vec.push_back(Range_Vec[0]->GetValue(0) + 1);
      else
         central_vec.push_back((Range_Vec[0]->GetValue(0) + Range_Vec[1]->GetValue(0)) / 2);
   }
   else
   {
      for (int i = 0; i < NDim; i++)
      {
         double sum = 0;
         for (int j = 0; j < Range_Vec.size(); j++)
            sum = sum + Range_Vec[j]->GetValue(i);
         central_vec.push_back(sum / Range_Vec.size());
      }
   }
   return central_vec;
}

TString MorphingCategory::ModeToTString(MorphingMode mode)
{
   TString str = "UNKNOWN";
   if (mode == MorphingMode::UNKNOWN)
      str = "UNKNOWN";
   else if (mode == MorphingMode::FULL_LINEAR)
      str = "FULL_LINEAR";
   else if (mode == MorphingMode::POL6_EXP)
      str = "POL6_EXP";
   else if (mode == MorphingMode::TRIANGLE_LINEAR)
      str = "TRIANGLE_LINEAR";

   return str;
}
MorphingMode MorphingCategory::TStringToMode(TString mode)
{
   if (mode.TString::EqualTo("UNKNOWN"))
      return MorphingMode::UNKNOWN;
   if (mode.TString::EqualTo("FULL_LINEAR"))
      return MorphingMode::FULL_LINEAR;
   if (mode.TString::EqualTo("POL6_EXP"))
      return MorphingMode::POL6_EXP;
   if (mode.TString::EqualTo("TRIANGLE_LINEAR"))
      return MorphingMode::TRIANGLE_LINEAR;

   return MorphingMode::UNKNOWN;
}

TString Parameter::TypeToTString(ParameterType type)
{
   TString str = "UNKNOWN";
   if (type == ParameterType::UNKNOWN)
      str = "UNKNOWN";
   if (type == ParameterType::Interest)
      str = "Interest";
   if (type == ParameterType::Nuisance)
      str = "Nuisance";
   if (type == ParameterType::Normalize)
      str = "Normalize";
   if (type == ParameterType::StatGamma)
      str = "StatGamma";

   return str;
}
ParameterType Parameter::TStringToType(TString type)
{
   if (type.TString::EqualTo("Interest"))
      return ParameterType::Interest;
   if (type.TString::EqualTo("Nuisance"))
      return ParameterType::Nuisance;
   if (type.TString::EqualTo("Normalize"))
      return ParameterType::Normalize;
   if (type.TString::EqualTo("StatGamma"))
      return ParameterType::StatGamma;

   return ParameterType::UNKNOWN;
}
