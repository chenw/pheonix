#include "Phoenix/Correlation.h"

using namespace Phoenix;
using namespace std;

void Correlation::ConfigFrom2DDistribution(TH2D* hist)
{
   if (hist->GetXaxis()->GetNbins() != NBins_X)
      return;
   if (hist->GetYaxis()->GetNbins() != NBins_Y)
      return;

   // Cov(A+B, C+B) = Cov(A,C) + Cov(A,B) + Cov(B,C) + Cov(B, B)
   // = Var(B)
   for (int i = 0; i < NBins_X; i++)
      for (int j = 0; j < NBins_Y; j++)
         cov(i, j) = pow(hist->GetBinError(i + 1, j + 1), 2);

   TCov->Config(cov.transpose());
}

Correlation::Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y, Eigen::MatrixXd m)
{
   Samp_ptr_x = nullptr;
   Samp_ptr_y = nullptr;
   Obs_ptr_x = obs_x;
   Obs_ptr_y = obs_y;
   NBins_X = obs_x->GetNBins();
   NBins_Y = obs_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
   if (m.rows() != NBins_X)
      return;
   if (m.cols() != NBins_Y)
      return;
   cov = m;
}

Correlation::Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y, Eigen::MatrixXd m)
{
   Samp_ptr_x = samp_x;
   Samp_ptr_y = samp_y;
   Obs_ptr_x = samp_x->RetriveObservable();
   Obs_ptr_y = samp_y->RetriveObservable();
   NBins_X = samp_x->GetNBins();
   NBins_Y = samp_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
   if (m.rows() != NBins_X)
      return;
   if (m.cols() != NBins_Y)
      return;
   cov = m;
}

Correlation::Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y, TH2D* hist)
{
   Samp_ptr_x = nullptr;
   Samp_ptr_y = nullptr;
   Obs_ptr_x = obs_x;
   Obs_ptr_y = obs_y;
   NBins_X = obs_x->GetNBins();
   NBins_Y = obs_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
   if (hist->GetXaxis()->GetNbins() != NBins_X)
      return;
   if (hist->GetYaxis()->GetNbins() != NBins_Y)
      return;
   for (int i = 0; i < NBins_X; i++)
      for (int j = 0; j < NBins_Y; j++)
         cov(i, j) = hist->GetBinContent(i + 1, j + 1);
}

Correlation::Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y, TH2D* m)
{
   Samp_ptr_x = samp_x;
   Samp_ptr_y = samp_y;
   Obs_ptr_x = samp_x->RetriveObservable();
   Obs_ptr_y = samp_y->RetriveObservable();
   NBins_X = samp_x->GetNBins();
   NBins_Y = samp_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
   if (m->GetXaxis()->GetNbins() != NBins_X)
      return;
   if (m->GetYaxis()->GetNbins() != NBins_Y)
      return;
   for (int i = 0; i < NBins_X; i++)
      for (int j = 0; j < NBins_Y; j++)
         cov(i, j) = m->GetBinContent(i + 1, j + 1);
}
Correlation::Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y)
{
   Samp_ptr_x = nullptr;
   Samp_ptr_y = nullptr;
   Obs_ptr_x = obs_x;
   Obs_ptr_y = obs_y;
   NBins_X = obs_x->GetNBins();
   NBins_Y = obs_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
}

Correlation::Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y)
{
   Samp_ptr_x = samp_x;
   Samp_ptr_y = samp_y;
   Obs_ptr_x = samp_x->RetriveObservable();
   Obs_ptr_y = samp_y->RetriveObservable();
   NBins_X = samp_x->GetNBins();
   NBins_Y = samp_y->GetNBins();
   cov = Eigen::MatrixXd::Zero(NBins_X, NBins_Y);
}

void Correlation::Config(Eigen::MatrixXd m)
{
   if (m.rows() != NBins_X)
      return;
   if (m.cols() != NBins_Y)
      return;
   cov = m;
}
double Correlation::GetCovariance(int index_x, int index_y)
{
   if (index_x < 0 || index_x >= NBins_X)
      return -999;
   if (index_y < 0 || index_y >= NBins_Y)
      return -999;

   return cov(index_x, index_y);
}

void Correlation::BookCache()
{
   Cache_ptr = std::make_shared<Cache2D>();
   Cache_ptr->SaveCache("Covariance", cov);
}

void Correlation::RecoverModifications()
{
   cov = Cache_ptr->GetCache("Covariance");
}

void Correlation::SetRangeX(int start, int end)
{
   if (start >= 0 && end >= start)
   {
      int newbinnum = end - start + 1;
      Eigen::MatrixXd New_Cov = Eigen::MatrixXd(newbinnum, NBins_Y);

      if (Cache_ptr == nullptr)
         BookCache();
      int index = 0;
      for (int i = start; i <= end; i++)
      {
         for (int j = 0; j < NBins_Y; j++)
            New_Cov(index, j) = cov(i, j);
         index = index + 1;
      }

      cov = New_Cov;
      NBins_X = newbinnum;
   }
}

void Correlation::SetRangeY(int start, int end)
{
   if (start >= 0 && end >= start)
   {
      int newbinnum = end - start + 1;
      Eigen::MatrixXd New_Cov = Eigen::MatrixXd(NBins_X, newbinnum);

      if (Cache_ptr == nullptr)
         BookCache();
      int index = 0;
      for (int i = start; i <= end; i++)
      {
         for (int j = 0; j < NBins_X; j++)
            New_Cov(j, index) = cov(j, i);
         index = index + 1;
      }

      cov = New_Cov;
      NBins_Y = newbinnum;
   }
}

void Correlation::RebinX(int num)
{
   if (GetNBinsX() % num != 0)
      return;
   if (Cache_ptr == nullptr)
      BookCache();

   int newbinnum = GetNBinsX() / num;
   Eigen::MatrixXd New_Cov = Eigen::MatrixXd(newbinnum, NBins_Y);

   // Cov(X1 + X2, Y) = Cov(X1, Y) + Cov(X2, Y)

   double sum_cov = 0;
   int index = 0;
   for (int i = 0; i < GetNBinsY(); i++)
   {
      for (int j = 0; j < GetNBinsX(); j++)
      {
         sum_cov = sum_cov + cov(j, i);
         if ((j + 1) % num == 0)
         {
            New_Cov(index, i) = sum_cov;
            index = index + 1;
            sum_cov = 0;
         }
      }
   }

   cov = New_Cov;
   NBins_X = newbinnum;
}
void Correlation::RebinY(int num)
{
   if (GetNBinsY() % num != 0)
      return;
   if (Cache_ptr == nullptr)
      BookCache();

   int newbinnum = GetNBinsY() / num;
   Eigen::MatrixXd New_Cov = Eigen::MatrixXd(NBins_X, newbinnum);

   // Cov(X1 + X2, Y) = Cov(X1, Y) + Cov(X2, Y)

   double sum_cov = 0;
   int index = 0;
   for (int i = 0; i < GetNBinsX(); i++)
   {
      for (int j = 0; j < GetNBinsY(); j++)
      {
         sum_cov = sum_cov + cov(i, j);
         if ((j + 1) % num == 0)
         {
            New_Cov(i, index) = sum_cov;
            index = index + 1;
            sum_cov = 0;
         }
      }
   }

   cov = New_Cov;
   NBins_Y = newbinnum;
}

CorrelationCategory::CorrelationCategory(std::vector<std::shared_ptr<Observable>> obs_vec, Eigen::MatrixXd corr)
{
   Obs_Vec = obs_vec;
   RCorr = corr.inverse();

   int sum = 0;
   for (int i = 0; i < obs_vec.size(); i++)
      sum = sum + obs_vec[i]->GetNBins();
   NBins = sum;

   rest = TMath::Log(sqrt(pow(2 * TMath::Pi(), GetNBins()) * corr.determinant()));

   Data = Eigen::VectorXd(NBins);
   int index = 0;
   for (int i = 0; i < obs_vec.size(); i++)
   {
      Eigen::VectorXd data_sum = obs_vec[i]->GetValue();
      for (int j = 0; j < obs_vec[i]->GetNBins(); j++)
      {
         Data(index) = data_sum(j);
         index = index + 1;
      }
   }
}
std::shared_ptr<Observable> CorrelationCategory::GetObservable(int index)
{
   if ((index >= 0) && (index < Obs_Vec.size()))
      return Obs_Vec[index];

   return nullptr;
}
double CorrelationCategory::GetLikelihood(Eigen::VectorXd prof)
{
   return -0.5 * (((Data - prof).transpose() * RCorr * (Data - prof))(0, 0)) - rest;
}
double CorrelationCategory::GetLikelihood(std::vector<Eigen::VectorXd> prof_vec)
{
   Eigen::VectorXd prof = Eigen::VectorXd(NBins);
   int index = 0;
   for (int i = 0; i < Obs_Vec.size(); i++)
   {
      for (int j = 0; j < Obs_Vec[i]->GetNBins(); j++)
      {
         prof(index) = prof_vec[i](j);
         index = index + 1;
      }
   }

   return GetLikelihood(prof);
}

std::map<std::shared_ptr<Parameter>, double> CorrelationCategory::GetLikelihood_Gradient(std::vector<Eigen::VectorXd> prof_vec,
                                                                                         std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> prof_grad_vec)
{
   Eigen::VectorXd jprof = Eigen::VectorXd(NBins);
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> jprof_grad;
   int index = 0;
   for (int i = 0; i < Obs_Vec.size(); i++)
   {
      for (int j = 0; j < Obs_Vec[i]->GetNBins(); j++)
      {
         jprof(index) = prof_vec[i](j);
         index = index + 1;
      }
   }
   for (int i = 0; i < Obs_Vec.size(); i++)
   {
      for (auto &par : prof_grad_vec[i])
      {
         // Loop Over All The Parameters
         auto findpar = jprof_grad.find(par.first);
         if (findpar != jprof_grad.end())
            continue;

         Eigen::VectorXd par_grad = Eigen::VectorXd::Zero(NBins);
         index = 0;
         for (int j = 0; j < Obs_Vec.size(); j++)
         {
            auto findpar_sub = prof_grad_vec[j].find(par.first);
            if (findpar_sub != prof_grad_vec[j].end())
            {
               for (int k = 0; k < Obs_Vec[j]->GetNBins(); k++)
               {
                  par_grad(index) = (findpar->second)(k);
                  index = index + 1;
               }
            }
            else
               index = index + Obs_Vec[j]->GetNBins();
         }
         jprof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(par.first, par_grad));
      }
   }

   return GetLikelihood_Gradient(jprof, jprof_grad);
}

std::map<std::shared_ptr<Parameter>, double> CorrelationCategory::GetLikelihood_Gradient(Eigen::VectorXd prof, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad)
{
   // -0.5 * (((Data - prof).transpose() * RCorr * (Data - prof))(0, 0)) - rest;
   // Gradient: 0.5 * (((Data - prof_grad).transpose() * RCorr * (Data - prof))(0, 0)) + 0.5 * (((Data - prof).transpose() * RCorr * (Data - prof_grad))(0, 0))
   std::map<std::shared_ptr<Parameter>, double> lgrad;
   for (auto &par : prof_grad)
   {
      double grad = 0.5 * (((Data - par.second).transpose() * RCorr * (Data - prof))(0, 0)) + 0.5 * (((Data - prof).transpose() * RCorr * (Data - par.second))(0, 0));
      lgrad.emplace(par.first, grad);
   }
   return lgrad;
}
