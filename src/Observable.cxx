#include "Phoenix/Observable.h"
#include <memory>

using namespace std;
using namespace Phoenix;

double Observable::GetLikelihood(Eigen::VectorXd prof)
{
   double prob = 0;
   if (GetNBins() == prof.rows())
   {
      for (int i = 0; i < GetNBins(); i++)
      {
         if (fabs(Value(i)) < 1e-8)
            prob = prob + (-prof(i));
         else if (Value(i) > 0)
         {
            // double p = TMath::PoissonI(Value(i), prof(i));
            // if (p < DBL_MIN)
            // p = DBL_MIN;
            // prob = prob + TMath::Log(p);
            prob = prob + Value(i) * TMath::Log(prof(i)) - prof(i) - TMath::LnGamma(Value(i) + 1.0);
         }
      }
   }
   return prob;
}

std::map<std::shared_ptr<Parameter>, double> Observable::GetLikelihood_Gradient(Eigen::VectorXd prof, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad)
{
   // Likelihood: Sum Over Value(i) * TMath::Log(prof(i)) - prof(i) - TMath::LnGamma(Value(i) + 1.0);
   // This is log(L), what we are using is -log(L)
   // Gradient: - Value(i) / prof(i) * prof_grad(i) + prof_grad(i)
   std::map<std::shared_ptr<Parameter>, double> lgrad;

   for (auto &par : prof_grad)
   {
      double prob = 0;
      if (GetNBins() == prof.rows())
      {
         for (int i = 0; i < GetNBins(); i++)
         {
            if (fabs(Value(i)) < 1e-8)
               prob = prob + par.second(i);
            else if (Value(i) > 0)
               prob = prob - Value(i) / prof(i) * par.second(i) + par.second(i);
         }
      }
      lgrad.emplace(std::pair<std::shared_ptr<Parameter>, double>(par.first, prob));
   }

   return lgrad;
}

double Observable::GetLikelihoodConstPart()
{
   double prob = 0;
   for (int i = 0; i < GetNBins(); i++)
      prob = prob + TMath::LnGamma(Value(i) + 1.0);
   return prob;
}

double Observable::GetLikelihood_Gaus(Eigen::VectorXd prof, Eigen::VectorXd prof_err2)
{
   double prob = 0;
   if ((GetNBins() == prof.rows()) &&
         (GetNBins() == prof_err2.rows()))
   {
      for (int i = 0; i < GetNBins(); i++)
      {
         double sigma2 = prof_err2(i) + pow(Error(i), 2);
         double sigma = sqrt(sigma2);

         prob = prob + (-pow(Value(i) - prof(i), 2) / 2 / sigma2) - TMath::Log(sqrt(2 * TMath::Pi()) * sigma);
      }
   }
   return prob;
}

std::map<std::shared_ptr<Parameter>, double> Observable::GetLikelihood_Gaus_Gradient(Eigen::VectorXd prof, Eigen::VectorXd prof_err2,
                                                                                     std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_err2_grad)
{
   // Log(L) = (-pow(Value(i) - prof(i), 2) / 2 / sigma2) - TMath::Log(sqrt(2 * TMath::Pi()) * sigma)
   // Gradient:
   // The Parameter Only Affect prof(i)
   // - ((Value(i) - prof(i)) / sigma2) * prof_grad(i) = (prof(i) - Value(i))/sigma2 * prof_grad(i)
   // The Parameter Affect Both Add this Term
   // (-pow(Value(i) - prof(i), 2) / 2 / pow(sigma2, 2)) * prof_err2_grad(i) + 1.0 / (sqrt(2 * TMath::Pi()) *sqrt( sigma2 ) ) * sqrt(2 * TMath::Pi()) * 0.5 / sqrt(sigma2) * prof_err2_grad(i)

   std::map<std::shared_ptr<Parameter>, double> lgrad;

   for (auto &par : prof_grad)
   {
      double prob = 0;
      if ((GetNBins() == prof.rows()) &&
            (GetNBins() == prof_err2.rows()))
      {
         auto findpar = prof_err2_grad.find(par.first);
         for (int i = 0; i < GetNBins(); i++)
         {
            double sigma2 = prof_err2(i) + pow(Error(i), 2);
            double sigma = sqrt(sigma2);

            prob = prob + (prof(i) - Value(i)) / sigma2 * par.second(i);
            if (findpar != prof_err2_grad.end())
               prob = prob + ( - pow(prof(i), 2) + 2 * prof(i) * Value(i) + sigma2 - pow(Value(i), 2) ) / (2 * pow(sigma2, 2)) * (findpar->second)(i);
         }

      }

      lgrad.emplace(std::pair<std::shared_ptr<Parameter>, double>(par.first, prob));
   }
   return lgrad;
}

double Observable::GetLikelihoodConstPart_Gaus()
{
   return 0;
}

Eigen::VectorXd Observable::GetProfile_Thread(std::shared_ptr<Observable> tobs,
                                              std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map,
                                              std::map<std::shared_ptr<Observable>,
                                              std::map<std::shared_ptr<Sample>,
                                              std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>* VMR_Map,
                                              std::map<std::shared_ptr<Observable>,
                                              std::map<std::shared_ptr<Sample>,
                                              std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>>* metal_result_map,
                                              bool WithMetal,
                                              bool UseRelative,
                                              bool SumImpact,
                                              bool SeparatePOI)
{
   Eigen::VectorXd Profile = Eigen::VectorXd::Zero(tobs->GetNBins());
   auto findobs = S_Map->find(tobs);
   if (findobs != S_Map->end())
   {
      if (SeparatePOI)
      {
         for (auto &samp : (*S_Map)[tobs])
         {
            // Check Whether this sample is related to an poi
            bool needpoicheck = false;
            for (auto &morph_cate : (*VMR_Map)[tobs][samp.second])
            {
               for (int i = 0; i < morph_cate.first->GetNParameters(); i++)
               {
                  std::shared_ptr<Parameter> par = morph_cate.first->GetParameter(i);
                  if (par->GetType() == ParameterType::Interest)
                     needpoicheck = true;
               }
            }

            Eigen::VectorXd Profile_Sample = samp.second->GetValue();
            Eigen::VectorXd SampleNominal = samp.second->GetValue();
            double norm = 1;
            if (samp.second->HasNormalizationParameter())
               norm = samp.second->GetNormalizationParameter()->GetValue();

            auto findobs2 = VMR_Map->find(tobs);
            if (findobs2 != VMR_Map->end())
            {
               auto findsamp = findobs2->second.find(samp.second);
               if (findsamp != findobs2->second.end())
               {
                  for (auto &morph_cate : (*VMR_Map)[tobs][samp.second])
                  {
                     if (morph_cate.first->GetCache() == nullptr)
                        morph_cate.first->BookCache();

                     // Check Whether Contains POI
                     if (needpoicheck)
                     {
                        bool containpoi = false;
                        for (int i = 0; i < morph_cate.first->GetNParameters(); i++)
                        {
                           std::shared_ptr<Parameter> par = morph_cate.first->GetParameter(i);
                           if (par->GetType() == ParameterType::Interest)
                              containpoi = true;
                        }
                        if (!containpoi)
                           continue;
                     }

                     int npar = morph_cate.first->GetNParameters();
                     std::vector<double> val_vec = morph_cate.first->GetParameterValues();
                     bool getvariation = false;
                     Eigen::VectorXd Var;
                     Eigen::VectorXd Impact;
                     for (int i = 0; i < morph_cate.second.size(); i++)
                     {
                        if (morph_cate.second[i]->Contains(val_vec))
                        {
                           if (needpoicheck)
                           {
                              std::vector<Eigen::VectorXd> Profiled_Variations;
                              int nvar = val_vec.size() + 1;
                              for (int j = 0; j < nvar; j++)
                              {
                                 Eigen::VectorXd Virtual_Nominal = morph_cate.second[i]->RetriveMorphingValue(j)->GetVariation()->GetValue();
                                 Eigen::VectorXd Virtual_Profile = morph_cate.second[i]->RetriveMorphingValue(j)->GetVariation()->GetValue();
                                 // cout << "Original Variation for Var " << j << " " << Virtual_Nominal.transpose() << endl;
                                 for (auto &morph_cate2 : (*VMR_Map)[tobs][samp.second])
                                 {
                                    if (morph_cate2.first->GetCache() == nullptr)
                                       morph_cate2.first->BookCache();

                                    bool containpoi = false;
                                    for (int k = 0; k < morph_cate2.first->GetNParameters(); k++)
                                    {
                                       std::shared_ptr<Parameter> par = morph_cate2.first->GetParameter(k);
                                       if (par->GetType() == ParameterType::Interest)
                                          containpoi = true;
                                    }
                                    if (containpoi)
                                       continue;

                                    std::vector<double> virtual_val_vec = morph_cate2.first->GetParameterValues();
                                    bool virtual_getvariation = false;
                                    Eigen::VectorXd Virtual_Var;
                                    Eigen::VectorXd Virtual_Impact;

                                    if (WithMetal)
                                    {
                                       Virtual_Var = (*metal_result_map)[tobs][samp.second][morph_cate2.first];
                                       virtual_getvariation = true;
                                    }
                                    else
                                    {
                                       for (int k = 0; k < morph_cate2.second.size(); k++)
                                       {
                                          if (morph_cate2.second[k]->Contains(virtual_val_vec))
                                          {
                                             Virtual_Var = morph_cate2.second[k]->GetMorphingResult(virtual_val_vec);
                                             virtual_getvariation = true;
                                             break;
                                          }
                                       }
                                       if (virtual_getvariation == false)
                                       {
                                          cout << morph_cate2.first->GetName().TString::Data() << " Out of Range!" << endl;
                                          continue;
                                       }
                                    }

                                    TString nom_name = TString::Format("%s_%s_Nominal", tobs->GetName().TString::Data(), samp.second->GetName().TString::Data());
                                    if (morph_cate2.first->GetCache()->FindCache(nom_name))
                                    {
                                       Eigen::VectorXd Virtual_Nom = morph_cate2.first->GetCache()->GetCache(nom_name);
                                       if (UseRelative)
                                          Virtual_Impact = ((Virtual_Var.array() / Virtual_Nom.array() - 1) * Virtual_Nominal.array()).matrix();
                                       else
                                          Virtual_Impact = Virtual_Var - Virtual_Nom;
                                       if (SumImpact)
                                          Virtual_Profile = Virtual_Profile + Virtual_Impact;
                                       else
                                          Virtual_Profile = (Virtual_Var.array() / Virtual_Nom.array() * Virtual_Profile.array()).matrix();
                                    }
                                    else
                                    {
                                       virtual_val_vec = morph_cate2.first->GetParameterNominals();
                                       for (int k = 0; k < morph_cate2.second.size(); k++)
                                       {
                                          if (morph_cate2.second[k]->Contains(virtual_val_vec))
                                          {
                                             Eigen::VectorXd virtual_nominal = morph_cate2.second[k]->GetMorphingResult(virtual_val_vec);
                                             if (UseRelative)
                                                Virtual_Impact = ((Virtual_Var.array() / virtual_nominal.array() - 1) * Virtual_Nominal.array()).matrix();
                                             else
                                                Virtual_Impact = Virtual_Var - virtual_nominal;
                                             if (SumImpact)
                                                Virtual_Profile = Virtual_Profile + Virtual_Impact;
                                             else
                                                Virtual_Profile = (Virtual_Var.array() / virtual_nominal.array() * Virtual_Profile.array()).matrix();
                                             morph_cate2.first->LockThread();
                                             morph_cate2.first->GetCache()->SaveCache(nom_name, virtual_nominal);
                                             morph_cate2.first->UnlockThread();
                                             break;
                                          }
                                       }
                                    }
                                 }
                                 Profiled_Variations.push_back(Virtual_Profile);
                                 // cout << "Profiled Variation for Var " << j << " " << Virtual_Profile.transpose() << endl;
                              }

                              // Make a new MorphingRange with the profiled variations
                              // Transform Eigen::VectorXd to Variation
                              std::vector<std::shared_ptr<Variation>> Virtual_Variations;
                              for (int j = 0; j < Profiled_Variations.size(); j++)
                              {
                                 Eigen::VectorXd virtual_zero = Eigen::VectorXd::Zero(tobs->GetNBins());
                                 std::shared_ptr<Variation> virtual_var = std::make_shared<Variation>(tobs, samp.second, morph_cate.first, Profiled_Variations[j], virtual_zero);
                                 Virtual_Variations.push_back(virtual_var);
                              }
                              // Transform Variation to MorphingValue
                              std::vector<std::shared_ptr<MorphingValue>> Virtual_MorphingValues;
                              for (int j = 0; j < Virtual_Variations.size(); j++)
                              {
                                 int virtual_nvalue = morph_cate.second[i]->RetriveMorphingValue(j)->GetNValues();
                                 std::vector<double> virtual_val;
                                 for (int k = 0; k < virtual_nvalue; k++)
                                    virtual_val.push_back(morph_cate.second[i]->RetriveMorphingValue(j)->GetValue(k));
                                 std::shared_ptr<MorphingValue> virtual_morphingvalue = std::make_shared<MorphingValue>(virtual_val, Virtual_Variations[j], morph_cate.first);
                                 Virtual_MorphingValues.push_back(virtual_morphingvalue);
                              }

                              // Transform MorphingValue to MorphingRange
                              std::shared_ptr<MorphingRange> Virtual_MorphingRange;
                              std::vector<std::shared_ptr<MorphingValue>> Original_RangeMorphingValues;
                              for (int j = 0; j < Virtual_Variations.size(); j++)
                                 Original_RangeMorphingValues.push_back(morph_cate.second[i]->RetriveRangeMorphingValue(j));
                              Virtual_MorphingRange = std::make_shared<MorphingRange>(Virtual_MorphingValues, Original_RangeMorphingValues, morph_cate.first, samp.second, tobs);

                              // Calculate Var with the Virutal Morphing Range
                              Var = Virtual_MorphingRange->GetMorphingResult(val_vec);
                           }
                           else
                           {
                              if (WithMetal)
                                 Var = (*metal_result_map)[tobs][samp.second][morph_cate.first];
                              else
                                 Var = morph_cate.second[i]->GetMorphingResult(val_vec);
                           }
                           getvariation = true;
                           break;
                        }
                     }

                     if (getvariation == false)
                     {
                        cout << morph_cate.first->GetName().TString::Data() << " Out of Range!" << endl;
                        continue;
                     }

                     TString nom_name = TString::Format("%s_%s_Nominal", tobs->GetName().TString::Data(), samp.second->GetName().TString::Data());
                     if (morph_cate.first->GetCache()->FindCache(nom_name))
                     {
                        Eigen::VectorXd Nom = morph_cate.first->GetCache()->GetCache(nom_name);
                        if (UseRelative)
                           Impact = ((Var.array() / Nom.array() - 1) * SampleNominal.array()).matrix();
                        else
                           Impact = Var - Nom;
                        if (SumImpact)
                           Profile_Sample = Profile_Sample + Impact;
                        else
                           Profile_Sample = (Var.array() / Nom.array() * Profile_Sample.array()).matrix();
                     }
                     else
                     {

                        val_vec = morph_cate.first->GetParameterNominals();
                        for (int i = 0; i < morph_cate.second.size(); i++)
                        {
                           if (morph_cate.second[i]->Contains(val_vec))
                           {
                              Eigen::VectorXd nominal = morph_cate.second[i]->GetMorphingResult(val_vec);
                              if (UseRelative)
                                 Impact = ((Var.array() / nominal.array() - 1) * SampleNominal.array()).matrix();
                              else
                                 Impact = Var - nominal;
                              if (SumImpact)
                                 Profile_Sample = Profile_Sample + Impact;
                              else
                                 Profile_Sample = (Var.array() / nominal.array() * Profile_Sample.array()).matrix();
                              morph_cate.first->LockThread();
                              morph_cate.first->GetCache()->SaveCache(nom_name, nominal);
                              morph_cate.first->UnlockThread();
                              break;
                           }
                        }
                     }
                  }
               }
            }
            if (samp.second->HasGammaParameters())
            {
               Eigen::VectorXd gamma = norm * samp.second->GetGammaValue();
               Profile = Profile + (Profile_Sample.array() * gamma.array()).matrix();
            }
            else
               Profile = Profile + norm * Profile_Sample;
         }
      }
      else
      {
         for (auto &samp : (*S_Map)[tobs])
         {
            Eigen::VectorXd Profile_Sample = samp.second->GetValue();
            Eigen::VectorXd SampleNominal = samp.second->GetValue();
            double norm = 1;
            if (samp.second->HasNormalizationParameter())
               norm = samp.second->GetNormalizationParameter()->GetValue();

            auto findobs2 = VMR_Map->find(tobs);
            if (findobs2 != VMR_Map->end())
            {
               auto findsamp = findobs2->second.find(samp.second);
               if (findsamp != findobs2->second.end())
               {
                  for (auto &morph_cate : (*VMR_Map)[tobs][samp.second])
                  {
                     if (morph_cate.first->GetCache() == nullptr)
                        morph_cate.first->BookCache();

                     int npar = morph_cate.first->GetNParameters();
                     std::vector<double> val_vec = morph_cate.first->GetParameterValues();
                     bool getvariation = false;
                     Eigen::VectorXd Var;
                     Eigen::VectorXd Impact;

                     if (WithMetal)
                     {
                        Var = (*metal_result_map)[tobs][samp.second][morph_cate.first];
                        getvariation = true;
                     }
                     else
                     {
                        for (int i = 0; i < morph_cate.second.size(); i++)
                        {
                           if (morph_cate.second[i]->Contains(val_vec))
                           {
                              Var = morph_cate.second[i]->GetMorphingResult(val_vec);
                              getvariation = true;
                              break;
                           }
                        }

                        if (getvariation == false)
                        {
                           cout << morph_cate.first->GetName().TString::Data() << " Out of Range!" << endl;
                           continue;
                        }
                     }

                     TString nom_name = TString::Format("%s_%s_Nominal", tobs->GetName().TString::Data(), samp.second->GetName().TString::Data());
                     if (morph_cate.first->GetCache()->FindCache(nom_name))
                     {
                        Eigen::VectorXd Nom = morph_cate.first->GetCache()->GetCache(nom_name);
                        if (UseRelative)
                           Impact = ((Var.array() / Nom.array() - 1) * SampleNominal.array()).matrix();
                        else
                           Impact = Var - Nom;
                        if (SumImpact)
                           Profile_Sample = Profile_Sample + Impact;
                        else
                           Profile_Sample = (Var.array() / Nom.array() * Profile_Sample.array()).matrix();
                     }
                     else
                     {

                        val_vec = morph_cate.first->GetParameterNominals();
                        for (int i = 0; i < morph_cate.second.size(); i++)
                        {
                           if (morph_cate.second[i]->Contains(val_vec))
                           {
                              Eigen::VectorXd nominal = morph_cate.second[i]->GetMorphingResult(val_vec);
                              if (UseRelative)
                                 Impact = ((Var.array() / nominal.array() - 1) * SampleNominal.array()).matrix();
                              else
                                 Impact = Var - nominal;
                              if (SumImpact)
                                 Profile_Sample = Profile_Sample + Impact;
                              else
                                 Profile_Sample = (Var.array() / nominal.array() * Profile_Sample.array()).matrix();
                              morph_cate.first->LockThread();
                              morph_cate.first->GetCache()->SaveCache(nom_name, nominal);
                              morph_cate.first->UnlockThread();
                              break;
                           }
                        }
                     }
                  }
               }
            }
            if (samp.second->HasGammaParameters())
            {
               Eigen::VectorXd gamma = norm * samp.second->GetGammaValue();
               Profile = Profile + (Profile_Sample.array() * gamma.array()).matrix();
            }
            else
               Profile = Profile + norm * Profile_Sample;
         }
      }
   }
   return Profile;
}

Eigen::VectorXd Observable::GetProfileError2_Thread(std::shared_ptr<Observable> tobs,
                                                    std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map)
{
   Eigen::VectorXd Profile_Error2 = Eigen::VectorXd::Zero(tobs->GetNBins());
   auto findobs = S_Map->find(tobs);
   if (findobs != S_Map->end())
   {
      for (auto &samp : (*S_Map)[tobs])
      {
         Eigen::VectorXd Profile_Error2_Sample = samp.second->GetError2();
         double norm = 1;
         if (samp.second->HasNormalizationParameter())
            norm = samp.second->GetNormalizationParameter()->GetValue();
         Profile_Error2 = Profile_Error2 + pow(norm, 2) * Profile_Error2_Sample;
      }
   }
   return Profile_Error2;
}
std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> Observable::GetProfileError2_Gradient_Thread(std::shared_ptr<Observable> tobs,
                                                                                                   std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map)
{
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_err2_grad;
   auto findobs = S_Map->find(tobs);
   if (findobs != S_Map->end())
   {
      for (auto &samp : (*S_Map)[tobs])
      {
         Eigen::VectorXd Profile_Error2_Sample = samp.second->GetError2();
         if (samp.second->HasNormalizationParameter())
         {
            double norm = samp.second->GetNormalizationParameter()->GetValue();
            auto findpar = prof_err2_grad.find(samp.second->GetNormalizationParameter());
            if (findpar == prof_err2_grad.end())
               prof_err2_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(samp.second->GetNormalizationParameter(), 2 * norm * Profile_Error2_Sample));
            else
               findpar->second = findpar->second + 2 * norm * Profile_Error2_Sample;
         }
      }
   }
   return prof_err2_grad;
}
void Observable::Rebin(int num)
{
   if (GetNBins() % num != 0)
      return;
   if (Cache_ptr == nullptr)
      BookCache();

   int newbinnum = GetNBins() / num;
   Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
   Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

   double sum_val = 0;
   double sum_err2 = 0;
   int index = 0;
   for (int i = 0; i < GetNBins(); i++)
   {
      sum_val = sum_val + Value(i);
      sum_err2 = sum_err2 + pow(Error(i), 2);
      if ((i + 1) % num == 0)
      {
         New_Value(index) = sum_val;
         New_Error(index) = sqrt(sum_err2);
         index = index + 1;
         sum_val = 0;
         sum_err2 = 0;
      }
   }

   Value = New_Value;
   Error = New_Error;
   NBins = newbinnum;
}

void Observable::SetRange(int start, int end)
{
   if (start >= 0 && end >= start)
   {
      int newbinnum = end - start + 1;
      Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
      Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

      if (Cache_ptr == nullptr)
         BookCache();
      int index = 0;
      for (int i = start; i <= end; i++)
      {
         New_Value(index) = Value(i);
         New_Error(index) = Error(i);
         index = index + 1;
      }

      Value = New_Value;
      Error = New_Error;
      NBins = newbinnum;
   }
}

void Observable::RecoverModifications()
{
   if (Cache_ptr != nullptr)
   {
      Value = Cache_ptr->GetCache("Value");
      Error = Cache_ptr->GetCache("Error");
      NBins = Value.rows();
   }
}
void Observable::BookCache()
{
   if (Cache_ptr == nullptr)
   {
      Cache_ptr = std::make_shared<Cache>();
      Cache_ptr->SaveCache("Value", Value);
      Cache_ptr->SaveCache("Error", Error);
   }
}
