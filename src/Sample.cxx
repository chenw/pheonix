#include "Phoenix/Sample.h"

using namespace std;
using namespace Phoenix;

void Sample::RecoverModifications()
{
   if (Cache_ptr != nullptr)
   {
      Value = Cache_ptr->GetCache("Value");
      Error = Cache_ptr->GetCache("Error");
      NBins = Value.rows();
   }
}

void Sample::SetRange(int start, int end)
{
   if (start >= 0 && end >= start)
   {
      int newbinnum = end - start + 1;
      Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
      Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

      if (Cache_ptr == nullptr)
         BookCache();
      int index = 0;
      for (int i = start; i <= end; i++)
      {
         New_Value(index) = Value(i);
         New_Error(index) = Error(i);
         index = index + 1;
      }

      Value = New_Value;
      Error = New_Error;
      NBins = newbinnum;
   }
}

void Sample::Rebin(int num)
{
   if (GetNBins() % num != 0)
      return;
   if (Cache_ptr == nullptr)
      BookCache();

   int newbinnum = GetNBins() / num;
   Eigen::VectorXd New_Value = Eigen::VectorXd(newbinnum);
   Eigen::VectorXd New_Error = Eigen::VectorXd(newbinnum);

   double sum_val = 0;
   double sum_err2 = 0;
   int index = 0;
   for (int i = 0; i < GetNBins(); i++)
   {
      sum_val = sum_val + Value(i);
      sum_err2 = sum_err2 + pow(Error(i), 2);
      if ((i + 1) % num == 0)
      {
         New_Value(index) = sum_val;
         New_Error(index) = sqrt(sum_err2);
         index = index + 1;
         sum_val = 0;
         sum_err2 = 0;
      }
   }

   Value = New_Value;
   Error = New_Error;
   NBins = newbinnum;
}

void Sample::SetNormalizationParameter(std::shared_ptr<Parameter> par)
{
   if (par->GetType() == ParameterType::Normalize)
      Norm_Par_ptr = par;
}

void Sample::SetGammaParameter(int index, std::shared_ptr<Parameter> par)
{
   if (index < 0 || index >= NBins)
      return;
   if (par->GetType() != ParameterType::StatGamma)
      return;
   while (index >= Gamma_Vec.size())
      Gamma_Vec.push_back(nullptr);
   Gamma_Vec[index] = par;
}

Eigen::VectorXd Sample::GetGammaValue()
{
   Eigen::VectorXd GammaValue = Eigen::VectorXd(NBins);

   for (int i = 0; i < NBins; i++)
   {
      if (i < Gamma_Vec.size() && HasGammaParameters())
         GammaValue(i) = Gamma_Vec[i]->GetValue();
      else
         GammaValue(i) = 1.0;
   }

   return GammaValue;
}

Eigen::VectorXd Sample::GetError2()
{
   Eigen::VectorXd err2 = Eigen::VectorXd(NBins);

   for (int i = 0; i < NBins; i++)
      err2(i) = pow(Error(i), 2);

   return err2;
}

TString Sample::GetGammaParameterName(int index)
{
   return TString::Format("%s_%s_Bin_%d", GammaPrefix.TString::Data(), Obs_ptr->GetName().TString::Data(), index);
}

void Sample::BookCache()
{
   if (Cache_ptr == nullptr)
   {
      Cache_ptr = std::make_shared<Cache>();
      Cache_ptr->SaveCache("Value", Value);
      Cache_ptr->SaveCache("Error", Error);
   }
}

Eigen::VectorXd Sample::GetProfile_Thread(std::shared_ptr<Sample> tsamp, Eigen::VectorXd total_impact, bool SeparatePOI, bool UseRelative, bool SumImpact,
                                          std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>> v_map)
{
   Eigen::VectorXd prof = tsamp->GetValue();

   std::shared_ptr<MorphingCategory> center_cate = nullptr;

   if (SeparatePOI)
   {
      bool findcentral = false;
      for (auto &cate : v_map)
      {
         if ((cate.first->ContainsPOI()) && (findcentral == false))
         {
            findcentral = true;
            center_cate = cate.first;
         }
         else if ((cate.first->ContainsPOI()) && (findcentral == true))
         {
            std::cout << "Already found 1 morphing category contains POI! Cannot Separate POI with NP when POIs are separated in different categories." << std::endl;
            return prof;
         }
      }
   }

   if (center_cate == nullptr)
   {
      if (SumImpact && UseRelative)
         prof = (prof.array() * (total_impact + Eigen::VectorXd::Ones(tsamp->GetNBins())).array()).matrix();
      else if (SumImpact && !UseRelative)
         prof = prof + total_impact;
      else if (!SumImpact && UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
      else if (!SumImpact && !UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
   }
   else
   {
      std::vector<double> par_val;
      for (int i = 0; i < center_cate->GetNParameters(); i++)
         par_val.push_back(center_cate->GetParameter(i)->GetValue());

      std::vector<std::shared_ptr<MorphingRange>> r_vec = v_map[center_cate];
      std::shared_ptr<MorphingRange> central_range = nullptr;
      for (int i = 0; i < r_vec.size(); i++)
      {
         if (r_vec[i]->Contains(par_val))
         {
            central_range = r_vec[i];
            break;
         }
      }
      if (central_range == nullptr)
      {
         std::cout << "Parameter out of the morphing range!" << std::endl;
         return prof;
      }

      prof = central_range->GetMorphingResult(par_val);

      if (SumImpact && UseRelative)
         prof = (prof.array() * (total_impact + Eigen::VectorXd::Ones(tsamp->GetNBins())).array()).matrix();
      else if (SumImpact && !UseRelative)
         prof = prof + total_impact;
      else if (!SumImpact && UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
      else if (!SumImpact && !UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
   }

   double norm = 1;
   if (tsamp->HasNormalizationParameter())
      norm = tsamp->GetNormalizationParameter()->GetValue();

   if (tsamp->HasGammaParameters())
   {
      Eigen::VectorXd gamma = norm * tsamp->GetGammaValue();
      prof = (prof.array() * gamma.array()).matrix();
   }
   else
      prof = norm * prof;

   return prof;
}
std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> Sample::GetProfile_Gradient_Thread(std::shared_ptr<Sample> tsamp, Eigen::VectorXd total_impact,
                                                                                         std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> total_impact_grad,
                                                                                         bool SeparatePOI, bool UseRelative, bool SumImpact,
                                                                                         std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>> v_map)
{
   std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad;
   Eigen::VectorXd prof = tsamp->GetValue();

   std::shared_ptr<MorphingCategory> center_cate = nullptr;

   if (SeparatePOI)
   {
      bool findcentral = false;
      for (auto &cate : v_map)
      {
         if ((cate.first->ContainsPOI()) && (findcentral == false))
         {
            findcentral = true;
            center_cate = cate.first;
         }
         else if ((cate.first->ContainsPOI()) && (findcentral == true))
         {
            std::cout << "Already found 1 morphing category contains POI! Cannot Separate POI with NP when POIs are separated in different categories." << std::endl;
            return prof_grad;
         }
      }
   }

   if (center_cate == nullptr)
   {
      if (SumImpact && !UseRelative)
         prof_grad = total_impact_grad;
      else
      {
         for (auto &par : total_impact_grad)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(par.first, (prof.array() * par.second.array()).matrix()));
      }

      if (SumImpact && UseRelative)
         prof = (prof.array() * (total_impact + Eigen::VectorXd::Ones(tsamp->GetNBins())).array()).matrix();
      else if (SumImpact && !UseRelative)
         prof = prof + total_impact;
      else if (!SumImpact && UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
      else if (!SumImpact && !UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();

   }
   else
   {
      std::vector<double> par_val;
      for (int i = 0; i < center_cate->GetNParameters(); i++)
         par_val.push_back(center_cate->GetParameter(i)->GetValue());

      std::vector<std::shared_ptr<MorphingRange>> r_vec = v_map[center_cate];
      std::shared_ptr<MorphingRange> central_range = nullptr;
      for (int i = 0; i < r_vec.size(); i++)
      {
         if (r_vec[i]->Contains(par_val))
         {
            central_range = r_vec[i];
            break;
         }
      }
      if (central_range == nullptr)
      {
         std::cout << "Parameter out of the morphing range!" << std::endl;
         return prof_grad;
      }

      prof = central_range->GetMorphingResult(par_val);

      if (SumImpact && !UseRelative)
         prof_grad = total_impact_grad;
      else
      {
         for (auto &par : total_impact_grad)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(par.first, (prof.array() * par.second.array()).matrix()));
      }

      std::vector<Eigen::VectorXd> central_grad = central_range->GetMorphingResult_Gradient(par_val);
      for (int i = 0; i < central_grad.size(); i++)
      {
         if (SumImpact && UseRelative)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(center_cate->GetParameter(i),
                                                                                     (central_grad[i].array() * (total_impact + Eigen::VectorXd::Ones(tsamp->GetNBins())).array()).matrix()));
         else if (SumImpact && !UseRelative)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(center_cate->GetParameter(i), central_grad[i]));
         else if (!SumImpact && UseRelative)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(center_cate->GetParameter(i), (central_grad[i].array() * total_impact.array()).matrix()));
         else if (!SumImpact && !UseRelative)
            prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(center_cate->GetParameter(i), (central_grad[i].array() * total_impact.array()).matrix()));
      }

      if (SumImpact && UseRelative)
         prof = (prof.array() * (total_impact + Eigen::VectorXd::Ones(tsamp->GetNBins())).array()).matrix();
      else if (SumImpact && !UseRelative)
         prof = prof + total_impact;
      else if (!SumImpact && UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();
      else if (!SumImpact && !UseRelative)
         prof = (prof.array() * total_impact.array()).matrix();

   }

   if (tsamp->HasNormalizationParameter())
   {
      double norm = tsamp->GetNormalizationParameter()->GetValue();
      for (auto &par : prof_grad)
         par.second = norm * par.second;

      prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(tsamp->GetNormalizationParameter(), prof));
      prof = norm * prof;
   }

   if (tsamp->HasGammaParameters())
   {
      Eigen::VectorXd gamma = tsamp->GetGammaValue();
      for (auto &par : prof_grad)
         par.second = (gamma.array() * par.second.array()).matrix();

      for (int i = 0; i < tsamp->GetNBins(); i++)
      {
         Eigen::VectorXd gamma_grad = Eigen::VectorXd::Zero(tsamp->GetNBins());
         gamma_grad(i) = prof(i);
         prof_grad.emplace(std::pair<std::shared_ptr<Parameter>, Eigen::VectorXd>(tsamp->GetGammaParameter(i), gamma_grad));
      }
   }

   return prof_grad;
}
