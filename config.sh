#!/bin/bash
cat > setupPheonix.sh << EOF
# Pheonix
export PATH=`pwd`/bin:\$PATH
export LD_LIBRARY_PATH=`pwd`/lib:\$LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=`pwd`/lib:\$DYLD_LIBRARY_PATH
export LIBRARY_PATH=`pwd`/lib:\$LIBRARY_PATH
export LIBPATH=`pwd`/lib:\$LIBPATH
export C_INCLUDE_PATH=`pwd`/include:\$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=`pwd`/include:\$CPLUS_INCLUDE_PATH
EOF
