#ifndef PHOENIX_PARAMETER_HEADER
#define PHOENIX_PARAMETER_HEADER

#include "NAGASH/NAGASH.h"
#include "Sample.h"
#include "Variation.h"
#include "Observable.h"
#include "Cache.h"

namespace Phoenix
{
   class Variation;
   class Observable;
   class Sample;

   enum class ParameterType
   {
      Interest,
      Nuisance,
      Normalize,
      StatGamma,
      UNKNOWN
   };
   class Parameter
   {
      private:
         TString Name;

         double Value;
         double Error;
         double Error_Up;
         double Error_Down;
         double Nominal;
         double Min;
         double Max;

         ParameterType Type;

         bool isFixed = false;

         double Gamma_Gaus_Sigma = -999;
         double Nuisance_Expectation = 0;
         // The default expectation of the nuisance parameter is 0
         // Can be used to shift the nuisance parameters
         bool WithNPConstrain = true;
         bool NeedMinosUnc = false;

      public:
         Parameter(TString name, double nom, double min, double max, ParameterType type);
         TString GetName()
         {
            return Name;
         }
         double GetValue()
         {
            return Value;
         }
         double GetMin()
         {
            return Min;
         }
         double GetMax()
         {
            return Max;
         }
         double GetNominal()
         {
            return Nominal;
         }
         double GetError()
         {
            return Error;
         }
         double GetErrorUp()
         {
            return Error_Up;
         }
         double GetErrorDown()
         {
            return Error_Down;
         }
         ParameterType GetType()
         {
            return Type;
         }
         bool IsFixed()
         {
            return isFixed;
         }
         void Fix()
         {
            isFixed = true;
         }
         void Release()
         {
            isFixed = false;
         }
         void SetMin(double min)
         {
            Min = min;
         }
         void SetMax(double max)
         {
            Max = max;
         }
         void SetValue(double val)
         {
            Value = val;
         }
         void SetError(double err)
         {
            Error = err;
         }
         void ReleaseNPConstrain()
         {
            WithNPConstrain = false;
         }
         void ApplyNPConstrain()
         {
            WithNPConstrain = true;
         }
         void RequireMinosUnc(bool req = true)
         {
            NeedMinosUnc = req;
         }
         bool GetMinosRequirement()
         {
            return NeedMinosUnc;
         }
         void SetGammaSigma(double sig)
         {
            Gamma_Gaus_Sigma = sig;
         }
         void SetNuisanceExpectation(double exp)
         {
            Nuisance_Expectation = exp;
         }
         double GetNuisanceExpectation()
         {
            return Nuisance_Expectation;
         }
         double GetGammaSigma()
         {
            return Gamma_Gaus_Sigma;
         }
         double GetLikelihood();
         double GetLikelihood_Gradient();
         void SetMinosUncertainty(double up, double down);
         void SetMinosUncertainty(std::pair<double, double> unc_pair);
         static double GetLikelihood_Thread(std::shared_ptr<Parameter> tpar)
         {
            return tpar->GetLikelihood();
         }
         static double GetLikelihood_Gradient_Thread(std::shared_ptr<Parameter> tpar)
         {
            return tpar->GetLikelihood_Gradient();
         }
         static TString TypeToTString(ParameterType type);
         static ParameterType TStringToType(TString type);
   };

   inline Parameter::Parameter(TString name, double nom, double min, double max, ParameterType type)
   {
      Name = name;
      Value = nom;
      Error = max - min;
      Nominal = nom;
      Min = min;
      Max = max;
      Type = type;
   }

   enum class MorphingMode
   {
      UNKNOWN,

      // For 1D Morphing
      FULL_LINEAR,
      POL6_EXP,

      // For 2D Morphing
      TRIANGLE_LINEAR
   };

   class MorphingCategory
   {
      private:
         TString Name;
         std::vector<std::shared_ptr<Parameter>> Par_Vec;
         MorphingMode Mode = MorphingMode::UNKNOWN;

         std::shared_ptr<Cache> Cache_ptr = nullptr;

         std::recursive_mutex DefaultMutex = std::recursive_mutex();

      public:
         MorphingCategory(TString name);
         void AddParameter(std::shared_ptr<Parameter> par)
         {
            Par_Vec.push_back(par);
         }
         int GetNParameters()
         {
            return Par_Vec.size();
         }
         TString GetName()
         {
            return Name;
         }

         static TString ModeToTString(MorphingMode mode);
         static MorphingMode TStringToMode(TString mode);

         void SetMorphingMode(MorphingMode mode)
         {
            Mode = mode;
         }
         void ConfigMorphingMode();
         MorphingMode GetMorphingMode()
         {
            return Mode;
         }

         std::shared_ptr<Parameter> GetParameter(int index);
         std::shared_ptr<Parameter> GetParameter(TString name);

         std::vector<double> GetParameterValues();
         std::vector<double> GetParameterNominals();

         bool ContainsPOI();

         std::shared_ptr<Cache> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache()
         {
            Cache_ptr = std::make_shared<Cache>();
         }

         void LockThread()
         {
            DefaultMutex.lock();
         }
         void UnlockThread()
         {
            DefaultMutex.unlock();
         }

         void Clear()
         {
            Par_Vec.clear();
         }
   };

   inline bool MorphingCategory::ContainsPOI()
   {
      bool contain = false;
      for (int i = 0; i < Par_Vec.size(); i++)
         if (Par_Vec[i]->GetType() == ParameterType::Interest)
            contain = true;

      return contain;
   }

   inline void MorphingCategory::ConfigMorphingMode()
   {
      if (GetNParameters() == 1)
      {
         if (GetParameter(0)->GetType() == ParameterType::Interest)
            Mode = MorphingMode::FULL_LINEAR;
         else if (GetParameter(0)->GetType() == ParameterType::Nuisance)
            Mode = MorphingMode::POL6_EXP;
         else
            Mode = MorphingMode::FULL_LINEAR;
      }
      else if (GetNParameters() == 2)
         Mode = MorphingMode::TRIANGLE_LINEAR;
      else
         Mode = MorphingMode::FULL_LINEAR;
   }

   inline MorphingCategory::MorphingCategory(TString name)
   {
      Name = name;
   }

   inline std::shared_ptr<Parameter> MorphingCategory::GetParameter(int index)
   {
      if (GetNParameters() == 0)
         return nullptr;
      else if (index >= GetNParameters())
         return nullptr;
      else
         return Par_Vec[index];
   }

   inline std::shared_ptr<Parameter> MorphingCategory::GetParameter(TString name)
   {
      if (GetNParameters() == 0)
         return nullptr;
      for (int i = 0; i < GetNParameters(); i++)
      {
         if (Par_Vec[i]->GetName().TString::EqualTo(name))
            return Par_Vec[i];
      }

      return nullptr;
   }

   inline std::vector<double> MorphingCategory::GetParameterValues()
   {
      std::vector<double> val_vec;
      for (int i = 0; i < Par_Vec.size(); i++)
         val_vec.push_back(Par_Vec[i]->GetValue());
      return val_vec;
   }

   inline std::vector<double> MorphingCategory::GetParameterNominals()
   {
      std::vector<double> val_vec;
      for (int i = 0; i < Par_Vec.size(); i++)
         val_vec.push_back(Par_Vec[i]->GetNominal());
      return val_vec;
   }

   class MorphingValue
   {
      private:
         std::vector<double> Value;
         std::shared_ptr<Variation> Variation_ptr;
         std::shared_ptr<MorphingCategory> Morphing_ptr;

      public:
         MorphingValue(const std::vector<double> &val_vec, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group);
         MorphingValue(double val, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group);
         MorphingValue(double val_x, double val_y, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> groug);
         MorphingValue(double val_x, double val_y, double val_z, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group);

         double GetValue();
         double GetValueX();
         double GetValueY();
         double GetValueZ();
         double GetValue(int index);

         int GetNValues()
         {
            return Value.size();
         }

         std::shared_ptr<MorphingCategory> GetMorphingCategory()
         {
            return Morphing_ptr;
         }
         std::shared_ptr<Variation> GetVariation()
         {
            return Variation_ptr;
         }
   };

   inline MorphingValue::MorphingValue(const std::vector<double> &val_vec, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group)
   {
      Value = val_vec;

      Variation_ptr = var;
      Morphing_ptr = group;
   }

   inline MorphingValue::MorphingValue(double val, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group)
   {
      Value.push_back(val);

      Variation_ptr = var;
      Morphing_ptr = group;
   }

   inline MorphingValue::MorphingValue(double val_x, double val_y, std::shared_ptr<Variation> var, std::shared_ptr<MorphingCategory> group)
   {
      Value.push_back(val_x);
      Value.push_back(val_y);

      Variation_ptr = var;
      Morphing_ptr = group;
   }

   inline double MorphingValue::GetValue()
   {
      if (GetNValues() == 1)
         return Value[0];
      else
         return -999;
   }

   inline double MorphingValue::GetValue(int index)
   {
      if (GetNValues() > index && index >= 0)
         return Value[index];
      else
         return -999;
   }

   inline double MorphingValue::GetValueX()
   {
      if (GetNValues() == 2 || GetNValues() == 3)
         return Value[0];
      else
         return -999;
   }

   inline double MorphingValue::GetValueY()
   {
      if (GetNValues() == 2 || GetNValues() == 3)
         return Value[1];
      else
         return -999;
   }

   inline double MorphingValue::GetValueZ()
   {
      if (GetNValues() == 3)
         return Value[2];
      else
         return -999;
   }

   class MorphingRange
   {

      private:
         std::vector<std::shared_ptr<MorphingValue>> Val_Vec;
         std::vector<std::shared_ptr<MorphingValue>> Range_Vec;

         std::shared_ptr<MorphingCategory> Morphing_ptr;
         std::shared_ptr<Observable> Obs_ptr;
         std::shared_ptr<Sample> Sample_ptr;

         std::shared_ptr<Cache> Cache_ptr = nullptr;

         Eigen::VectorXd GetMorphingResult_1D_FULL_LINEAR(const std::vector<double> &val_vec);
         Eigen::VectorXd GetMorphingResult_1D_POL6_EXP(const std::vector<double> &val_vec);
         Eigen::VectorXd GetMorphingResult_2D_TRIANGLE_LINEAR(const std::vector<double> &val_vec);

         std::vector<Eigen::VectorXd> GetMorphingResult_Gradient_1D_FULL_LINEAR(const std::vector<double> &val_vec);
         std::vector<Eigen::VectorXd> GetMorphingResult_Gradient_1D_POL6_EXP(const std::vector<double> &val_vec);
         std::vector<Eigen::VectorXd> GetMorphingResult_Gradient_2D_TRIANGLE_LINEAR(const std::vector<double> &val_vec);

      public:
         MorphingRange(const std::vector<std::shared_ptr<MorphingValue>> &val_vec, std::shared_ptr<MorphingCategory> group, std::shared_ptr<Sample> samp, std::shared_ptr<Observable> obs);
         MorphingRange(const std::vector<std::shared_ptr<MorphingValue>> &val_vec, const std::vector<std::shared_ptr<MorphingValue>> &range_vec, std::shared_ptr<MorphingCategory> group,
                       std::shared_ptr<Sample> samp, std::shared_ptr<Observable> obs);
         bool Contains(const std::vector<double> &val_vec);
         Eigen::VectorXd GetMorphingResult(const std::vector<double> &val_vec);
         std::vector<Eigen::VectorXd> GetMorphingResult_Gradient(const std::vector<double> &val_vec);
         std::shared_ptr<MorphingCategory> RetriveMorphingCategory()
         {
            return Morphing_ptr;
         }
         std::shared_ptr<MorphingValue> RetriveMorphingValue(int index)
         {
            return Val_Vec[index];
         }
         int GetNMorphingValues()
         {
            return Val_Vec.size();
         }
         std::shared_ptr<Cache> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache()
         {
            Cache_ptr = std::make_shared<Cache>();
         }
         std::shared_ptr<MorphingValue> RetriveRangeMorphingValue(int index)
         {
            return Range_Vec[index];
         }
         std::vector<double> GetCentral();
   };

   inline MorphingRange::MorphingRange(const std::vector<std::shared_ptr<MorphingValue>> &val_vec, std::shared_ptr<MorphingCategory> group, std::shared_ptr<Sample> samp, std::shared_ptr<Observable> obs)
   {
      Range_Vec.clear();
      for (int i = 0; i < val_vec.size(); i++)
         Range_Vec.push_back(val_vec[i]);
      Val_Vec.clear();
      for (int i = 0; i < val_vec.size(); i++)
         Val_Vec.push_back(val_vec[i]);

      Morphing_ptr = group;
      Obs_ptr = obs;
      Sample_ptr = samp;
   }

   inline MorphingRange::MorphingRange(const std::vector<std::shared_ptr<MorphingValue>> &val_vec, const std::vector<std::shared_ptr<MorphingValue>> &range_vec, std::shared_ptr<MorphingCategory> group,
                                       std::shared_ptr<Sample> samp, std::shared_ptr<Observable> obs)
   {
      Range_Vec.clear();
      for (int i = 0; i < range_vec.size(); i++)
         Range_Vec.push_back(range_vec[i]);
      Val_Vec.clear();
      for (int i = 0; i < val_vec.size(); i++)
         Val_Vec.push_back(val_vec[i]);

      Morphing_ptr = group;
      Obs_ptr = obs;
      Sample_ptr = samp;
   }
}

#endif
