#ifndef PHOENIX_OBSERVABLE_HEADER
#define PHOENIX_OBSERVABLE_HEADER

#include "NAGASH/NAGASH.h"
#include "Cache.h"
#include "Sample.h"
#include "Parameter.h"
#include "Eigen/Dense"

namespace Phoenix
{
   class Cache;
   class Sample;
   class MorphingCategory;
   class MorphingRange;
   class Parameter;

   class Observable
   {
      private:
         int NBins = -999;
         TString Name;
         Eigen::VectorXd Value;
         Eigen::VectorXd Error;

         std::shared_ptr<Cache> Cache_ptr = nullptr;

      public:
         Observable(TString name, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         TString GetName()
         {
            return Name;
         }
         Eigen::VectorXd GetValue()
         {
            return Value;
         }
         Eigen::VectorXd GetError()
         {
            return Error;
         }
         double GetValue(int i);
         double GetError(int i);
         int GetNBins()
         {
            return NBins;
         }
         double GetLikelihood(Eigen::VectorXd prof);
         static double GetLikelihood_Thread(std::shared_ptr<Observable> tobs, Eigen::VectorXd prof)
         {
            return tobs->GetLikelihood(prof);
         }
         std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gradient(Eigen::VectorXd prof, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad);
         static std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gradient_Thread(std::shared_ptr<Observable> tobs, Eigen::VectorXd prof,
                                                                                           std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad)
         {
            return tobs->GetLikelihood_Gradient(prof, prof_grad);
         }
         double GetLikelihoodConstPart();
         double GetLikelihood_Gaus(Eigen::VectorXd prof, Eigen::VectorXd prof_err2);
         std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gaus_Gradient(Eigen::VectorXd prof, Eigen::VectorXd prof_err2,
                                                                                  std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_err2_grad);
         static std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gaus_Gradient_Thread(std::shared_ptr<Observable> tobs, Eigen::VectorXd prof, Eigen::VectorXd prof_err2,
                                                                                                std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_err2_grad)
         {
            return tobs->GetLikelihood_Gaus_Gradient(prof, prof_err2, prof_grad, prof_err2_grad);
         }
         static double GetLikelihood_Gaus_Thread(std::shared_ptr<Observable> tobs, Eigen::VectorXd prof, Eigen::VectorXd prof_err2)
         {
            return tobs->GetLikelihood_Gaus(prof, prof_err2);
         }
         double GetLikelihoodConstPart_Gaus();

         static Eigen::VectorXd GetProfile_Thread(std::shared_ptr<Observable> tobs,
                                                  std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map,
                                                  std::map<std::shared_ptr<Observable>,
                                                  std::map<std::shared_ptr<Sample>,
                                                  std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>* VMR_Map,
                                                  std::map<std::shared_ptr<Observable>,
                                                  std::map<std::shared_ptr<Sample>,
                                                  std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>>* metal_result_map,
                                                  bool WithMetal,
                                                  bool UseRelative,
                                                  bool SumImpact,
                                                  bool SeparatePOI);
         static Eigen::VectorXd GetProfileError2_Thread(std::shared_ptr<Observable> tobs,
                                                        std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map);
         static std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> GetProfileError2_Gradient_Thread(std::shared_ptr<Observable> tobs,
                                                                                                       std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* S_Map);

         std::shared_ptr<Cache> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache();
         void Rebin(int num);
         void SetRange(int start, int end);
         void RecoverModifications();
   };

   inline Observable::Observable(TString name, const std::vector<double> &val_vec, const std::vector<double> &err_vec)
   {
      Name = name;
      NBins = val_vec.size();
      Value = Eigen::VectorXd(NBins);
      Error = Eigen::VectorXd(NBins);
      for (int i = 0; i < NBins; i++)
      {
         Value(i) = val_vec[i];
         Error(i) = err_vec[i];
      }
   }

   inline double Observable::GetValue(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Value(i);
   }
   inline double Observable::GetError(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Error(i);
   }
}

#endif
