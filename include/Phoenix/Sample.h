#ifndef PHOENIX_SAMPLE_HEADER
#define PHOENIX_SAMPLE_HEADER

#include "NAGASH/NAGASH.h"
#include "Parameter.h"
#include "Observable.h"
#include "Eigen/Dense"

namespace Phoenix
{
   class Cache;
   class Parameter;
   class Observable;
   enum class ParameterType;

   class Sample
   {
      private:
         int NBins = -999;
         TString Name;
         Eigen::VectorXd Value;
         Eigen::VectorXd Error;
         std::shared_ptr<Observable> Obs_ptr;
         std::shared_ptr<Parameter> Norm_Par_ptr = nullptr;
         std::vector<std::shared_ptr<Parameter>> Gamma_Vec;
         TString GammaPrefix = "StatGamma";

         std::shared_ptr<Cache> Cache_ptr = nullptr;

      public:
         Sample(TString name, std::shared_ptr<Observable> obs_ptr, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         TString GetName()
         {
            return Name;
         }
         Eigen::VectorXd GetValue()
         {
            return Value;
         }
         Eigen::VectorXd GetError()
         {
            return Error;
         }
         Eigen::VectorXd GetError2();
         double GetValue(int i);
         double GetError(int i);
         int GetNBins()
         {
            return NBins;
         }
         std::shared_ptr<Observable> RetriveObservable()
         {
            return Obs_ptr;
         }
         void SetNormalizationParameter(std::shared_ptr<Parameter> par);
         std::shared_ptr<Parameter> GetNormalizationParameter()
         {
            return Norm_Par_ptr;
         }
         bool HasNormalizationParameter()
         {
            return !(Norm_Par_ptr == nullptr);
         }
         void SetGammaPrefix(TString prefix)
         {
            GammaPrefix = prefix;
         }
         void SetGammaParameter(int index, std::shared_ptr<Parameter> par);
         std::shared_ptr<Parameter> GetGammaParameter(int index);
         TString GetGammaPrefix()
         {
            return GammaPrefix;
         }
         TString GetGammaParameterName(int index);
         bool HasGammaParameters()
         {
            return (Gamma_Vec.size() != 0);
         }
         Eigen::VectorXd GetGammaValue();
         std::shared_ptr<Cache> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache();
         void Rebin(int num);
         void SetRange(int start, int end);
         void RecoverModifications();

         static Eigen::VectorXd GetProfile_Thread(std::shared_ptr<Sample> tsamp, Eigen::VectorXd total_impact, bool SeparatePOI, bool UseRelative, bool SumImpact,
                                                  std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>> v_map);
         static std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> GetProfile_Gradient_Thread(std::shared_ptr<Sample> tsamp, Eigen::VectorXd total_impact,
                                                                                                 std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> total_impact_grad,
                                                                                                 bool SeparatePOI, bool UseRelative, bool SumImpact,
                                                                                                 std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>> v_map);
   };

   inline std::shared_ptr<Parameter> Sample::GetGammaParameter(int index)
   {
      if (index < 0 || index >= NBins)
         return nullptr;
      if (index >= Gamma_Vec.size())
         return nullptr;

      return Gamma_Vec[index];
   }

   inline Sample::Sample(TString name, std::shared_ptr<Observable> obs_ptr, const std::vector<double> &val_vec, const std::vector<double> &err_vec)
   {
      Name = name;
      NBins = val_vec.size();
      Value = Eigen::VectorXd(NBins);
      Error = Eigen::VectorXd(NBins);
      for (int i = 0; i < NBins; i++)
      {
         Value(i) = val_vec[i];
         Error(i) = err_vec[i];
      }
      Obs_ptr = obs_ptr;
   }

   inline double Sample::GetValue(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Value(i);
   }
   inline double Sample::GetError(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Error(i);
   }
}

#endif
