#ifndef PHOENIX_CACHE_HEADER
#define PHOENIX_CACHE_HEADER

#include "NAGASH/NAGASH.h"
#include "Sample.h"
#include "Observable.h"
#include "Parameter.h"

namespace Phoenix
{
   class Parameter;
   class Observable;
   class Sample;
   class MorphingCategory;

   class Cache
   {
      private:
         std::map<TString, Eigen::VectorXd> cache_map;

      public:
         Cache();
         bool FindCache(TString name);
         void SaveCache(TString name, Eigen::VectorXd vec);
         Eigen::VectorXd GetCache(TString name);
   };

   inline Cache::Cache()
   {
   }

   inline bool Cache::FindCache(TString name)
   {
      auto findcache = cache_map.find(name);
      if (findcache != cache_map.end())
         return true;
      return false;
   }

   inline void Cache::SaveCache(TString name, Eigen::VectorXd vec)
   {
      cache_map.emplace(std::pair<TString, Eigen::VectorXd>(name, vec));
   }

   inline Eigen::VectorXd Cache::GetCache(TString name)
   {
      return cache_map[name];
   }

   class Cache2D
   {
      private:
         std::map<TString, Eigen::MatrixXd> cache_map;

      public:
         Cache2D();
         bool FindCache(TString name);
         void SaveCache(TString name, Eigen::MatrixXd vec);
         Eigen::MatrixXd GetCache(TString name);
   };

   inline Cache2D::Cache2D()
   {
   }

   inline bool Cache2D::FindCache(TString name)
   {
      auto findcache = cache_map.find(name);
      if (findcache != cache_map.end())
         return true;
      return false;
   }

   inline void Cache2D::SaveCache(TString name, Eigen::MatrixXd vec)
   {
      cache_map.emplace(std::pair<TString, Eigen::MatrixXd>(name, vec));
   }

   inline Eigen::MatrixXd Cache2D::GetCache(TString name)
   {
      return cache_map[name];
   }

   class ProfileRecord
   {
      private:
         std::map<std::shared_ptr<Phoenix::Parameter>, double> par_value_map;
         std::map<std::shared_ptr<Observable>,
             std::map<std::shared_ptr<Sample>,
             std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> result_map;
         // In the sequence of Observable, Sample and Morphing Category;

         static std::vector<std::shared_ptr<MorphingCategory>> cate_vec;
      public:
         ProfileRecord(std::map<TString, std::shared_ptr<Parameter>>* par_map,
                       std::map<std::shared_ptr<Observable>,
                       std::map<std::shared_ptr<Sample>,
                       std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> r_map);

         // std::map<std::shared_ptr<MorphingCategory>, bool> CheckParameters(double par_prec);

         // void Recover(double fit_prec, std::map<TString, std::shared_ptr<Parameter>>* par_map, std::map<std::shared_ptr<Observable>,
         // std::map<std::shared_ptr<Sample>,
         // std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> &r_map);

         // void Recover_MultiThread(int nthread, double fit_prec, std::map<TString, std::shared_ptr<Parameter>>* par_map, std::map<std::shared_ptr<Observable>,
         // std::map<std::shared_ptr<Sample>,
         // std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> &r_map);
   };

   class GlobalCache
   {
      private:
         // Long Term Cache
         // Which is used more than once
         // Managed by FIT_MAX_LONGTERMCACHE

   };
}

#endif
