#ifndef PHOENIX_CORRELATION_HEADER
#define PHOENIX_CORRELATION_HEADER

#include "Sample.h"
#include "Observable.h"
#include "NAGASH/NAGASH.h"
#include "Cache.h"

namespace Phoenix
{
   class Sample;
   class Observable;

   class Correlation
   {
      private:
         std::shared_ptr<Observable> Obs_ptr_x;
         std::shared_ptr<Observable> Obs_ptr_y;
         std::shared_ptr<Sample> Samp_ptr_x;
         std::shared_ptr<Sample> Samp_ptr_y;
         Eigen::MatrixXd cov;

         int NBins_X;
         int NBins_Y;

         std::shared_ptr<Cache2D> Cache_ptr = nullptr;

         std::shared_ptr<Correlation> TCov;

      public:
         Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y, Eigen::MatrixXd m);
         Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y, TH2D* m);
         Correlation(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y);
         Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y, Eigen::MatrixXd m);
         Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y, TH2D* m);
         Correlation(std::shared_ptr<Sample> samp_x, std::shared_ptr<Sample> samp_y);

         void ConfigFrom2DDistribution(TH2D* hist);
         void Config(Eigen::MatrixXd m);
         int GetNBinsX()
         {
            return NBins_X;
         }
         int GetNBinsY()
         {
            return NBins_Y;
         }
         double GetCovariance(int index_x, int index_y);
         Eigen::MatrixXd GetCovariance()
         {
            return cov;
         }
         std::shared_ptr<Observable> RetriveObservableX()
         {
            return Obs_ptr_x;
         }
         std::shared_ptr<Observable> RetriveObservableY()
         {
            return Obs_ptr_y;
         }
         std::shared_ptr<Sample> RetriveSampleX()
         {
            return Samp_ptr_x;
         }
         std::shared_ptr<Sample> RetriveSampleY()
         {
            return Samp_ptr_y;
         }

         std::shared_ptr<Correlation> GetTransposeCorrelation()
         {
            return TCov;
         }
         void SetTransposeCorrelation(std::shared_ptr<Correlation> t_ptr)
         {
            TCov = t_ptr;
         }

         void RebinX(int num);
         void RebinY(int num);
         void SetRangeX(int start, int end);
         void SetRangeY(int start, int end);

         std::shared_ptr<Cache2D> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache();
         void RecoverModifications();
   };

   class CorrelationCategory
   {
      private:
         std::vector<std::shared_ptr<Observable>> Obs_Vec;
         Eigen::MatrixXd RCorr;
         Eigen::VectorXd Data;
         double rest;
         // f(x1...xk) = 1/sqrt((2pi)^k |Sigma|) e^(-1/2 Delta^T Sigma^-1 Delta)
         // ln(f) = (-1/2 Delta^T Sigma^-1 Delta) - ln(sqrt((2pi)^k |Sigma|))
         // RCorr = Sigma^-1
         // rest = ln(sqrt((2pi)^k |Sigma|))

         int NBins = -999;

      public:
         CorrelationCategory(std::vector<std::shared_ptr<Observable>> obs_vec, Eigen::MatrixXd corr);

         int GetNBins()
         {
            return NBins;
         }
         int GetNObservable()
         {
            return Obs_Vec.size();
         }
         std::shared_ptr<Observable> GetObservable(int index);
         double GetLikelihood(Eigen::VectorXd prof);
         double GetLikelihood(std::vector<Eigen::VectorXd> prof_vec);
         static double GetLikelihood_Thread(std::shared_ptr<CorrelationCategory> cate_ptr, Eigen::VectorXd prof)
         {
            return cate_ptr->GetLikelihood(prof);
         }
         static double GetLikelihood_Thread_Vec(std::shared_ptr<CorrelationCategory> cate_ptr, std::vector<Eigen::VectorXd> prof_vec)
         {
            return cate_ptr->GetLikelihood(prof_vec);
         }
         std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gradient(Eigen::VectorXd prof, std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> prof_grad);
         std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gradient(std::vector<Eigen::VectorXd> prof_vec, std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> prof_grad_vec);
         static std::map<std::shared_ptr<Parameter>, double> GetLikelihood_Gradient_Thread(std::shared_ptr<CorrelationCategory> tcate,
                                                                                           std::vector<Eigen::VectorXd> prof_vec, std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> prof_grad_vec)
         {
            return tcate->GetLikelihood_Gradient(prof_vec, prof_grad_vec);
         }
   };
}

#endif
