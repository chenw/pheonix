#pragma once

#include "NAGASH/NAGASH.h"
#include "Phoenix/Parameter.h"
#include "Phoenix/Observable.h"
#include "Phoenix/Sample.h"
#include "Phoenix/Correlation.h"
#include "Phoenix/FitResult.h"
#include "Phoenix/WorkSpace.h"

#ifdef KRONOS
#include "AnalyticalFit/KronosFit.h"
#endif

namespace Phoenix
{
   enum class FittingMethod
   {
      PLH,
      ACS
   };
   class Fitter : public NAGASH::Tool
   {
      public:
         Fitter(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> config);

         std::shared_ptr<Parameter> GetParameter(TString name);
         std::shared_ptr<Parameter> GetParameter(int index);
         std::shared_ptr<Parameter> BookParameter(TString name, double norm, double min, double max, ParameterType type);
         std::shared_ptr<Parameter> BookParameterOfInterest(TString name, double norm, double min, double max);
         std::shared_ptr<Parameter> BookPOI(TString name, double norm, double min, double max);
         std::shared_ptr<Parameter> BookNuisanceParameter(TString name);
         std::shared_ptr<Parameter> BookNP(TString name);
         int GetNParameters();
         void FixParameter(TString name, double val);
         // The function to book and retrive parameters

         std::shared_ptr<MorphingCategory> GetMorphingCategory(TString name);
         std::shared_ptr<MorphingCategory> BookMorphingCategory(TString name);
         void AddParameterToMorphingCategory(TString pname, TString gname);
         // The function to book and retrive Morphing Category

         std::shared_ptr<Parameter> BookNormalizationParameter(TString name, double norm, double min, double max);
         void LinkNormalizationParameterToSample(TString pname, TString obsname, TString samplename);
         void LinkNormalizationParameterToSample(TString pname, TString samplename);
         // The function to book and link Normalization Parameters

         void SetGammaPrefix(TString prefix, TString samplename);
         void SetGammaPrefix(TString prefix, TString obsname, TString samplename);
         // The function to set Gamma Prefix
         // This function only affect the sample you have already booked!

         void InitializeParameters();
         // Automatically create other Morphing Category which contains only one parameter

         std::shared_ptr<Observable> GetObservable(TString name);
         std::shared_ptr<Observable> BookObservable(TString name, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         std::shared_ptr<Observable> BookObservable(TString name, TString fname, TString hname, int start = -1, int end = -1, int rebin = -1);
         std::shared_ptr<Observable> BookObservable(TString name, std::shared_ptr<NAGASH::TFileHelper> fptr, TString hname, int start = -1, int end = -1, int rebin = -1);
         std::shared_ptr<Observable> BookObservable(TString name, TH1* hptr, int start = -1, int end = -1, int rebin = -1);
         // the function to book and retrive Observables

         std::shared_ptr<Sample> GetSample(TString obsname, TString samplename);
         std::shared_ptr<Sample> BookSample(TString obsname, TString samplename, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         std::shared_ptr<Sample> BookSample(TString obsname, TString samplename, TH1* hptr, int start = -1, int end = -1, int rebin = -1);
         // The function to book and retrive Sample

         int GetNVariations(TString obsname, TString samplename, TString gname);
         std::shared_ptr<MorphingValue> GetVariationMorphingValue(TString obsname, TString samplename, TString gname, int i);
         std::shared_ptr<Variation> GetVariation(TString obsname, TString samplename, TString gname, int i);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, const std::vector<double> &morph_vec, const std::vector<double> &val_vec,
                                                  const std::vector<double> &err_vec);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, const std::vector<double> &morph_vec,
                                                  TH1* hptr, int start = -1, int end = -1, int rebin = -1);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, double morph_val, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, double morph_val,
                                                  TH1* hptr, int start = -1, int end = -1, int rebin = -1);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, double morph_val1, double morph_val2, const std::vector<double> &val_vec,
                                                  const std::vector<double> &err_vec);
         std::shared_ptr<Variation> BookVariation(TString obsname, TString samplename, TString gname, double morph_val1, double morph_val2,
                                                  TH1* hptr, int start = -1, int end = -1, int rebin = -1);
         // The function to book and retrive Variations

         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString obsname_y);
         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString obsname_y, Eigen::MatrixXd m);
         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString obsname_y, TH2D* m);
         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y);
         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y, Eigen::MatrixXd m);
         std::shared_ptr<Correlation> BookCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y, TH2D* m);
         std::shared_ptr<Correlation> GetCorrelation(TString obsname_x, TString obsname_y);
         std::shared_ptr<Correlation> GetCorrelation(TString obsname_x, TString sampname_x, TString obsname_y, TString sampname_y);
         bool IsCorrelated(std::shared_ptr<Observable> obs_x, std::shared_ptr<Observable> obs_y);
         bool IsCorrelated(TString obsname_x, TString obsname_y);
         // The function to book and retrive Variations

         void InitializeCorrelation();
         // Call this before initialize morphing!
         void InitializeMorphing();

         // Modification of the variations
         void SynchronizeNominals(TString obs, TString sample, TString parname);
         void SynchronizeNominals(TString parname);
         void SynchronizeNominals();
         void NormalizeVariations(TString obsname, TString samplename, TString parname);
         void NormalizeVariations(TString parname);
         void NormalizeVariations();
         void SmoothVariations(TString obsname, TString samplename, TString parname, int level);
         void SmoothVariations(TString parname, int level);
         void SymmetrizeVariations(TString obsname, TString samplename, TString parname);
         void SymmetrizeVariations(TString parname);
         void Rebin(TString obsname, int num);
         void Rebin(int num);
         void SetRange(TString obsname, int start, int end);
         void SetRange(int start, int end);
         void RecoverModifications();

         void Fit(FittingMethod method);
         void Fit(TString method);
         void PrintResults();
         double GetValue(TString name)
         {
            return GetParameter(name)->GetValue();
         }
         double GetError(TString name)
         {
            return GetParameter(name)->GetError();
         }

         void GetFitResult(std::shared_ptr<FitResult> res);

         void SaveWorkSpace(std::shared_ptr<WorkSpace> ws);

         void SaveFigures();

#ifdef KRONOS
         KronosFit* GetKronosFitter();
#endif

      private:
         std::map<TString, std::shared_ptr<Parameter>> Par_Map;
         std::map<TString, std::shared_ptr<MorphingCategory>> Morph_Map;

         std::map<TString, std::shared_ptr<Observable>> Obs_Map;

         std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>> Sample_Map;
         std::map<std::shared_ptr<Observable>,
             std::map<std::shared_ptr<Sample>,
             std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<Variation>>>>>
             Variation_Map;
         std::map<std::shared_ptr<Observable>,
             std::map<std::shared_ptr<Sample>,
             std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>>>
             Variation_Sigma_Map;
         std::map<std::shared_ptr<Observable>,
             std::map<std::shared_ptr<Sample>,
             std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>
             Variation_MorphingRange_Map;
         std::map<std::shared_ptr<Observable>,
             std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>
             Corr_Obs_Map;
         std::map<std::shared_ptr<Sample>,
             std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>
             Corr_Sample_Map;
         std::vector<std::shared_ptr<CorrelationCategory>> Corr_Cate_Vec;

         // Constant
         double FIT_RANGE_STEP = 5;
         double FIT_MAX_NUISANCE = 3;
         bool FIT_NEED_STAT_GAMMA = false;
         int FIT_NTHREAD = 1;
         bool FIT_WITH_CORRELATION = false;
         double FIT_PARAMETER_PRECISION = 1e-5;
         bool FIT_NEGATIVE_PROTECTION = true;
         bool FIT_USE_RELATIVE_IMPACT = false;
         bool FIT_SUM_IMPACT = false;
         bool FIT_SEPARATE_POI = false;
         bool FIT_RUN_MINOS = false;
         bool FIT_ENABLE_SPEEDUP = false;
         bool FIT_ENABLE_METAL_BOOST = false;

         // Thread Pool
         std::shared_ptr<NAGASH::ThreadPool> pool = nullptr;

         std::vector<std::shared_ptr<MorphingRange>> InitializeMorphingRange1D(
                  const std::vector<std::shared_ptr<MorphingValue>> &val_vec);
         std::vector<std::shared_ptr<MorphingRange>> InitializeMorphingRange1D_FULL_LINEAR(
                  const std::vector<std::shared_ptr<MorphingValue>> &val_vec);
         std::vector<std::shared_ptr<MorphingRange>> InitializeMorphingRange1D_POL6_EXP(
                  const std::vector<std::shared_ptr<MorphingValue>> &val_vec);

         std::vector<std::shared_ptr<MorphingRange>> InitializeMorphingRange2D(
                  const std::vector<std::shared_ptr<MorphingValue>> &val_vec);
         // Using Delaunay triangulation
         std::vector<std::shared_ptr<MorphingRange>> InitializeMorphingRangeXD(
                  const std::vector<std::shared_ptr<MorphingValue>> &val_vec);

         void Fit_PLH(); // Fitting with the Profile Likelihood method (PLH)
         void Fit_ACS(); // Fitting with the Analytical Chi-Square method (ACS)

#ifdef KRONOS
         KronosFit* kfitter = nullptr;
#endif

         FittingMethod TStringToFittingMethod(TString name);

         void Synchronize_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm, std::shared_ptr<Sample> samp);
         void Synchronize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec, std::shared_ptr<Sample> samp);
         void Smooth_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec, int level);
         void Smooth_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm, int level);
         void Symmetrize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec);
         void Symmetrize_Var(std::shared_ptr<Variation> up, std::shared_ptr<Variation> down, std::shared_ptr<Variation> norm);
         void Normalize_Var(std::vector<std::shared_ptr<Variation>> var_vec, std::vector<std::shared_ptr<MorphingValue>> val_vec);
         void Normalize_Var(std::shared_ptr<Variation> var, std::shared_ptr<Variation> norm);
   };
}
