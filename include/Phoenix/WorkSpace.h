#include "NAGASH/NAGASH.h"
#include "Parameter.h"
#include "Sample.h"
#include "Variation.h"
#include "Correlation.h"

namespace Phoenix
{
   class WorkSpace : public NAGASH::Result
   {
      private:
         TTree* Par_Tree = nullptr;

         TString Par_Name;
         double Par_Value;
         double Par_Error;
         double Par_Nominal;
         double Par_Max;
         double Par_Min;
         TString Par_Type;
         bool Par_IsFixed;
         double Par_Gamma_Gaus_Sigma;

         TTree* MorphCate_Tree = nullptr;

         TString MorphCate_Name;
         TString MorphCate_Mode;
         std::vector<TString>* MorphCate_Parameters;

         TTree* Obs_Tree = nullptr;

         TString Obs_Name;
         std::vector<double>* Obs_Value;
         std::vector<double>* Obs_Error;

         TTree* Sample_Tree = nullptr;

         TString Sample_Name;
         TString Sample_Observable_Name;
         std::vector<double>* Sample_Value;
         std::vector<double>* Sample_Error;

         TTree* Var_Tree = nullptr;

         TString Var_Observable_Name;
         TString Var_Sample_Name;
         TString Var_MorphingCategory_Name;
         std::vector<double>* Var_MorphingValue;
         std::vector<double>* Var_Value;
         std::vector<double>* Var_Error;

         TTree* Corr_Tree = nullptr;

         TString Corr_Observable_Name_X;
         TString Corr_Observable_Name_Y;
         TString Corr_Sample_Name_X;
         TString Corr_Sample_Name_Y;
         int Corr_NBins_X;
         int Corr_NBins_Y;
         std::vector<double>* Corr_Matrix;

         TString Name;
         TString FileName;

      public:
         WorkSpace(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> c, const TString &rname, const TString &fname = "");

         void SaveParameters(std::map<TString, std::shared_ptr<Parameter>>* par_map,
                             std::map<TString, std::shared_ptr<MorphingCategory>>* morph_map);
         void SaveObservables(std::map<TString, std::shared_ptr<Observable>>* obs_map);
         void SaveSamples(std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* samp_map);
         void SaveVariations(std::map<std::shared_ptr<Observable>,
                             std::map<std::shared_ptr<Sample>,
                             std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingValue>>>>>
                             * var_val_map);
         void SaveCorrelations(std::map<std::shared_ptr<Observable>,
                               std::map<std::shared_ptr<Observable>, std::shared_ptr<Correlation>>>
                               * corr_obs_map,
                               std::map<std::shared_ptr<Sample>,
                               std::map<std::shared_ptr<Sample>, std::shared_ptr<Correlation>>>
                               * corr_samp_map);

         virtual void WriteToFile();
         virtual void Combine(std::shared_ptr<Result> result) {}
   };
}
