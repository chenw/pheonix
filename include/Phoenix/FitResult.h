#ifndef PHOENIX_FITRESULT_HEADER
#define PHOENIX_FITRESULT_HEADER

#include "NAGASH/NAGASH.h"

namespace Phoenix
{
   class FitResult : public NAGASH::Result
   {
      private:
         std::map<TString, double> val_map;
         std::map<TString, double> unc_map;

      public:
         FitResult(std::shared_ptr<NAGASH::MSGTool> MSG, std::shared_ptr<NAGASH::ConfigTool> c, const TString &rname, const TString &fname);
         void SaveResult(TString name, double val, double unc);
         double GetValue(TString name);
         double GetUncertainty(TString name);
         std::pair<double, double> GetResult(TString name);

         virtual void WriteToFile();
         virtual void Combine(std::shared_ptr<Result> result) {}
   };
}

#endif
