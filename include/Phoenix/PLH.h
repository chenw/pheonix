#ifndef PHOENIX_PLH_HEADER
#define PHOENIX_PLH_HEADER

#include "Minuit2/FCNGradientBase.h"
#include "NAGASH/NAGASH.h"
#include "Parameter.h"
#include "Observable.h"
#include "Sample.h"
#include "Correlation.h"
#include "MetalThreadPool.h"

namespace Phoenix
{
   class PLH_FCN : public ROOT::Minuit2::FCNBase
   {
      private:
         mutable std::map<TString, std::shared_ptr<Parameter>>* Par_Map;
         mutable std::map<TString, std::shared_ptr<Observable>>* Obs_Map;
         mutable std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* Sample_Map;
         mutable std::map<std::shared_ptr<Observable>,
                 std::map<std::shared_ptr<Sample>,
                 std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>
                 * Variation_MorphingRange_Map;
         mutable std::vector<std::shared_ptr<CorrelationCategory>>* Corr_Cate_Vec;

         mutable double Max_Log_Likelihood = -999999;
         mutable int NCall = 0;
         mutable int NCall_Gradient = 0;
         mutable bool WithStatGamma = false;
         mutable bool WithCorr = false;
         mutable bool UseRelative = false;
         mutable bool SumImpact = false;
         mutable bool SeparatePOI = false;
         mutable bool WithMetal = false;

         mutable bool buff_booked = false;
#ifdef PHOENIX_ENABLE_METAL
         mutable MetalThreadPool* metal_pool = nullptr;
         mutable MTL::Buffer* par_value_buff = nullptr;
         mutable MTL::Buffer* par_start_index_buff = nullptr;
         mutable MTL::Buffer* morphing_type_buff = nullptr;
         mutable MTL::Buffer* range_number_buff = nullptr;
         mutable MTL::Buffer* variation_range_par_value_buff = nullptr;
         mutable MTL::Buffer* variation_range_par_start_index_buff = nullptr;
         mutable MTL::Buffer* variation_value_par_value_buff = nullptr;
         mutable MTL::Buffer* variation_value_par_start_index_buff = nullptr;
         mutable MTL::Buffer* variation_value_value_buff = nullptr;
         mutable MTL::Buffer* variation_value_value_start_index_buff = nullptr;
         mutable MTL::Buffer* cache_value_buff = nullptr;
         mutable MTL::Buffer* cache_start_index_buff = nullptr;
         mutable MTL::Buffer* base_value_buff = nullptr;
         mutable MTL::Buffer* userelative_buff = nullptr;
         mutable MTL::Buffer* sumimpact_buff = nullptr;
         mutable MTL::Buffer* result_buff = nullptr;
         mutable MTL::Buffer* result_grad_buff = nullptr;
#endif

         mutable std::vector<std::shared_ptr<Parameter>> par_ptr_vec;
         mutable std::vector<int> par_index_vec;
         mutable std::vector<int> binindex_vec;
         mutable std::vector<std::shared_ptr<Sample>> samp_ptr_vec;
         mutable std::vector<std::shared_ptr<Observable>> obs_ptr_vec;
         mutable std::vector<std::shared_ptr<MorphingCategory>> cate_ptr_vec;
         mutable std::map<std::shared_ptr<Observable>,
                 std::map<std::shared_ptr<Sample>,
                 std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd>>> metal_result_map;
         mutable std::map<std::shared_ptr<Observable>,
                 std::map<std::shared_ptr<Sample>,
                 std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>>>> metal_result_grad_map;
         mutable unsigned int n_metal_units;

         mutable float* par_value_ptr = nullptr;
         mutable unsigned int* par_start_index_ptr = nullptr;
         mutable unsigned int* morphing_type_ptr = nullptr;
         mutable unsigned int* range_number_ptr = nullptr;
         mutable float* variation_range_par_value_ptr = nullptr;
         mutable unsigned int* variation_range_par_start_index_ptr = nullptr;
         mutable float* variation_value_par_value_ptr = nullptr;
         mutable unsigned int* variation_value_par_start_index_ptr = nullptr;
         mutable float* variation_value_value_ptr = nullptr;
         mutable unsigned int* variation_value_value_start_index_ptr = nullptr;
         mutable float* cache_value_ptr = nullptr;
         mutable unsigned int* cache_start_index_ptr = nullptr;
         mutable float* base_value_ptr = nullptr;
         mutable bool* userelative_ptr = nullptr;
         mutable bool* sumimpact_ptr = nullptr;
         mutable float* result_ptr = nullptr;
         mutable float* result_grad_ptr = nullptr;

         mutable std::shared_ptr<NAGASH::MSGTool> msg;
         mutable int Fit_Nthread;

         void InitializeBuffer() const;
         void FillBuffer() const;
         void UpdateBuffer(const std::vector<double> &vpara) const;
         void UpdateBuffer() const;
         void InitializeAllCache() const;
         void CollectMetalResult() const;
         void CollectMetalResult_Gradient() const;

         static Eigen::VectorXd SumOverMorphingCategory(std::shared_ptr<Sample> samp, std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd> r_map, bool SeparatePOI, bool SumImpact);
         static Eigen::VectorXd SumOverSample(std::shared_ptr<Observable> obs, std::vector<Eigen::VectorXd> v_samp );

         static std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> SumOverMorphingCategory_Gradient_NotSumImpact(std::shared_ptr<Sample> samp,
                                                                                                                    std::map<std::shared_ptr<MorphingCategory>, Eigen::VectorXd> r_map,
                                                                                                                    std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>> r_grad_map,
                                                                                                                    Eigen::VectorXd total_impact, bool SeparatePOI);
         static std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> SumOverMorphingCategory_Gradient_SumImpact(std::shared_ptr<Sample> samp,
                                                                                                                 std::map<std::shared_ptr<MorphingCategory>, std::vector<Eigen::VectorXd>> r_grad_map,
                                                                                                                 bool SeparatePOI);
         static std::map<std::shared_ptr<Parameter>, Eigen::VectorXd> SumOverSample_Gradient(std::shared_ptr<Observable> obs, std::vector<std::map<std::shared_ptr<Parameter>, Eigen::VectorXd>> v_samp_grad);
         static std::map<std::shared_ptr<Parameter>, double> SumOverObservable_Gradient(std::vector<std::map<std::shared_ptr<Parameter>, double>> v_obs_grad);

      public:
         PLH_FCN(std::shared_ptr<NAGASH::MSGTool> _msg_,
                 std::map<TString, std::shared_ptr<Parameter>>* par_map,
                 std::map<TString, std::shared_ptr<Observable>>* obs_map,
                 std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* samp_map,
                 std::map<std::shared_ptr<Observable>,
                 std::map<std::shared_ptr<Sample>,
                 std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>* var_range_map,
                 std::vector<std::shared_ptr<CorrelationCategory>>* corr_vec,
                 bool withgamma,
                 bool withcorr,
                 bool userelative,
                 bool sumimpact,
                 bool separatepoi,
                 bool withmetal,
                 int fnthread = 1);

         // function value for negative log likelihood
         double operator()(const std::vector<double> &) const override;
         // error defination
         double Up() const
         {
            return 0.5;
         }
         std::vector<double> Gradient(const std::vector<double> &) const;
         void Reset()
         {
            NCall = 0;
            NCall_Gradient = 0;
            Max_Log_Likelihood = -999999;
         }
         // whether minuit check the gradient is correct, but its validility has been checked.
         bool CheckGradient() const
         {
            return false;
         }
         std::shared_ptr<NAGASH::MSGTool> MSGUser() const
         {
            return msg;
         }
   };

   inline PLH_FCN::PLH_FCN(std::shared_ptr<NAGASH::MSGTool> _msg_,
                           std::map<TString, std::shared_ptr<Parameter>>* par_map,
                           std::map<TString, std::shared_ptr<Observable>>* obs_map,
                           std::map<std::shared_ptr<Observable>, std::map<TString, std::shared_ptr<Sample>>>* samp_map,
                           std::map<std::shared_ptr<Observable>,
                           std::map<std::shared_ptr<Sample>,
                           std::map<std::shared_ptr<MorphingCategory>, std::vector<std::shared_ptr<MorphingRange>>>>>* var_range_map,
                           std::vector<std::shared_ptr<CorrelationCategory>>* corr_vec,
                           bool withgamma,
                           bool withcorr,
                           bool userelative,
                           bool sumimpact,
                           bool separatepoi,
                           bool withmetal,
                           int fnthread)
   {
      msg = _msg_;
      Par_Map = par_map;
      Obs_Map = obs_map;
      Sample_Map = samp_map;
      Variation_MorphingRange_Map = var_range_map;
      Corr_Cate_Vec = corr_vec;
      WithCorr = withcorr;
      WithStatGamma = withgamma;
      UseRelative = userelative;
      SumImpact = sumimpact;
      SeparatePOI = separatepoi;
      WithMetal = withmetal;
      Fit_Nthread = fnthread;

      if (WithMetal)
      {
#ifdef PHOENIX_ENABLE_METAL
         metal_pool = new MetalThreadPool(_msg_);
         metal_pool->LoadFunction("/Users/chenw/Software/Phoenix/MetalLib/Profile.metallib", "ProfileVariation");
#endif
      }
   }
}

#endif
