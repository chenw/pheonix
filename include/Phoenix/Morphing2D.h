#ifndef PHOENIX_MORPHING2D_HEADER
#define PHOENIX_MORPHING2D_HEADER

#include "NAGASH/NAGASH.h"
#include "Parameter.h"

namespace Phoenix
{
   class Triangle;
   class Circle;

   class Point
   {
      private:
         double x;
         double y;

         std::shared_ptr<MorphingValue> val_ptr = nullptr;

      public:
         Point(std::shared_ptr<MorphingValue> ptr)
         {
            val_ptr = ptr;
            x = ptr->GetValueX();
            y = ptr->GetValueY();
         }
         Point(double _x, double _y)
         {
            x = _x;
            y = _y;
         }

         Point()
         {
            x = 0;
            y = 0;
         }

         double X()
         {
            return x;
         }
         double Y()
         {
            return y;
         }
         void SetX(double _x)
         {
            x = _x;
         }
         void SetY(double _y)
         {
            y = _y;
         }
         double Distance(Point a)
         {
            return sqrt(pow(a.X() - x, 2) + pow(a.Y() - y, 2));
         }
         bool IsInside(Circle c);
         bool IsInside(Triangle t);
         std::shared_ptr<MorphingValue> GetMorphingValue()
         {
            return val_ptr;
         }
   };

   class Edge
   {
      private:
         Point s;
         Point e;

      public:
         Edge(Point start, Point end)
         {
            s = start;
            e = end;
         }

         Point Start()
         {
            return s;
         }
         Point End()
         {
            return e;
         }
         double Length()
         {
            return s.Distance(e);
         }
   };

   class Circle
   {
      private:
         Point c;
         double r;

      public:
         Circle(Point center, double rad)
         {
            c = center;
            r = rad;
         }
         Point Center()
         {
            return c;
         }
         double Radius()
         {
            return r;
         }
   };

   class Triangle
   {
      private:
         Point P[3];

      public:
         Triangle(Point a, Point b, Point c)
         {
            P[0] = a;
            P[1] = b;
            P[2] = c;
         }

         Triangle(Edge line, Point p)
         {
            P[0] = p;
            P[1] = line.Start();
            P[2] = line.End();
         }

         Point Vertex(int index)
         {
            return P[index];
         }
         Circle CircumscribedCircle();
         Point Circumcenter()
         {
            return CircumscribedCircle().Center();
         }
         double Space();
   };

   class Graph2DTool : public NAGASH::Tool
   {
      public:
         Graph2DTool(std::shared_ptr<NAGASH::MSGTool> msg, double prec = 1e-5);
         std::vector<Triangle> DelaunayTriangulation(std::vector<Point> p_vec);
         bool CheckDelaunayTriangylation(std::vector<Point> &MyPoints_Vec, std::vector<Triangle> &MyTriangles_Vec, TString fname, bool dosave);

      private:
         double PRECISION;
   };
}

#endif
