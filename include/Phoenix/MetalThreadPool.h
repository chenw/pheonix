#pragma once

#ifdef PHOENIX_ENABLE_METAL
#include <SingleHeader/Metal.hpp>
#include <NAGASH/NAGASH.h>

class MetalThreadPool
{
   private:
      NS::AutoreleasePool* m_pool;
      NS::Error* m_error;
      MTL::Device* m_device;
      MTL::CommandBuffer* m_commandbuffer;
      MTL::ComputePipelineState* m_functionPSO;
      MTL::CommandQueue* m_queue;

      std::shared_ptr<NAGASH::MSGTool> m_msg;

      // Buffers to hold data.
      std::map<TString, MTL::Buffer*> m_Buffer_map;
      std::map<TString, int > m_Buffer_Size_map;
      std::map<TString, int > m_Buffer_Offset_map;
      std::map<TString, int > m_Buffer_Position_map;

   public:
      MetalThreadPool(std::shared_ptr<NAGASH::MSGTool> msg);
      void LoadFunction(TString filename, TString funcname);
      MTL::Buffer* BookBuffer(TString name, int size, int offset, int post);
      MTL::Buffer* GetBuffer(TString name);
      MTL::Device* GetDevice()
      {
         return m_device;
      }
      void Enqueue(int nth);
      void Join()
      {
         m_commandbuffer->waitUntilCompleted();
      }
      std::shared_ptr<NAGASH::MSGTool> MSGUser()
      {
         return m_msg;
      }
      ~MetalThreadPool();
};
#endif
