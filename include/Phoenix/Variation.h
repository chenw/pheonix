#ifndef PHOENIX_VARIATION_HEADER
#define PHOENIX_VARIATION_HEADER

#include "NAGASH/NAGASH.h"
#include "Cache.h"
#include "Observable.h"
#include "Sample.h"
#include "Parameter.h"

namespace Phoenix
{
   class Cache;
   class Sample;
   class Observable;
   class MorphingCategory;

   class Variation
   {
      private:
         int NBins = -999;
         Eigen::VectorXd Value;
         Eigen::VectorXd Error;
         std::shared_ptr<Observable> Obs_ptr;
         std::shared_ptr<Sample> Sample_ptr;
         std::shared_ptr<MorphingCategory> Morphing_ptr;

         std::shared_ptr<Cache> Cache_ptr = nullptr;

      public:
         Variation(std::shared_ptr<Observable> obs_ptr, std::shared_ptr<Sample> sample_ptr, std::shared_ptr<MorphingCategory> group, const std::vector<double> &val_vec, const std::vector<double> &err_vec);
         Variation(std::shared_ptr<Observable> obs_ptr, std::shared_ptr<Sample> sample_ptr, std::shared_ptr<MorphingCategory> group, Eigen::VectorXd val_vec, Eigen::VectorXd err_vec);
         Eigen::VectorXd GetValue()
         {
            return Value;
         }
         Eigen::VectorXd GetError()
         {
            return Error;
         }
         double GetValue(int i);
         double GetError(int i);
         void SetValue(int i, double val);
         void SetError(int i, double err);
         int GetNBins()
         {
            return NBins;
         }
         std::shared_ptr<Observable> RetriveObservable()
         {
            return Obs_ptr;
         }
         std::shared_ptr<Sample> RetriveSample()
         {
            return Sample_ptr;
         }
         std::shared_ptr<MorphingCategory> RetriveMorphingCategory()
         {
            return Morphing_ptr;
         }
         std::shared_ptr<Cache> GetCache()
         {
            return Cache_ptr;
         }
         void BookCache();
         void Rebin(int num);
         void SetRange(int start, int end);
         void RecoverModifications();
   };

   inline Variation::Variation(std::shared_ptr<Observable> obs_ptr, std::shared_ptr<Sample> sample_ptr, std::shared_ptr<MorphingCategory> group, const std::vector<double> &val_vec,
                               const std::vector<double> &err_vec)
   {
      NBins = val_vec.size();
      Value = Eigen::VectorXd(NBins);
      Error = Eigen::VectorXd(NBins);
      for (int i = 0; i < NBins; i++)
      {
         Value(i) = val_vec[i];
         Error(i) = err_vec[i];
      }
      Obs_ptr = obs_ptr;
      Sample_ptr = sample_ptr;
      Morphing_ptr = group;
   }

   inline Variation::Variation(std::shared_ptr<Observable> obs_ptr, std::shared_ptr<Sample> sample_ptr, std::shared_ptr<MorphingCategory> group, Eigen::VectorXd val_vec, Eigen::VectorXd err_vec)
   {
      NBins = val_vec.cols();
      Value = val_vec;
      Error = err_vec;
      Obs_ptr = obs_ptr;
      Sample_ptr = sample_ptr;
      Morphing_ptr = group;
   }

   inline double Variation::GetValue(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Value(i);
   }
   inline double Variation::GetError(int i)
   {
      if (i < 0 || i >= NBins)
         return -999;
      return Error(i);
   }

   inline void Variation::SetValue(int i, double val)
   {
      if (i < 0 || i >= NBins)
         return;
      Value(i) = val;
   }

   inline void Variation::SetError(int i, double err)
   {
      if (i < 0 || i >= NBins)
         return;
      Error(i) = err;
   }
}

#endif
