#include "NAGASH/NAGASH.h"
#include "Phoenix/Fitter.h"

using namespace std;
using namespace Phoenix;

int main(int argc, char** argv)
{
   if (argc < 2)
   {
      std::cout << "Usage : PhoenixReadFitResult FILENAME" << std::endl;
      return 0;
   }

   TFile* infile = new TFile(argv[1], "READ");
   TH1D* result = nullptr;

   TString command;
   int cindex = 0;

   do
   {
      cout << "Command[" << cindex << "]: ";
      string temp_command;
      std::cin >> temp_command;
      command = temp_command;
      cindex++;
      if (command.TString::EqualTo(".List()"))
      {
         if (result == nullptr)
         {
            cout << "Please Load Result with .Load(RESULTNAME)!" << endl;
            continue;
         }
         for (int i = 0; i < result->GetXaxis()->GetNbins(); i++)
            cout << result->GetXaxis()->GetBinLabel(i + 1) << "\t" << result->GetBinContent(i + 1) << " +/- " << result->GetBinError(i + 1) << endl;
      }
      else if (command.TString::EqualTo(".Quit()"))
         return 0;
      else if (command.TString::BeginsWith(".Load("))
      {
         TString resname = command;
         resname.TString::Remove(0, 6);
         resname.TString::Remove(resname.TString::Length() - 1, 1);
         result = (TH1D*)infile->Get(resname.TString::Data());
         if (result == nullptr)
            cout << "Result: " << resname.TString::Data() << " not found!" << endl;
      }
      else
      {
         if (result == nullptr)
         {
            cout << "Please Load Result with .Load(RESULTNAME)!" << endl;
            continue;
         }
         bool found = false;
         for (int i = 0; i < result->GetXaxis()->GetNbins(); i++)
         {
            if (command.TString::EqualTo(result->GetXaxis()->GetBinLabel(i + 1)))
            {
               cout << result->GetBinContent(i + 1) << " +/- " << result->GetBinError(i + 1) << endl;
               found = true;
            }
         }
         if (!found)
            cout << "No Parameter with Name: " << command.TString::Data() << endl;
      }

   }
   while (!command.TString::EqualTo(".Quit()"));

   return 0;
}
