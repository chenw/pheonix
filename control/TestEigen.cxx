#include "Eigen/Dense"
#include <iostream>
int main()
{
   Eigen::MatrixXd mat(3, 3);
   mat(0, 0) = 2;
   mat(0, 1) = 0;
   mat(0, 2) = 0;

   mat(1, 0) = 0;
   mat(1, 1) = 2;
   mat(1, 2) = 0;

   mat(2, 0) = 0;
   mat(2, 1) = 0;
   mat(2, 2) = 2;

   std::cout << mat.inverse() << std::endl;
}
