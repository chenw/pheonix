#ifdef PHOENIX_ENABLE_METAL
#include <NAGASH/NAGASH.h>
#include <SingleHeader/Metal.hpp>
#include <MetalThreadPool.h>

const unsigned int ARRAY_LENGTH = 100000000;

void generateRandomFloatData(MTL::Buffer* buffer)
{
   float* dataPtr = (float*) buffer->contents();

   for (unsigned int index = 0; index < ARRAY_LENGTH; index++)
      dataPtr[index] = float(rand()) / float(RAND_MAX);
}

int main()
{
   std::cout << "Size of unsigned int: " << sizeof(unsigned int) << std::endl;
   std::cout << "UINT_MAX: " << UINT_MAX << std::endl;
   std::shared_ptr<NAGASH::MSGTool> msg = std::make_shared<NAGASH::MSGTool>(NAGASH::MSGLevel::INFO);
   MetalThreadPool* m_pool = new MetalThreadPool(msg);

   m_pool->LoadFunction("MetalLib/Test.metallib", "SumWithIndex");

   auto buffer_input = m_pool->BookBuffer("Input", ARRAY_LENGTH * sizeof(float), 0, 0);
   auto buffer_start = m_pool->BookBuffer("Start_Index", ARRAY_LENGTH * sizeof(unsigned int) / 5, 0, 1);
   auto buffer_end = m_pool->BookBuffer("End_Index", ARRAY_LENGTH * sizeof(unsigned int) / 5, 0, 2);
   auto buffer_result = m_pool->BookBuffer("Result", ARRAY_LENGTH * sizeof(float) / 5, 0, 3);

   generateRandomFloatData(buffer_input);
   unsigned int* start_ptr = (unsigned int*)buffer_start->contents();
   unsigned int* end_ptr = (unsigned int*)buffer_end->contents();
   for (unsigned int index = 0; index < ARRAY_LENGTH / 5; index++)
   {
      if (index % 2 == 0)
      {
         start_ptr[index] = (unsigned int)(5 * index);
         end_ptr[index] = (unsigned int)(5 * index + 3);
      }
      else
      {
         start_ptr[index] = (unsigned int)(5 * (index - 1) + 4);
         end_ptr[index] = (unsigned int)(5 * (index - 1) + 9);
      }
   }
   m_pool->Enqueue(ARRAY_LENGTH / 5);
   m_pool->Join();

   float* input = (float*) m_pool->GetBuffer("Input")->contents();
   unsigned int* start = (unsigned int*) m_pool->GetBuffer("Start_Index")->contents();
   unsigned int* end = (unsigned int*) m_pool->GetBuffer("End_Index")->contents();
   float* result = (float*) m_pool->GetBuffer("Result")->contents();

   bool AllCorrect = true;

   for (unsigned int index = 0; index < (unsigned int)(ARRAY_LENGTH / 5); index++)
   {
      // std::cout<<"Index: "<<index<<" Index MAX:"<<ARRAY_LENGTH / 5<<std::endl;
      double sum = 0;
      // std::cout<<"Start: "<<start[index]<<"   End: "<<end[index]<<std::endl;
      for (unsigned int i = start[index]; i <= end[index]; i++)
      {
         sum = sum + input[i];
         // std::cout << "Input " << i << "   " << input[i] << std::endl;
      }
      if (fabs(sum - result[index]) > 1e-5)
      {
         std::cout << "Result Not Correct! From CPU: " << sum << "   From GPU: " << result[index] << std::endl;
         AllCorrect = false;
      }
      // else
      // std::cout << "Result Correct! From CPU: " << sum << "   From GPU: " << result[index] << std::endl;
   }
   if (AllCorrect)
      std::cout << "All Metal Results are correct!" << std::endl;
   return 0;
}
#endif
#ifndef PHOENIX_ENABLE_METAL
#include <NAGASH/NAGASH.h>
int main()
{
      std::cout << "Please Enable Metal Support!" << std::endl;
     return 0;
}
#endif
