#include "NAGASH/NAGASH.h"
#include <NAGASH/Chi2Fitter.h>

int main()
{
   std::shared_ptr<NAGASH::MSGTool> msg = std::make_shared<NAGASH::MSGTool>(NAGASH::MSGLevel::INFO);
   std::shared_ptr<NAGASH::Chi2Fitter> mfit = std::make_shared<NAGASH::Chi2Fitter>(msg, 1);

   for (int i = 0; i < 1000; i++)
   {
      std::vector<double> val;
      val.push_back(80399 + i);
      mfit->SetModel(val, 20 * pow(val[0] - 80500, 2) + 50);
   }
   mfit->Fit(NAGASH::Chi2Fitter::FitMethod::Minuit);

   return 0;
}
