#include <metal_stdlib>
using namespace metal;

kernel void ProfileVariation(device const float* par_value,
                             device const uint* par_start_index,
                             // Parameter Block
                             device const uint* morphing_type,
                             // Morphing demention and method
                             device const uint* range_number,
                             // Number of Morphing Range
                             device const float* variation_range_par_value,
                             device const uint* variation_range_par_start_index,
                             // Morphing Range
                             device const float* variation_value_par_value,
                             device const uint* variation_value_par_start_index,
                             device const float* variation_value_value,
                             device const uint* variation_value_value_start_index,
                             // Morphing Variations
                             device const float* cache_value,
                             device const uint* cache_start_index,
                             // Cache
                             device const float* base_value,
                             device const bool* userelative,
                             device const bool* sumimpact,
                             device float* result,
                             device float* result_grad,
                             uint index[[thread_position_in_grid]])
{
   uint n_dim = 0;
   if (morphing_type[index] == 1 ||
         morphing_type[index] == 2)
      n_dim = 1;
   else if (morphing_type[index] == 3)
      n_dim = 2;
   else
      return;

   // Find Which Variation is Needed to Calculate the Profile
   uint range_index = 999999;
   if (n_dim == 1)
   {
      float m_par_value = par_value[par_start_index[index]];
      for (uint i = 0; i < range_number[index]; i++)
      {
         uint start_index = variation_range_par_start_index[index] + 2 * i;
         uint end_index = variation_range_par_start_index[index] + 2 * i + 1;
         if ((variation_range_par_value[start_index] < -900) &&
               (variation_range_par_value[end_index] < -900))
            return;
         if (variation_range_par_value[start_index] < -900)
         {
            if (variation_range_par_value[end_index] >= m_par_value)
            {
               range_index = i;
               break;
            }
         }
         else if (variation_range_par_value[end_index] < -900)
         {
            if (variation_range_par_value[start_index] <= m_par_value)
            {
               range_index = i;
               break;
            }
         }
         else
         {
            if ((variation_range_par_value[end_index] >= m_par_value) &&
                  (variation_range_par_value[start_index] <= m_par_value))
            {
               range_index = i;
               break;
            }
         }
      }
   }
   else if (n_dim == 2)
   {
   }
   if (range_index > 999998)
      return;

   // Now Get Cache Run Profile
   if (n_dim == 1)
   {
      float m_par_value = par_value[par_start_index[index]];
      if (morphing_type[index] == 1)
         // FULL_LINEAR
      {
         float k = cache_value[cache_start_index[index] + range_index];
         uint value_index = variation_value_value_start_index[index] + 2 * range_index;
         uint par_index = variation_value_par_start_index[index] + 2 * range_index;
         result[index] = k * (m_par_value - variation_value_par_value[par_index]) + variation_value_value[value_index];
         result_grad[par_start_index[index]] = k;
      }
      else if (morphing_type[index] == 2)
         // POL6_EXP
      {
         uint mid_par_index = variation_value_par_start_index[index] + 3 * range_index + 1;
         uint mid_value_index = variation_value_value_start_index[index] + 3 * range_index + 1;
         uint range_start_index = variation_range_par_start_index[index] + 2 * range_index;
         uint range_end_index = variation_range_par_start_index[index] + 2 * range_index + 1;
         float top;
         float base;
         float A1;
         float A2;
         float A3;
         float A4;
         float A5;
         float A6;
         float top_grad;
         if (variation_range_par_value[range_start_index] < -900)
         {
            base = cache_value[cache_start_index[index]];
            if (variation_value_par_value[mid_par_index - 1] < -900)
            {
               top = (variation_value_par_value[mid_par_index] - m_par_value) / (variation_value_par_value[mid_par_index + 1] - variation_value_par_value[mid_par_index]);
               top_grad = -1.0 / (variation_value_par_value[mid_par_index + 1] - variation_value_par_value[mid_par_index]);
            }
            else
            {
               top = (variation_value_par_value[mid_par_index] - m_par_value) / (variation_value_par_value[mid_par_index] - variation_value_par_value[mid_par_index - 1]);
               top_grad = -1.0 / (variation_value_par_value[mid_par_index] - variation_value_par_value[mid_par_index - 1]);
            }
            result[index] = pow(base, top) * variation_value_value[mid_value_index];
            result_grad[par_start_index[index]] = pow(base, top) * log(base) * top_grad * variation_value_value[mid_value_index];
         }
         else if (variation_range_par_value[range_end_index] < -900)
         {
            base = cache_value[cache_start_index[index] + 7];
            if (variation_value_par_value[mid_par_index + 1] < -900)
            {
               top = (m_par_value - variation_value_par_value[mid_par_index]) / (variation_value_par_value[mid_par_index] - variation_value_par_value[mid_par_index - 1]);
               top_grad = 1.0 / (variation_value_par_value[mid_par_index] - variation_value_par_value[mid_par_index - 1]);
            }
            else
            {
               top = (m_par_value - variation_value_par_value[mid_par_index]) / (variation_value_par_value[mid_par_index + 1] - variation_value_par_value[mid_par_index]);
               top_grad = 1.0 / (variation_value_par_value[mid_par_index + 1] - variation_value_par_value[mid_par_index]);
            }
            result[index] = pow(base, top) * variation_value_value[mid_value_index];
            result_grad[par_start_index[index]] = pow(base, top) * log(base) * top_grad * variation_value_value[mid_value_index];
         }
         else
         {
            A1 = cache_value[cache_start_index[index] + 1];
            A2 = cache_value[cache_start_index[index] + 2];
            A3 = cache_value[cache_start_index[index] + 3];
            A4 = cache_value[cache_start_index[index] + 4];
            A5 = cache_value[cache_start_index[index] + 5];
            A6 = cache_value[cache_start_index[index] + 6];
            float I = 1 + (m_par_value - variation_value_par_value[mid_par_index]) * A1 +
                      pow(m_par_value - variation_value_par_value[mid_par_index], 2) * A2 +
                      pow(m_par_value - variation_value_par_value[mid_par_index], 3) * A3 +
                      pow(m_par_value - variation_value_par_value[mid_par_index], 4) * A4 +
                      pow(m_par_value - variation_value_par_value[mid_par_index], 5) * A5 +
                      pow(m_par_value - variation_value_par_value[mid_par_index], 6) * A6;
            float I_grad = A1 + 2 * A2 * (m_par_value - variation_value_par_value[mid_par_index]) +
                           3 * A3 * pow(m_par_value - variation_value_par_value[mid_par_index], 2) +
                           4 * A4 * pow(m_par_value - variation_value_par_value[mid_par_index], 3) +
                           5 * A5 * pow(m_par_value - variation_value_par_value[mid_par_index], 4) +
                           6 * A6 * pow(m_par_value - variation_value_par_value[mid_par_index], 5);
            result[index] = I * variation_value_value[mid_value_index];
            result_grad[par_start_index[index]] = I_grad * variation_value_value[mid_value_index];
         }
      }
   }
   else if (n_dim == 2)
   {
   }

   bool UseRelative = userelative[index];
   bool SumImpact = sumimpact[index];

   if (UseRelative && SumImpact)
      result[index] = ( result[index] / base_value[index] - 1);
   else if (UseRelative && !SumImpact)
      result[index] = result[index] / base_value[index];
   else if (!UseRelative && SumImpact)
      result[index] = result[index] - base_value[index];
   else if (!UseRelative && !SumImpact)
      result[index] = result[index] / base_value[index];
   // We don't understand this option

}
