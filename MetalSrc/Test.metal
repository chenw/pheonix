#include <metal_stdlib>
using namespace metal;

kernel void SumWithIndex(device const float* input_val,
                         device const uint* start_index,
                         device const uint* end_index,
                         device float* result,
                         uint index[[thread_position_in_grid]])
{
   float sum = 0;
   for (uint i = start_index[index]; i <= end_index[index]; i++)
      sum = sum + input_val[i];
   result[index] = sum;
}
