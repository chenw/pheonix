###################################
# Makefile of Phoenix Fitter      #
# Author : Chen Wang              #
# Last Modified: 2023/07/13        #
###################################

# Enable METAL CPP support
ENABLE_METAL=false

# This is quite a general makefile which includes automatic deduction of dependencies.
# Users can change the name of the .so file in order to apply to other projects
# as well as change the flags and compile options to suit their own needs.

# normal compile flags
CXX = g++
ROOT_FLAGS = $(shell root-config --cflags --libs) -lMinuit -lMinuit2
ifeq ($(ENABLE_METAL),true) 
CXX_FLAGS = -std=c++2a -lpthread -lm -O3 -fPIC -D NAGASH_MACOS -D KRONOS -D PHONEX_ENABLE_METAL -lKronosFitLib -lRooFit -lRooFitCore
else 
CXX_FLAGS = -std=c++2a -lpthread -lm -O3 -fPIC -D NAGASH_MACOS -D KRONOS -lKronosFitLib -lRooFit -lRooFitCore
endif
CXX_INCLUDE = -I./include
CXX_LIB = -L./lib -lNAGASH -Wl,-rpath,$(shell pwd)/lib $(patsubst lib%.so,-l%,$(notdir $(wildcard ./lib/*.so)))
ifeq ($(ENABLE_METAL),true) 
METAL_LIB = -Wl,$(shell xcrun --show-sdk-path)/System/Library/Frameworks/Foundation.framework/Foundation.tbd -Wl,$(shell xcrun --show-sdk-path)/System/Library/Frameworks/QuartzCore.framework/QuartzCore.tbd -Wl,$(shell xcrun --show-sdk-path)/System/Library/Frameworks/Metal.framework/Metal.tbd
else 
METAL_LIB = 
endif

# for auto generation of dependency file 
OBJDIR := obj
DEPDIR := $(OBJDIR)/.deps
DEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.d
COMPILE.o = $(CXX) $(DEPFLAGS) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_INCLUDE) $(METAL_LIB) -c $< -o ./$(OBJDIR)/$@
COMPILE.so = $(CXX) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_INCLUDE) $(METAL_LIB) -lNAGASH -shared -fPIC -o $@ $^
COMPILE.exe = $(CXX) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_LIB) $(CXX_INCLUDE) $(METAL_LIB) -o $@ $^

# list of objects needed to be built
SRCS = $(notdir $(wildcard ./src/*.cxx ./control/*.cxx))
OBJECTS = $(patsubst %.cxx,%.o,$(notdir $(wildcard ./src/*.cxx ./control/*.cxx)))
ifeq ($(ENABLE_METAL),true) 
LIBS = ./lib/libPhoenix.so METAL
else
LIBS = ./lib/libPhoenix.so
endif
EXECUTABLES = $(patsubst %.cxx,./bin/%,$(notdir $(wildcard ./control/*.cxx)))

VPATH = ./control:./src:./include:./obj

# build all objects
release : 
	@mkdir -p $(OBJDIR) bin lib
	+make $(OBJECTS)
	+make $(LIBS)
	+make $(EXECUTABLES)

# build METAL Lib
METAL : 
	@mkdir -p MetalLib
	+make MetalLib/Profile.metallib
	+make MetalLib/Test.metallib

MetalLib/Profile.metallib : MetalSrc/Profile.metal
	xcrun -sdk macosx metal -c MetalSrc/Profile.metal -o MetalLib/Profile.air
	xcrun -sdk macosx metallib -o MetalLib/Profile.metallib MetalLib/Profile.air

MetalLib/Test.metallib : MetalSrc/Test.metal
	xcrun -sdk macosx metal -c MetalSrc/Test.metal -o MetalLib/Test.air
	xcrun -sdk macosx metallib -o MetalLib/Test.metallib MetalLib/Test.air

# build .o file
%.o : %.cxx $(DEPDIR)/%.d | $(DEPDIR)
	@$(COMPILE.o)

# for dependency files
$(DEPDIR): ; @mkdir -p $@

DEPFILES := $(SRCS:%.cxx=$(DEPDIR)/%.d)
$(DEPFILES):
include $(wildcard $(DEPFILES))

# build LIBS
./lib/libPhoenix.so : $(patsubst %.cxx,./obj/%.o,$(notdir $(wildcard ./src/*.cxx)))
	@$(COMPILE.so)

# build executable file 
./bin/% : ./$(OBJDIR)/%.o
	@$(COMPILE.exe)

# phony objects
.PHONY : clean
clean :
	-rm ./bin/* ./obj/* ./lib/* ./obj/.deps/* ./MetalLib/*
